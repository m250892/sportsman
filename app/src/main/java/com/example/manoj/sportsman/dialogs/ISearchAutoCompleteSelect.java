package com.example.manoj.sportsman.dialogs;

import com.example.manoj.sportsman.databasemanager.SearchItemModel;

/**
 * Created by manoj on 01/11/15.
 */
public interface ISearchAutoCompleteSelect {
    void onAutoCompleteItemSelect(SearchItemModel searchItemModel);
}
