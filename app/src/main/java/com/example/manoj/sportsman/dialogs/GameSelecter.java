package com.example.manoj.sportsman.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.adapters.GameSelecterAdapter;

/**
 * Created by manoj on 28/10/15.
 */
public class GameSelecter extends DialogFragment implements AdapterView.OnItemClickListener {

    private Context context;
    private DialogCallbacks callback;

    public GameSelecter(DialogCallbacks callback) {
        this.callback = callback;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_selecter_layout, container, true);

        GridView gridView = (GridView) view.findViewById(R.id.grid_view);
        GameSelecterAdapter adapter = new GameSelecterAdapter(context);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismiss();
        if (callback != null) {
            callback.onGameSelected(Constants.getAvailableGameIds().get(position));
        }

    }
}
