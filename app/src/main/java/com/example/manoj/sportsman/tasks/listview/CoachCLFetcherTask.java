package com.example.manoj.sportsman.tasks.listview;

import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.listview.CoachCardModel;
import com.example.manoj.sportsman.models.listview.PlayerCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 26/12/15.
 */
public class CoachCLFetcherTask extends BaseCLFetcherTask {

    public CoachCLFetcherTask(FetchCardListTaskCallback callback, CardsListType cardsListType) {
        super(callback, cardsListType);
    }

    @Override
    protected String getBaseRequestUrl() {
        return null;
    }

    @Override
    protected List<BaseCardModel> fetchData(String flatDetailsRequest) throws Exception {
        List<BaseCardModel> baseCardModels = new ArrayList<>();
        baseCardModels.add(new PlayerCardModel());
        baseCardModels.add(new PlayerCardModel());
        baseCardModels.add(new PlayerCardModel());
        return baseCardModels;
    }

    private class CoachesCLResponseWrapper extends CardListResponseWrapper<CoachCardModel> {
    }
}
