package com.example.manoj.sportsman.fragments.listview.cards.players;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.models.listview.PlayerCardModel;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;
import com.example.manoj.sportsman.views.GLevelRingView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by manoj on 25/12/15.
 */
public class PlayerCardViewSetter extends BaseListCardViewSetter<PlayerCardModel, PlayerCardViewHolder> {

    private PlayerCardModel playerCardModel;
    private int listingIndex;

    public PlayerCardViewSetter(PlayerCardModel baseCardModel,
                                Context context,
                                IChildViewClickListener childViewClickListener,
                                int listingIndex) {
        super(baseCardModel, context, childViewClickListener);

        this.playerCardModel = baseCardModel;
        this.listingIndex = listingIndex;
    }

    @Override
    protected void setViews(PlayerCardViewHolder holder) {
        holder.nameView.setText(playerCardModel.getName());
        holder.ageGenderView.setText(playerCardModel.getAgeGenderString());
        if (playerCardModel.getSearchItemModel() != null) {
            holder.areaName.setText(playerCardModel.getSearchItemModel().getAddressDetail());
        } else {
            holder.areaName.setVisibility(View.GONE);
        }
        Picasso.with(getContext()).load(playerCardModel.getImageUrl()).into(holder.profilePic);
        addPlayerGamesWithLevel(holder.gameLevelView);
    }

    @Override
    protected View buildView(LayoutInflater inflater, ViewGroup parent) {
        return getLayoutView(R.layout.lv_player_card_layout);
    }

    private void addPlayerGamesWithLevel(LinearLayout convertView) {
        convertView.removeAllViews();
        LinearLayout gLevelView = (LinearLayout) convertView.findViewById(R.id.glevel_view);
        List<UserFavSportModel> favSportModels = playerCardModel.getUserFavSportModels();
        for (int i = 0; i < favSportModels.size(); i++) {
            gLevelView.addView(getChildView(favSportModels.get(i)));
        }
    }

    private View getChildView(UserFavSportModel favSportModel) {
        View childView = getLayoutView(R.layout.lvpglevel_representation_layout);
        GLevelRingView gLevelRingView = (GLevelRingView) childView.findViewById(R.id.glevel_ring_view);
        gLevelRingView.setLevel(favSportModel.getRating());
        return childView;
    }

    @Override
    protected PlayerCardViewHolder buildViewHolder(View cardview) {
        return new PlayerCardViewHolder(cardview);
    }
}
