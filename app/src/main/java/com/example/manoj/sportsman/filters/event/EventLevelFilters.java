package com.example.manoj.sportsman.filters.event;

import android.app.Activity;

import com.example.manoj.sportsman.filters.FilterConstants;
import com.example.manoj.sportsman.filters.core.BaseCheckBoxFilterGroup;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by manoj on 05/01/16.
 */
public class EventLevelFilters extends BaseCheckBoxFilterGroup<EventFilterHash> {

    public EventLevelFilters(Activity activity) {
        super(activity);
    }

    @Override
    protected Map getLinkedHashMap() {
        return FilterConstants.EVENT_GAME_LEVEL_MAP;
    }

    @Override
    protected int getNumberOfElementsPerRow() {
        return 1;
    }

    @Override
    protected String getLabel() {
        return "GAME LEVEL";
    }

    @Override
    protected void setFilterHashList(EventFilterHash filterHash, ArrayList<String> typeList) {
        filterHash.setEventLevels(typeList);
    }

    @Override
    protected ArrayList<String> getKeyListFromFilterHash(EventFilterHash filterHash) {
        return filterHash.getEventLevels();
    }
}
