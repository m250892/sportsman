package com.example.manoj.sportsman.tasks.listview;

import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.listview.PlaygroundCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 26/12/15.
 */
public class PlaygroundCLFetcherTask extends BaseCLFetcherTask {

    public PlaygroundCLFetcherTask(FetchCardListTaskCallback callback, CardsListType cardsListType) {
        super(callback, cardsListType);
    }

    @Override
    protected String getBaseRequestUrl() {
        return null;
    }

    @Override
    protected List<BaseCardModel> fetchData(String flatDetailsRequest) throws Exception {
        List<BaseCardModel> baseCardModels = new ArrayList<>();
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        baseCardModels.add(new PlaygroundCardModel());
        return baseCardModels;
    }

    private class PlaygroundCLResponseWrapper extends CardListResponseWrapper<PlaygroundCardModel> {
    }
}
