package com.example.manoj.sportsman.fragments.listview.cards.playground;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.models.listview.PlaygroundCardModel;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;
import com.example.manoj.sportsman.views.GLevelRingView;

import java.util.List;

/**
 * Created by manoj on 26/12/15.
 */
public class PlaygroundCardViewSetter extends BaseListCardViewSetter<PlaygroundCardModel, PlaygroundCardViewHolder> {

    private PlaygroundCardModel playgroundCardModel;
    private int listingIndex;

    public PlaygroundCardViewSetter(PlaygroundCardModel baseCardModel, Context context, IChildViewClickListener childViewClickListener, int listingIndex) {
        super(baseCardModel, context, childViewClickListener);
        this.playgroundCardModel = baseCardModel;
        this.listingIndex = listingIndex;
    }

    @Override
    protected void setViews(PlaygroundCardViewHolder holder) {
        addPlayerGamesWithLevel(holder.availableGamesView);
    }

    private void addPlayerGamesWithLevel(LinearLayout convertView) {
        convertView.removeAllViews();
        LinearLayout gLevelView = (LinearLayout) convertView.findViewById(R.id.glevel_view);
        List<UserFavSportModel> favSportModels = playgroundCardModel.getUserFavSportModels();
        for (int i = 0; i < favSportModels.size(); i++) {
            gLevelView.addView(getChildView(favSportModels.get(i)));
        }
    }

    private View getChildView(UserFavSportModel favSportModel) {
        View childView = getLayoutView(R.layout.lvpglevel_representation_layout);
        GLevelRingView gLevelRingView = (GLevelRingView) childView.findViewById(R.id.glevel_ring_view);
        gLevelRingView.setLevel(favSportModel.getRating());
        return childView;
    }

    @Override
    protected View buildView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.lv_playground_card_layout, null);
    }

    @Override
    protected PlaygroundCardViewHolder buildViewHolder(View cardview) {
        return new PlaygroundCardViewHolder(cardview);
    }
}
