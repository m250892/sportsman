package com.example.manoj.sportsman.models.listview;

import com.example.manoj.sportsman.models.CardType;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 24/12/15.
 */
public class PlaygroundCardModel extends BaseCardModel {

    private List<UserFavSportModel> userFavSportModels;

    public PlaygroundCardModel() {
        userFavSportModels = new ArrayList<>();
        userFavSportModels.add(new UserFavSportModel(1));
        userFavSportModels.add(new UserFavSportModel(2));
        userFavSportModels.add(new UserFavSportModel(3));
    }

    @Override
    public CardType getCardType() {
        return CardType.PLAYGROUND_CARD;
    }

    @Override
    public String getId() {
        return null;
    }

    public List<UserFavSportModel> getUserFavSportModels() {
        return userFavSportModels;
    }
}
