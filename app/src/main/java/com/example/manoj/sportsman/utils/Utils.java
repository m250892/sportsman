package com.example.manoj.sportsman.utils;

import android.content.Context;
import android.location.LocationManager;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.MainApplication;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.Calendar;
import java.util.Collection;

/**
 * Created by manoj on 01/11/15.
 */
public class Utils {


    public static String createJSONStringFromObject(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static <T> T createObjectFromJSONString(String jsonString, Class<T> clazz)
            throws JsonSyntaxException {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, clazz);
    }

    public static String getMonthNameFromDate(int monthNumber) {
        if (monthNumber >= 0 && monthNumber < 12) {
            return Constants.monthNames[monthNumber];
        }
        return "";
    }

    public static String getDayName(int dayNumber) {
        if (dayNumber >= 0 && dayNumber < 7) {
            return Constants.daysNames[dayNumber];
        }
        return "";
    }

    public static String addSuffixInDate(int day) {
        if (day >= 11 && day <= 13) {
            return day + "th";
        }
        switch (day % 10) {
            case 1:
                return day + "st";
            case 2:
                return day + "nd";
            case 3:
                return day + "rd";
            default:
                return day + "th";
        }
    }

    public static String getCompleteDateFormattedString(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        return getDateMonthYearFormatString(day, month, year);
    }

    public static String getDateTimeFormateString(long mills) {
        return getCompleteDateFormattedString(mills) + ", at " + getTimeString(mills);
    }

    public static String getDateMonthFormatString(int day, int month) {
        return addSuffixInDate(day) + " " + Constants.getMonthName(month);
    }

    public static String getDateMonthYearFormatString(int day, int month, int year) {
        return getDateMonthFormatString(day, month) + " " + year;
    }

    public static String getDayName(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return Constants.getDayName(calendar.get(Calendar.DAY_OF_WEEK) - 1);
    }

    public static String getDayShortName(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return Constants.getDayShortName(calendar.get(Calendar.DAY_OF_WEEK) - 1);
    }

    public static String getTwoDigitString(int number) {
        String result = "";
        if (number < 10) {
            result = "0";
        }
        result += String.valueOf(number);
        return result;
    }

    public static String getTimeString(long millis) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        int hour = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        return getTimeString(hour, minutes);
    }

    public static String getTimeString(int hour, int minutes) {
        String result = "";
        if (hour >= 12) {
            result += (hour - 12) + ":" + getTwoDigitString(minutes) + "pm";
        } else {
            result += hour + ":" + getTwoDigitString(minutes) + "am";
        }
        return result;
    }

    public static String getDateString(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        String result = getDayName(calendar.getFirstDayOfWeek()) + ", " + day + " " + getMonthNameFromDate(month) + " " + year;
        return result;
    }

    public static boolean isGPSEnabled() {
        LocationManager lm = (LocationManager) MainApplication.getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled;
    }

    public static int getColorWithAlpha(float alpha, int baseColor) {
        int a = Math.min(255, Math.max(0, (int) (alpha * 255))) << 24;
        int rgb = 0x00ffffff & baseColor;
        return a + rgb;
    }

    public static boolean isCollectionFilled(Collection<?> collection) {
        return null != collection && collection.isEmpty() == false;
    }

    public static boolean compareString(String str1, String str2) {
        if (str1 == null && str2 == null) {
            return true;
        } else if (str1 == null || str2 == null) {
            return false;
        } else {
            return str1.equals(str2);
        }
    }


}
