package com.example.manoj.sportsman.utils.customvalueselecter;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by manoj on 26/11/15.
 */
public class SelecterFragment extends BaseFragment implements CustomSelecterCallbacks {

    private ArrayList<CustomSelectionItem> mMonthsArrayList;
    public RecyclerView mMonthList;
    private CenterLockListener mMonthCenterLock;
    private int mPrevSelectedMonthPos = 7;
    public View highlighter;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static SelecterFragment newInstance() {
        return new SelecterFragment();
    }

    public ArrayList<CustomSelectionItem> getMonthList() {
        ArrayList localArrayList = new ArrayList();
        for (String month : Constants.monthNames) {
            localArrayList.add(new CustomSelectionItem(month, 0, false));
        }
        return localArrayList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.custom_selector_fragment_layout, container, false);
        mMonthList = (RecyclerView) baseView.findViewById(R.id.my_recycler_view);
        initializeRecycleView();

        highlighter = baseView.findViewById(R.id.highlighter_view);
        highlighter.getViewTreeObserver().addOnGlobalLayoutListener(new SelectorGlobelLayoutListener());
        return baseView;
    }

    public void initializeRecycleView() {
        mLayoutManager = new LinearLayoutManager(getContext());
        mMonthList.setLayoutManager(mLayoutManager);
        mMonthsArrayList = getMonthList();

        this.mMonthsArrayList.get(this.mPrevSelectedMonthPos).isSelected = true;
        mAdapter = new CustomSelectorAdapter(getContext(), mMonthsArrayList);
        mMonthList.setAdapter(mAdapter);
    }

    @Override
    public void handlePositionChanged(int position, int viewId) {
        this.mMonthsArrayList.get(mPrevSelectedMonthPos).isSelected = false;
        this.mMonthsArrayList.get(position).isSelected = true;
        mPrevSelectedMonthPos = position;
        this.mMonthList.getAdapter().notifyDataSetChanged();
    }

    private class SelectorGlobelLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mMonthCenterLock = new CenterLockListener(getActivityReference(), (highlighter.getTop() + highlighter.getBottom()) / 2, SelecterFragment.this);
            mMonthList.setOnScrollListener(mMonthCenterLock);
            mMonthList.scrollToPosition(mPrevSelectedMonthPos);
            int paddingTop = highlighter.getTop();
            int paddingBottom = mMonthList.getHeight() - highlighter.getBottom();
            mMonthList.setPadding(mMonthList.getPaddingLeft(), paddingTop, mMonthList.getPaddingRight(), paddingBottom);
            highlighter.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }
}
