package com.example.manoj.sportsman.utils.customvalueselecter;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by manoj on 26/11/15.
 */
public class DateSelectorFragment extends BaseFragment implements CustomSelecterCallbacks {

    private ArrayList<CustomSelectionItem> mMonthsArrayList;
    public RecyclerView mMonthRecycleView;
    private CenterLockListener mMonthCenterLock;
    private int mPrevSelectedMonthPos = 0;
    public View monthHighlighterView;
    private RecyclerView.Adapter mMonthAdapter;
    private RecyclerView.LayoutManager mMonthLayoutManager;

    private ArrayList<CustomSelectionItem> mDayssArrayList;
    public RecyclerView mDaysRecycleView;
    private CenterLockListener mDaysCenterLock;
    private int mPrevSelectedDaysPos = 0;
    public View dayHighlighterView;
    private RecyclerView.Adapter mDaysAdapter;
    private RecyclerView.LayoutManager mDaysLayoutManager;
    private Calendar calendar = Calendar.getInstance();

    public static DateSelectorFragment newInstance() {
        return new DateSelectorFragment();
    }

    public ArrayList<CustomSelectionItem> getMonthList() {
        int totalMonthsInList = 0;
        int currentMonth = calendar.get(Calendar.MONTH);
        ArrayList localArrayList = new ArrayList();
        while (totalMonthsInList < 4) {
            if (currentMonth == 12) {
                currentMonth = 0;
            }
            localArrayList.add(new CustomSelectionItem(Constants.monthNames[currentMonth], currentMonth, false));
            totalMonthsInList++;
            currentMonth++;
        }
        return localArrayList;
    }

    public ArrayList<CustomSelectionItem> getDaysList(int maxSize) {
        ArrayList localArrayList = new ArrayList();
        for (int index = 1; index <= maxSize; index++) {
            localArrayList.add(new CustomSelectionItem(String.valueOf(index), index, false));
        }
        return localArrayList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.select_date_fragment_layout, container, false);
        calendar.setTimeInMillis(System.currentTimeMillis());
        initViews(baseView);
        return baseView;
    }

    private void initViews(View baseView) {
        mMonthRecycleView = (RecyclerView) baseView.findViewById(R.id.month_recycle_view);
        initializeMonthRecycleView();
        monthHighlighterView = baseView.findViewById(R.id.month_highlighter_view);
        monthHighlighterView.getViewTreeObserver().addOnGlobalLayoutListener(new CustomMonthGlobalLayoutListener());

        mDaysRecycleView = (RecyclerView) baseView.findViewById(R.id.days_recycle_view);
        initializeDaysRecycleView();
        dayHighlighterView = baseView.findViewById(R.id.days_highlighter_view);
        dayHighlighterView.getViewTreeObserver().addOnGlobalLayoutListener(new CustomDaysGlobalLayoutListener());

    }

    public void initializeMonthRecycleView() {
        mMonthLayoutManager = new LinearLayoutManager(getContext());
        mMonthRecycleView.setLayoutManager(mMonthLayoutManager);
        mMonthsArrayList = getMonthList();
        int preSelectedMonthId = getPreviousSelectedMonth();
        Log.d("manoj", "preSelectedMonthId : " + preSelectedMonthId);
        for (int index = 0; index < mMonthsArrayList.size(); index++) {
            if (mMonthsArrayList.get(index).id == preSelectedMonthId) {
                mPrevSelectedMonthPos = index;
                break;
            }
        }
        Log.d("manoj", "mPrevSelectedMonthPos : " + mPrevSelectedMonthPos);
        this.mMonthsArrayList.get(this.mPrevSelectedMonthPos).isSelected = true;
        mMonthAdapter = new CustomSelectorAdapter(getContext(), mMonthsArrayList);
        mMonthRecycleView.setAdapter(mMonthAdapter);
        mMonthRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mMonthRecycleView.smoothScrollToPosition(position);
                        handlePositionChanged(position, mMonthRecycleView.getId());
                    }
                })
        );
    }

    public void initializeDaysRecycleView() {
        mDaysLayoutManager = new LinearLayoutManager(getContext());
        mDaysRecycleView.setLayoutManager(mDaysLayoutManager);
        mDayssArrayList = getDaysList(31);
        int preSelectedDayId = getPreviousSelectetDay();
        for (int index = 0; index < mDayssArrayList.size(); index++) {
            if (mDayssArrayList.get(index).id == preSelectedDayId) {
                mPrevSelectedDaysPos = index;
                break;
            }
        }
        this.mDayssArrayList.get(this.mPrevSelectedDaysPos).isSelected = true;
        mDaysAdapter = new CustomSelectorAdapter(getContext(), mDayssArrayList);
        mDaysRecycleView.setAdapter(mDaysAdapter);
        mDaysRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mDaysRecycleView.smoothScrollToPosition(position);
                        handlePositionChanged(position, mDaysRecycleView.getId());
                    }
                })
        );
    }

    @Override
    public void handlePositionChanged(int position, int viewId) {
        Log.d("manoj", "handlePositionChanged,  position : " + position);
        if (R.id.month_recycle_view == viewId) {
            this.mMonthsArrayList.get(mPrevSelectedMonthPos).isSelected = false;
            this.mMonthsArrayList.get(position).isSelected = true;
            mPrevSelectedMonthPos = position;
            this.mMonthRecycleView.getAdapter().notifyDataSetChanged();
        } else if (R.id.days_recycle_view == viewId) {
            this.mDayssArrayList.get(mPrevSelectedDaysPos).isSelected = false;
            this.mDayssArrayList.get(position).isSelected = true;
            mPrevSelectedDaysPos = position;
            this.mDaysRecycleView.getAdapter().notifyDataSetChanged();
        } else {
            Log.d("manoj", "handlePositionChanged,  did not match to any  pre define ID");
        }
    }

    public int getPreviousSelectedMonth() {
        if (EventCreateDataModel.getInstance().getDateTimeModel().isDateSelected()) {
            return EventCreateDataModel.getInstance().getDateTimeModel().getMonth();
        }
        return calendar.get(Calendar.MONTH);
    }

    public int getPreviousSelectetDay() {
        if (EventCreateDataModel.getInstance().getDateTimeModel().isDateSelected()) {
            return EventCreateDataModel.getInstance().getDateTimeModel().getDay();
        }
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    private class CustomMonthGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mMonthCenterLock = new CenterLockListener(getActivityReference(), (monthHighlighterView.getTop() + monthHighlighterView.getBottom()) / 2, DateSelectorFragment.this);
            mMonthRecycleView.setOnScrollListener(mMonthCenterLock);
            mMonthRecycleView.smoothScrollToPosition(mPrevSelectedMonthPos);
            int paddingTop = monthHighlighterView.getTop();
            int paddingBottom = mMonthRecycleView.getHeight() - monthHighlighterView.getBottom();
            mMonthRecycleView.setPadding(mMonthRecycleView.getPaddingLeft(), paddingTop, mMonthRecycleView.getPaddingRight(), paddingBottom);
            monthHighlighterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }

    private class CustomDaysGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mDaysCenterLock = new CenterLockListener(getActivityReference(), (monthHighlighterView.getTop() + monthHighlighterView.getBottom()) / 2, DateSelectorFragment.this);
            mDaysRecycleView.setOnScrollListener(mDaysCenterLock);
            mDaysRecycleView.smoothScrollToPosition(mPrevSelectedDaysPos);
            int paddingTop = dayHighlighterView.getTop();
            int paddingBottom = mDaysRecycleView.getHeight() - dayHighlighterView.getBottom();
            mDaysRecycleView.setPadding(mDaysRecycleView.getPaddingLeft(), paddingTop, mDaysRecycleView.getPaddingRight(), paddingBottom);
            dayHighlighterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }
}

