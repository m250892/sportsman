package com.example.manoj.sportsman.filters.core;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.filters.filterhash.BaseFilterHash;

public abstract class BaseSpinnerFilter<T extends BaseFilterHash> extends BaseFilterGroup<T> {

    private View view;
    private Spinner spinner;

    public BaseSpinnerFilter(Activity activity) {
        super(activity);
        initView();
    }

    private void initView() {

        this.spinner = (Spinner) LayoutInflater.from(this.getActivity())
                .inflate(R.layout.filters_spinner, null);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(),
                R.layout.filters_spinner_item,
                getItemArray());
        this.spinner.setAdapter(adapter);

        if (getLabel() != null) {
            LinearLayout linearLayout = new LinearLayout(getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            TextView label = (TextView) LayoutInflater.from(this.getActivity())
                    .inflate(R.layout.filters_label, linearLayout,
                            false);
            label.setText(getLabel());
            linearLayout.addView(label);
            linearLayout.addView(this.spinner);
            this.view = linearLayout;
        } else {
            this.view = this.spinner;
        }
    }

    protected int getSelectedItemPosition() {
        return this.spinner.getSelectedItemPosition();
    }

    protected void setSelectionPosition(int position) {
        this.spinner.setSelection(position);
    }

    @Override
    public View getView() {
        return this.view;
    }

    protected abstract String[] getItemArray();

    protected abstract int getDefaultItemIndex();

    protected abstract String getLabel();
}
