package com.example.manoj.sportsman.utils.customvalueselecter;

/**
 * Created by manoj on 26/11/15.
 */
public interface CustomSelecterCallbacks {
    void handlePositionChanged(int position, int viewID);
}
