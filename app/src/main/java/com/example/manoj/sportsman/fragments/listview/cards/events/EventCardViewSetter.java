package com.example.manoj.sportsman.fragments.listview.cards.events;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.models.listview.EventCardModel;
import com.example.manoj.sportsman.utils.Utils;


/**
 * Created by manoj on 24/12/15.
 */
public class EventCardViewSetter
        extends BaseListCardViewSetter<EventCardModel, EventCardViewHolder> implements View.OnClickListener {

    private EventCardModel eventCardModel;
    private int listingIndex;

    public EventCardViewSetter(EventCardModel baseCardModel,
                               Context context,
                               IChildViewClickListener childViewClickListener,
                               int listingIndex) {
        super(baseCardModel, context, childViewClickListener);

        this.eventCardModel = baseCardModel;
        this.listingIndex = listingIndex;
    }

    @Override
    protected void setViews(EventCardViewHolder holder) {
        holder.backgroundView.setBackgroundResource(Constants.getBackgroundId(eventCardModel.getSportId()));
        holder.gameName.setText(eventCardModel.getSportName());
        holder.gameInfo.setText(getGameInfo(eventCardModel));
        holder.priceMode.setText(eventCardModel.getPriceType());
        if (eventCardModel.getEpochTime() != null) {
            holder.dateTimeView.setText(Utils.getDateTimeFormateString(1000L * Long.parseLong(eventCardModel.getEpochTime())));
        }
        holder.addressView.setText(eventCardModel.getAddress());
        holder.joinEventButton.setOnClickListener(this);
    }

    private String getGameInfo(EventCardModel eventModel) {
        return eventModel.getEventType() + " Match";
    }

    @Override
    protected View buildView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.lv_event_card_layout, null);
    }

    @Override
    protected EventCardViewHolder buildViewHolder(View cardview) {
        return new EventCardViewHolder(cardview);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.join_event_button:
                childViewClickListener.onChildViewButtonClick(IChildViewClickListener.JOIN_EVENT, eventCardModel, v);
                break;
        }
    }
}