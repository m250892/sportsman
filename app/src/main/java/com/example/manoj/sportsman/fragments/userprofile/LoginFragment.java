package com.example.manoj.sportsman.fragments.userprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.tasks.NewUserLoginTask;
import com.example.manoj.sportsman.utils.IPermissionsCallback;
import com.example.manoj.sportsman.utils.loginhelper.FbLoginHelper;
import com.example.manoj.sportsman.utils.loginhelper.GPlusLoginHelper;
import com.example.manoj.sportsman.utils.loginhelper.SignInRequestCallback;

import org.json.JSONObject;

/**
 * Created by manoj on 06/12/15.
 */
public class LoginFragment extends BaseFragment implements SignInRequestCallback, NewUserLoginTask.ServerLoginCallback {

    public static BaseFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.login_fragment_layout, container, false);
        baseView.findViewById(R.id.fb_login_button).setOnClickListener(this);
        baseView.findViewById(R.id.google_login_button).setOnClickListener(this);
        return baseView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fb_login_button:
                onFbLoginButtonClick();
                break;
            case R.id.google_login_button:
                onGPlusLoginButtonClick();
                break;
        }
    }

    private void onFbLoginButtonClick() {
        Log.d("manoj", "Fb login button click");
        FbLoginHelper.getInstance().signIn(this);
    }

    private void onGPlusLoginButtonClick() {
        Log.d("manoj", "google login button click");
        getFragmentController().requestPermission(IPermissionsCallback.GET_ACCOUNTS, new IPermissionsCallback() {
            @Override
            public void onPermissionRequestResult(int requestCode, boolean result) {
                if (result) {
                    GPlusLoginHelper.getInstance().signIn(LoginFragment.this);
                } else {
                    showToastMessage("Please provide permission for login via google!!");
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        FbLoginHelper.getInstance().unregisterCallback();
        GPlusLoginHelper.getInstance().unregisterCallback();
        super.onDestroyView();
    }


    @Override
    public void onSignInSuccess(String signInMethod, SignInResponse signInResponse) {
        Log.d("manoj", "Login Success with : " + signInMethod);
        //Log.d("manoj", "Sign data : " + signInResponse.toString());

        JSONObject jsonObject = signInResponse.getJsonObject();
        if (jsonObject != null) {
            Log.d("manoj", "Json object data : " + jsonObject);
        }
        new NewUserLoginTask(jsonObject, this).execute();
    }

    @Override
    public void onSignInFailed(String signInMethod, String message) {
        Log.d("manoj", "Login failed with : " + signInMethod + ", message: " + message);
    }

    @Override
    public void onServerLoginSuccess(SignInResponse signInResponse) {
        Log.d("manoj", "Server Login Success : " + signInResponse.toString());
        UserProfileManager.getInstance().onLoginSuccess(signInResponse);
        getActivity().finish();
        //getFragmentController().performOperation(IFragmentController.EDIT_PROFILE, null);
    }

    @Override
    public void onServerLoginFailed(String message) {
        showToastMessage("Login failed, please try again!");
    }
}
