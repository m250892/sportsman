package com.example.manoj.sportsman.filters.player;

import android.app.Activity;

import com.example.manoj.sportsman.filters.core.BaseSearchAndSelectCard;
import com.example.manoj.sportsman.filters.filterhash.PlayerFilterHash;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 08/01/16.
 */
public class PlayerGameSearch extends BaseSearchAndSelectCard<PlayerFilterHash> {

    public PlayerGameSearch(Activity activity) {
        super(activity);
    }


    public void writeToFilterHash(PlayerFilterHash filterHash) {

    }

    public void readFromFilterHash(PlayerFilterHash filterHash) {

    }

    @Override
    protected String getLabel() {
        return "GAME";
    }

    @Override
    protected List<String> allResults() {
        List<String> data = new ArrayList<>();
        data.add("Football");
        data.add("Cricket");
        data.add("BasketBall");
        data.add("Tennis");
        return data;
    }
}
