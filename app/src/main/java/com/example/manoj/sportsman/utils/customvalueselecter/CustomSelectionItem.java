package com.example.manoj.sportsman.utils.customvalueselecter;

public class CustomSelectionItem {
    public String value;
    public int id;

    public CustomSelectionItem(String value, int id) {
        this.value = value;
        this.id = id;
    }

    public boolean isSelected;

    public CustomSelectionItem(String value) {
        this.value = value;
        id = -1;
        isSelected = false;
    }

    public CustomSelectionItem(String value, boolean isSelected) {
        this.value = value;
        id = -1;
        this.isSelected = isSelected;
    }

    public CustomSelectionItem(String value, int id, boolean isSelected) {
        this.value = value;
        this.id = id;
        this.isSelected = isSelected;
    }
}