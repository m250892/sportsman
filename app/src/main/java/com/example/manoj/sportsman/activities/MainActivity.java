package com.example.manoj.sportsman.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.fragments.HomeFragment;
import com.example.manoj.sportsman.fragments.VenueDetailPage;
import com.example.manoj.sportsman.fragments.listview.CardsListHolderFragment;
import com.example.manoj.sportsman.fragments.recentevents.RecentListHolderFragment;
import com.example.manoj.sportsman.fragments.userprofile.UserProfileFragment;
import com.example.manoj.sportsman.fragments.userprofile.UserProfileManager;
import com.example.manoj.sportsman.models.listview.EventCardModel;
import com.example.manoj.sportsman.models.listview.PlayerCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.utils.EventManager;
import com.example.manoj.sportsman.utils.events.DrawerOpenEvent;

public class MainActivity extends BaseActivity
        implements OnNavigationItemSelectedListener {
    private FrameLayout frameLayout;
    private DrawerLayout drawer;
    private MenuItem navLogin;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = (FrameLayout) findViewById(R.id.mainFrameLayout);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        navLogin = menu.findItem(R.id.nav_login);
        loadInitialFragment();

    }

    @Override
    public void intigrateToolbarWithDrawer(Toolbar toolbar) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            boolean isEventSend = false;

            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (!isEventSend) {
                    isEventSend = true;
                    EventManager.getDefaultEventBus().post(new DrawerOpenEvent());
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                isEventSend = false;
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public ViewGroup getFragmentContainer() {
        return frameLayout;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // if current fragment is null, or has not handled backpress, handle activity back press
            Fragment currentFragment = getCurrentFragment();
            if (currentFragment != null && currentFragment instanceof BaseFragment) {
                if (((BaseFragment) currentFragment).handleBackPress()) {
                    return;
                }
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MainApplication.printLog("onOptionsItemSelected main Application");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {

        } else if (id == R.id.create_event) {
            launchEventCreateActivity();
        } else if (id == R.id.my_activities) {
            launchRecentActivtyList();
        } else if (id == R.id.edit_profile) {
            handleEditProfileButtonClick();
        } else if (id == R.id.vanue_detail_page) {
            openVenueDetailPage();
        } else if (id == R.id.nav_share) {
        } else if (id == R.id.nav_login) {
            if (UserProfileManager.isLoggedIn()) {
                UserProfileManager.handleLogout();
                navLogin.setTitle("Logout");
            } else {
                navLogin.setTitle("Login");
                launchUserProfileActivity();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void handleEditProfileButtonClick() {
        launchUserProfileActivity();
    }

    private void launchRecentActivtyList() {
        replaceFragmentInDefaultLayout(RecentListHolderFragment.newInstance());
    }

    private void openEventsListViewPage(Object input) {
        if (input instanceof CardsListType) {
            replaceFragmentInDefaultLayout(CardsListHolderFragment.newInstance((CardsListType) input));
        } else {
            new RuntimeException("Input should be instance of Card List Type");
        }
    }

    private void openVenueDetailPage() {
        replaceFragmentInDefaultLayout(VenueDetailPage.newInstance());
    }

    private void loadInitialFragment() {
        addFragmentInDefaultLayout(HomeFragment.newInstance(), false);
    }

    private void launchUserProfileActivity() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    private void launchEventCreateActivity() {
        Intent intent = new Intent(this, CreateNewEventActivity.class);
        startActivity(intent);
    }

    private void launchEventDetailPageActivity(EventCardModel data) {
        Intent intent = new Intent(this, EventDetailPageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(EventDetailPageActivity.EVENT_ID_KEY, data.getId());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void performOperation(int operation, Object input) {
        switch (operation) {
            case IFragmentController.EVENT_DETAIL_PAGE_FRAGMENT:
                openEventDetailPage(input);
                break;
            case IFragmentController.CREATE_NEW_EVENT:
                launchEventCreateActivity();
                break;
            case IFragmentController.EVENTS_FRAGMENT:
                openEventsListViewPage(input);
                break;
            case IFragmentController.LOGIN_SCREEN:
                launchUserProfileActivity();
                break;
            case IFragmentController.USER_DETAIL_PAGE_FRAGMENT:
                openProfileDetail(input);
                break;
            case IFragmentController.PLAYGROUND_DETAIL_PAGE_FRAGMENT:
                openVenueDetailPage();
                break;
            case IFragmentController.FILTER_FRAGMENT:

                break;

        }
    }

    private void openProfileDetail(Object input) {
        if (input instanceof PlayerCardModel) {
            String userId = ((PlayerCardModel) input).getId();
            replaceFragmentInDefaultLayout(UserProfileFragment.newInstance(userId));
        } else {
            new RuntimeException("Input should be instance of Event Model");
        }
    }

    private void openEventDetailPage(Object input) {
        Log.d("manoj", "open event detail page called");
        if (input instanceof EventCardModel) {
            launchEventDetailPageActivity((EventCardModel) input);
        } else {
            new RuntimeException("Input should be instance of Event Model");
        }
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.mainFrameLayout);
    }


}
