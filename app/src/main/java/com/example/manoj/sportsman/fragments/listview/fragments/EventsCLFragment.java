package com.example.manoj.sportsman.fragments.listview.fragments;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.fragments.userprofile.UserProfileManager;
import com.example.manoj.sportsman.models.EventJoinModel;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.tasks.core.GeneralCallTaskCallback;
import com.example.manoj.sportsman.tasks.core.PostCallAsyncTask;
import com.example.manoj.sportsman.tasks.listview.EventsCLFetcherTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manoj on 25/12/15.
 */
public class EventsCLFragment extends BaseCLFragment {

    public static EventsCLFragment newInstance() {
        return new EventsCLFragment();
    }

    private PostCallAsyncTask postCallAsyncTask;

    @Override
    public CardsListType getListType() {
        return CardsListType.EVENTS_LIST;
    }

    @Override
    public void fetchFirstPageData() {
        if (!getDataStore().isDataAvailable()) {
            new EventsCLFetcherTask(this, getListType()).execute();
        }
    }

    @Override
    public void onChildViewButtonClick(int actionId, BaseCardModel baseCardModel, View view) {
        Log.d("manoj", "onChildViewButtonClick : " + actionId);
        if (IChildViewClickListener.JOIN_EVENT == actionId) {
            if (UserProfileManager.isLoggedIn()) {
                String userId = UserProfileManager.getUserProfile().getId();
                String eventId = baseCardModel.getId();
                JSONObject data = getJoinEventJson(userId, eventId);
                if (postCallAsyncTask == null) {
                    postCallAsyncTask = new PostCallAsyncTask<>(getJoinEventUrl(), data, EventJoinModel.class, new GeneralCallTaskCallback<EventJoinModel>() {
                        @Override
                        public void onSuccess(EventJoinModel model) {
                            postCallAsyncTask = null;
                            showToastMessage("Event joined");
                        }

                        @Override
                        public void onFailure() {
                            postCallAsyncTask = null;
                            showToastMessage("Network problem, try again!!");
                        }
                    });
                    postCallAsyncTask.execute();
                }
            } else {
                getFragmentController().performOperation(IFragmentController.LOGIN_SCREEN, null);
            }
        }
    }

    private JSONObject getJoinEventJson(String userId, String eventId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userId);
            jsonObject.put("event_id", eventId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("manoj", "Item clicked at Position : " + position);
        getFragmentController().performOperation(IFragmentController.EVENT_DETAIL_PAGE_FRAGMENT, parent.getAdapter().getItem(position));
    }

    public String getJoinEventUrl() {
        String url = Constants.BASE_URL + "/create_event/join";
        return url;
    }

    @Override
    public void onDestroyView() {
        if (postCallAsyncTask != null) {
            postCallAsyncTask.unregisterCallback();
        }
        super.onDestroyView();
    }
}
