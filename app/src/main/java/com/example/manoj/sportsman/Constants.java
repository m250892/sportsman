package com.example.manoj.sportsman;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 03/11/15.
 */
public class Constants {
    public final static String APPLICATION_ID = "3W1OcLypp4WZXZ4fNwaR6vnHB6MHJTtbM6Tniu2N";
    public final static String CLIENT_ID = "bltjqtWPbEuXpOp8HEi2LKLmApmr2Ecc3sr3HxnB";

    public final static String GOOGLE_API_SERVER_KEY = "AIzaSyBIlynL43sgrlWSRJE1MOTclpUidKe8GY0";

    public static final String FB_LOGIN = "fb";
    public static final String GPLUS_LOGIN = "gplus";

    private static final String FREE = "free";
    private static final String PAID = "paid";

    public static final String BASE_URL = "http://192.168.43.91:3000";

    public static final int PRICE_TYPE_FREE = 0;
    public static final int PRICE_TYPE_PAID = 1;

    public static final int GAME_TYPE_FRIENDLY = 0;
    public static final int GAME_TYPE_PRACTICE = 1;
    public static final int GAME_TYPE_COMPETE = 2;

    public static final int GAME_LEVEL_BEGINNER = 0;
    public static final int GAME_LEVEL_INTERMEDIATE = 1;
    public static final int GAME_LEVEL_ADVANCED = 2;

    public static final int GAME_REPEAT_NONE = 0;
    public static final int GAME_REPEAT_DAILY = 1;
    public static final int GAME_REPEAT_WEEKLY = 2;

    public static final int GAME_VISIBILITY_PRIVATE = 0;
    public static final int GAME_VISIBILITY_FRIENDS = 1;
    public static final int GAME_VISIBILITY_PUBLIC = 2;

    public static final int GAME_GENDER_NONE = 0;
    public static final int GAME_GENDER_MALE = 1;
    public static final int GAME_GENDER_FEMALE = 2;

    public static final int MIN_GAME_ID = 0;
    public static final int FOOTBALL_GAME_ID = 0;
    public static final int CRICKET_GAME_ID = 1;
    public static final int BASKETBALL_GAME_ID = 2;
    public static final int TABLE_TENNIS_GAME_ID = 3;
    public static final int LAWN_TENNIS_GAME_ID = 4;
    public static final int MAX_GAME_ID = 4;

    public static final String EVENT = "event_card";
    public static final String PLAYER = "player_card";
    public static final String PLAYGROUND = "playground_card";
    public static final String COACH = "coach_card";
    public static final String EVENTS = "events_list";
    public static final String PLAYERS = "players_list";
    public static final String PLAYGROUNDS = "playgrounds_list";
    public static final String COACHES = "coaches_list";

    public static String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"};
    public static String[] daysShortNames = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    public static String[] daysNames = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public static String[] gameNames = {"Football", "Cricket", "Basketball", "Table Tennis", "Lawn Tennis"};

    //public static int[] gameCircleIcons = {R.drawable.circle_bg_colored_football_icon, R.drawable.circle_bg_colored_cricket_icon, R.drawable.circle_bg_colored_basketball_icon, R.drawable.circle_bg_colored_tt_icon, R.drawable.circle_bg_colored_lawn_tennis_icon};
    public static int[] gameCircleIcons = {R.drawable.ic_football, R.drawable.ic_cricket, R.drawable.ic_basketball, R.drawable.ic_table_tennis, R.drawable.ic_lawn_tennis};
    public static int[] gameCircleSelectedIcons = {R.drawable.ic_football_selected, R.drawable.ic_cricket_selected, R.drawable.ic_basketball_selected, R.drawable.ic_table_tennis_selected, R.drawable.ic_lawn_tennis_selected};

    public static String[] gameMode = {"Friendly", "Practice", "Compete"};

    public static String[] gameLevel = {"Beginner", "Intermediate", "Compete"};

    public static String[] repeatMode = {"None", "Daily", "Weekly"};

    public static String[] priceMode = {"Free", "Paid"};

    public static String[] genderType = {"None", "Male", "Female"};

    public static String getGameName(int id) {
        String gameName = "";
        if (id >= MIN_GAME_ID && id <= MAX_GAME_ID) {
            gameName = gameNames[id];
        }
        return gameName;
    }

    public static List<Integer> getAvailableGameIds() {
        List<Integer> availableGameIds = new ArrayList<>();
        availableGameIds.add(FOOTBALL_GAME_ID);
        availableGameIds.add(CRICKET_GAME_ID);
        availableGameIds.add(BASKETBALL_GAME_ID);
        availableGameIds.add(TABLE_TENNIS_GAME_ID);
        availableGameIds.add(LAWN_TENNIS_GAME_ID);
        return availableGameIds;
    }

    public static String getMonthName(int month) {
        if (month >= 0 && month < 12) {
            return Constants.monthNames[month];
        }
        return "";
    }

    public static int getGameCircleIconId(int id) {
        int icon = gameCircleIcons[MIN_GAME_ID];
        if (id >= MIN_GAME_ID && id <= MAX_GAME_ID) {
            icon = gameCircleIcons[id];
        }
        return icon;
    }

    public static int getGameCircleSelectedIconId(int id) {
        int icon = gameCircleSelectedIcons[MIN_GAME_ID];
        if (id >= MIN_GAME_ID && id <= MAX_GAME_ID) {
            icon = gameCircleSelectedIcons[id];
        }
        return icon;
    }

    public static String getPriceTypeName(int id) {
        String data;
        switch (id) {
            case PRICE_TYPE_PAID:
                data = FREE;
                break;
            case PRICE_TYPE_FREE:
                data = PAID;
                break;
            default:
                data = FREE;
                break;
        }
        return data;
    }

    public static String getGameMode(int id) {
        if (id >= 0 && id < 3) {
            return gameMode[id];
        }
        return "";
    }

    public static String getPriceMode(int id) {
        if (id >= 0 && id < 2) {
            return priceMode[id];
        }
        return "";
    }

    public static int getBackgroundId(int id) {
        int bg_id = R.drawable.football_card_bg;
        switch (id) {
            case Constants.FOOTBALL_GAME_ID:
                bg_id = R.drawable.football_card_bg;
                break;
            case Constants.CRICKET_GAME_ID:
                bg_id = R.drawable.cricekt_card_bg;
                break;
            case Constants.BASKETBALL_GAME_ID:
                bg_id = R.drawable.basketball_card_bg;
                break;
            case Constants.TABLE_TENNIS_GAME_ID:
                bg_id = R.drawable.tt_card_bg;
                break;
            case Constants.LAWN_TENNIS_GAME_ID:
                bg_id = R.drawable.tennis_card_bg;
                break;
        }
        return bg_id;
    }

    public static String getDayName(int day) {
        if (day >= 0 || day < 7) {
            return daysNames[day];
        }
        return null;
    }

    public static String getDayShortName(int day) {
        if (day >= 0 || day < 7) {
            return daysShortNames[day];
        }
        return null;
    }


    //Shared Prefs Key
    public static final String PREF_USER_LOGGED_IN = "is_user_logged_in";
    public static final String PREF_USER_PROFILE_JSON_STRING = "user_profile_json_string";
}
