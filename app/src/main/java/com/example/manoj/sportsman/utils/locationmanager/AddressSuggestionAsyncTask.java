package com.example.manoj.sportsman.utils.locationmanager;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;

import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by manoj on 28/11/15.
 */
public class AddressSuggestionAsyncTask extends AsyncTask<String, Void, List<SearchItemModel>> {

    private static int REQUEST_CURRENT_LOCATION_ADDRESS = 1;
    private static int REQUEST_LOCATION_SUGGESTION = 2;
    private static final int SEARCH_MAX_RESULT = 20;
    private Location location;
    private String searchText;
    private AutocompleteInterface uiCallback;
    private int requestType;

    public AddressSuggestionAsyncTask(String inputStr, AutocompleteInterface uiCallback) {
        requestType = REQUEST_LOCATION_SUGGESTION;
        this.searchText = inputStr;
        this.uiCallback = uiCallback;
    }

    public AddressSuggestionAsyncTask(Location loacation, AutocompleteInterface uiCallback) {
        requestType = REQUEST_CURRENT_LOCATION_ADDRESS;
        this.location = loacation;
        this.uiCallback = uiCallback;
    }

    @Override
    protected List doInBackground(String... params) {
        //MainApplication.printLog("Async task start for text : " + searchText);
        Geocoder geocoder = new Geocoder(MainApplication.getContext(), Locale.getDefault());
        List<Address> addresses = null;
        String errorMessage = "";
        try {
            if (requestType == REQUEST_LOCATION_SUGGESTION) {
                addresses = geocoder.getFromLocationName(searchText, SEARCH_MAX_RESULT, LocaionManagerConstants.lowerLeftLatitude, LocaionManagerConstants.lowerLeftLongitude, LocaionManagerConstants.upperRightLatitude, LocaionManagerConstants.upperRightLongitude);
            } else {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            errorMessage = "service_not_available";
            return null;
        } catch (IllegalArgumentException iae) {
            errorMessage = "invalid_lat_long_used";
            iae.printStackTrace();
            return null;
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no_address_found";
            }
            return null;
        } else {
            List<SearchItemModel> result = new ArrayList<>();
            for (int index = 0; index < addresses.size(); index++) {
                Address address = addresses.get(index);
                if (address != null && address.getLatitude() != 0 && address.getLongitude() != 0) {
                    result.add(new SearchItemModel(addresses.get(index)));
                }
            }
            return result;
        }
    }

    protected void onPostExecute(List<SearchItemModel> suggestionResult) {
        //printSearchResults(suggestionResult);
        if (uiCallback != null) {
            if (requestType == REQUEST_LOCATION_SUGGESTION) {
                uiCallback.updateAutocompleteList(suggestionResult, searchText);
            } else {
                SearchItemModel searchItemModel = null;
                if (suggestionResult != null && suggestionResult.size() > 0) {
                    searchItemModel = (SearchItemModel) suggestionResult.get(0);
                }
                uiCallback.onCurrentLocationAddessFetched(searchItemModel);
            }
        }
    }

    private void printSearchResults(List suggestionResult) {
        if (suggestionResult == null) {
            MainApplication.printLog("Search Result for text : " + searchText + " is null");
            return;
        }
        MainApplication.printLog("Search Result for text : " + searchText);
        for (int index = 0; index < suggestionResult.size(); index++) {
            MainApplication.printLog("Result Index : " + index);
            MainApplication.printLog("Full address : " + ((SearchItemModel) suggestionResult.get(index)).getAddressDetail());
            MainApplication.printLog(((SearchItemModel) suggestionResult.get(index)).toString());
        }
    }

    public void unregisterCallback() {
        uiCallback = null;
    }

    public interface AutocompleteInterface {
        void updateAutocompleteList(List newList, String searchText);

        void onCurrentLocationAddessFetched(SearchItemModel result);
    }
}
