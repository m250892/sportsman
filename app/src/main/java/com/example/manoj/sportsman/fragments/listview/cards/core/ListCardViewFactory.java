package com.example.manoj.sportsman.fragments.listview.cards.core;

import android.content.Context;

import com.example.manoj.sportsman.fragments.listview.cards.events.EventCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.players.PlayerCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.playground.PlaygroundCardViewSetter;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.listview.EventCardModel;
import com.example.manoj.sportsman.models.listview.PlayerCardModel;
import com.example.manoj.sportsman.models.listview.PlaygroundCardModel;

public class ListCardViewFactory {

    public static BaseListCardViewSetter getListCardViewSetter(BaseCardModel cardModel,
                                                               Context context,
                                                               IChildViewClickListener childViewClickListener,
                                                               int position) {
        switch (cardModel.getCardType()) {
            case EVENT_CARD:
                return new EventCardViewSetter((EventCardModel) cardModel, context, childViewClickListener, position);
            case PLAYER_CARD:
                return new PlayerCardViewSetter((PlayerCardModel) cardModel, context, childViewClickListener, position);
            case PLAYGROUND_CARD:
                return new PlaygroundCardViewSetter((PlaygroundCardModel) cardModel, context, childViewClickListener, position);
            case COACH_CARD:
                return new PlayerCardViewSetter((PlayerCardModel) cardModel, context, childViewClickListener, position);
        }
        return null;
    }


}
