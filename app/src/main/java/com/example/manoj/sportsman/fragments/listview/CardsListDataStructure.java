package com.example.manoj.sportsman.fragments.listview;

import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 20/12/15.
 */
public class CardsListDataStructure {

    private CardsListType cardsListType;
    private List<BaseCardModel> cardList;

    public CardsListDataStructure(CardsListType cardsListType) {
        this.cardsListType = cardsListType;
        this.cardList = new ArrayList<>();
    }

    public CardsListDataStructure(CardsListType cardsListType, List<BaseCardModel> cardList) {
        this.cardsListType = cardsListType;
        if (null == cardList) {
            cardList = new ArrayList<>();
        }
        this.cardList = cardList;
    }

    public CardsListType getCardsListType() {
        return cardsListType;
    }

    public List<BaseCardModel> getCardList() {
        return cardList;
    }

    public void setCardList(List<BaseCardModel> cardList) {
        this.cardList = cardList;
    }

    public void addCardsInList(List<BaseCardModel> cardList) {
        if (this.cardList == null) {
            this.cardList = new ArrayList<>();
        }
        this.cardList.addAll(cardList);
    }

    public void clearCardList() {
        if (this.cardList != null) {
            this.cardList.clear();
        }
    }

    public int getCardsCount() {
        if (null == this.cardList) {
            return 0;
        }
        return this.cardList.size();
    }

    public BaseCardModel getCard(int position) {
        return this.cardList.get(position);
    }

    public boolean isDataAvailable() {
        return getCardsCount() > 0;
    }
}
