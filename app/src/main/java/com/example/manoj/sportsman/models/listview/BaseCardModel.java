package com.example.manoj.sportsman.models.listview;

import com.example.manoj.sportsman.models.CardType;
import com.example.manoj.sportsman.utils.Model;

import java.io.Serializable;

/**
 * Created by manoj on 24/12/15.
 */
public abstract class BaseCardModel extends Model implements Serializable {

    public abstract CardType getCardType();

    public abstract String getId();
}
