package com.example.manoj.sportsman.tasks.core;

import android.os.AsyncTask;

import com.example.manoj.sportsman.utils.Model;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

/**
 * Created by manoj on 30/12/15.
 */
public class GetCallAsyncTask<M extends Model> extends AsyncTask<Void, Void, M> {

    private GeneralCallTaskCallback callback;
    private String url;
    private Class<M> responseClass;

    public GetCallAsyncTask(String url, Class<M> responseClass, GeneralCallTaskCallback callback) {
        this.callback = callback;
        this.url = url;
        this.responseClass = responseClass;
    }

    @Override
    protected M doInBackground(Void[] params) {
        try {
            ServerResponse<M> response = NetworkUtils.doGetCall(url, responseClass);
            return response.getData();
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(M model) {
        if (callback != null) {
            if (model != null) {
                callback.onSuccess(model);
            } else {
                callback.onFailure();
            }
        }
    }

    public void unregisterCallback() {
        callback = null;
    }

}
