package com.example.manoj.sportsman.models.userprofile.gplusloginresponse;

public class Image {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ClassPojo [url = " + url + "]";
    }
}

		