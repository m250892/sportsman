package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.fragments.userprofile.UserProfileManager;
import com.example.manoj.sportsman.models.EventCreateDataModel;
import com.example.manoj.sportsman.tasks.EventPostTask;
import com.example.manoj.sportsman.utils.Utils;
import com.example.manoj.sportsman.views.CustomScrollView;
import com.example.manoj.sportsman.views.RangeSeekBar;
import com.example.manoj.sportsman.views.SeekArc;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by manoj on 29/11/15.
 */
public class EventMoreEditOptionFragment extends BaseFragment implements RangeSeekBar.OnRangeSeekBarChangeListener<Integer>, CustomScrollView.OnScrollViewListener, EventPostTask.EventPostTaskCallback {
    private LinearLayout baseCardContainerView;
    private final ArrayList<Integer> intervalList = new ArrayList<>();
    private RangeSeekBar<Integer> seekBar;
    private TextView minPerson, maxPerson;
    private int LAYOUT_MARGIN = 0;
    private int MARGIN_4 = 0;
    private int MARGIN_8 = 0;
    private int MIN_PLAYER = 2;

    private int toolbarHeight;
    private int basicCardInfoHeight;

    private TextView eventDescriptionView;
    private TextView gameNameTextView;
    private TextView gameDateTimeTextView;
    private TextView gameVenueAddressTextView;

    private View basicCardLayoutView;
    private CustomScrollView customScrollView;
    private View backPressView;
    private View overlayView;
    private ImageView loactionIcon;
    private ImageView approvalRequiredIconView;
    private boolean isApprovalRequired;
    private TextView priceTextView;

    private EventPostTask eventPostTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_more_edit_option_fragment, container, false);
        toolbarHeight = getResources().getDimensionPixelSize(R.dimen.dimen_56);
        basicCardInfoHeight = getResources().getDimensionPixelSize(R.dimen.list_card_height);
        initView(baseView);
        return baseView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        EventCreateDataModel dataModel = EventCreateDataModel.getInstance();
        String eventDescription = dataModel.getEventDescription();
        if (eventDescription != null) {
            eventDescriptionView.setText(eventDescription);
        }
        int resourceId = Constants.getBackgroundId(dataModel.getGameId());
        basicCardLayoutView.setBackgroundResource(resourceId);

        if (dataModel.isGameSelected()) {
            gameNameTextView.setText(dataModel.getGameName());
        }
        if (dataModel.isDateTimeSelected()) {
            gameDateTimeTextView.setText(dataModel.getDateTimeModel().getDateTimeFormatedString());
        }
        if (dataModel.isLocalitySelected()) {
            gameVenueAddressTextView.setText(dataModel.getSearchItemModel().getAddressDetail());
        }

        int minP = dataModel.getMinPlayer() - MIN_PLAYER;
        int maxP = dataModel.getMaxPlayer() - MIN_PLAYER;
        seekBar.setSelectedMinValue(minP);
        seekBar.setSelectedMaxValue(maxP);
        onRangeSeekBarValuesChanged(seekBar, minP, maxP);
    }

    public static BaseFragment newInstance() {
        return new EventMoreEditOptionFragment();
    }

    private void initView(View baseView) {
        baseCardContainerView = (LinearLayout) baseView.findViewById(R.id.base_card_container_view);
        backPressView = baseView.findViewById(R.id.back_press);
        backPressView.setOnClickListener(this);
        gameNameTextView = (TextView) baseView.findViewById(R.id.game_name);
        gameDateTimeTextView = (TextView) baseView.findViewById(R.id.game_date_and_time);
        gameVenueAddressTextView = (TextView) baseView.findViewById(R.id.game_venue_address);
        loactionIcon = (ImageView) baseView.findViewById(R.id.location_icon);
        customScrollView = (CustomScrollView) baseView.findViewById(R.id.custom_scroll_view);
        customScrollView.setOnScrollViewListener(this);
        basicCardLayoutView = baseView.findViewById(R.id.basic_info_card);
        overlayView = baseView.findViewById(R.id.overlay_view);
        baseView.findViewById(R.id.create_event_button).setOnClickListener(this);

        LAYOUT_MARGIN = getContext().getResources()
                .getDimensionPixelSize(R.dimen.event_create_card_margin);
        MARGIN_4 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_4);
        MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        renderUI();
    }

    private void renderUI() {
        renderAddDescriptionCard();
        renderSelectMinMaxPlayerCard();
        renderPriceModeAndApporvalTypeCard();
        renderOtherInfoCard();
    }

    private void renderOtherInfoCard() {

        View otherInfo = getActivityReference().getLayoutInflater().inflate(
                R.layout.basic_card_layout, null);
        LinearLayout contentView = (LinearLayout) otherInfo.findViewById(R.id.content_view);
        contentView.addView(renderGameLevelCard());
        contentView.addView(renderGenderPrefCard());
        contentView.addView(renderGameTypeCard());
        contentView.addView(renderGameRepeatCard());
        contentView.addView(renderGameVisibilityCard());
        setViewParams(otherInfo, 0, LAYOUT_MARGIN, 0, 0);
    }

    private void renderAddDescriptionCard() {
        View basicInfo = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_more_option_add_description_card, null);
        eventDescriptionView = (TextView) basicInfo.findViewById(R.id.event_description_text_view);
        basicInfo.findViewById(R.id.add_description_card).setOnClickListener(this);
        setViewParams(basicInfo, 0, 0, 0, 0);
    }

    private void renderSelectMinMaxPlayerCard() {
        View baseView = getActivityReference().getLayoutInflater().inflate(
                R.layout.num_player_selecter_layout, null);
        minPerson = (TextView) baseView.findViewById(R.id.value_min);
        maxPerson = (TextView) baseView.findViewById(R.id.value_max);
        ViewGroup seekBarLayout = (ViewGroup) baseView.findViewById(R.id.number_of_player_selecter);
        initIntervalList();
        this.seekBar = new RangeSeekBar<>(0, this.intervalList.size() - 1, this.getActivity());
        this.seekBar.setOnRangeSeekBarChangeListener(this);
        seekBarLayout.addView(seekBar);
        setViewParams(baseView, 0, LAYOUT_MARGIN, 0, 0);
    }

    private void renderPriceModeAndApporvalTypeCard() {
        View basicInfo = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_more_option_price_and_approval_mode_card, null);

        basicInfo.findViewById(R.id.approval_required_view).setOnClickListener(this);
        approvalRequiredIconView = (ImageView) basicInfo.findViewById(R.id.approval_required_icon);
        SeekArc seekArc = (SeekArc) basicInfo.findViewById(R.id.seekArc);
        priceTextView = (TextView) basicInfo.findViewById(R.id.price_text);
        seekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                int price = getPrice(progress);
                if (price == 0) {
                    EventCreateDataModel.getInstance().setIsFree(true);
                    EventCreateDataModel.getInstance().setCostPerPerson(-1);
                    priceTextView.setText("FREE");
                } else {
                    EventCreateDataModel.getInstance().setIsFree(false);
                    EventCreateDataModel.getInstance().setCostPerPerson(price);
                    priceTextView.setText("" + price + "/person");
                }

            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        setViewParams(basicInfo, 0, LAYOUT_MARGIN, 0, 0);
    }


    private int getPrice(int progress) {
        int value = progress / 5;
        return value * 50;
    }

    private int getProgressFromPrice(int price) {
        if (price == -1) {
            return 0;
        }
        return price / 50;
    }

    private View renderGameLevelCard() {
        View baseView = getViewForSelectors("Player Level", R.array.game_level, EventCreateDataModel.getInstance().getPlayerLevel(), gameLevelSelectionCallback);
        return baseView;
        //setViewParams(baseView, 0, LAYOUT_MARGIN, 0, 0);
    }

    private View renderGameTypeCard() {
        View baseView = getViewForSelectors("Type", R.array.game_type, EventCreateDataModel.getInstance().getGameType(), gameTypeSelectionCallback);
        return baseView;
        //setViewParams(baseView, 0, 0, 0, 0);
    }


    private View renderGameRepeatCard() {
        View baseView = getViewForSelectors("Repeat", R.array.game_repeat, EventCreateDataModel.getInstance().getFrequency(), gameRepeatModeSelectionCallback);
        return baseView;
        //setViewParams(baseView, 0, 0, 0, 0);
    }

    private View renderGameVisibilityCard() {
        View baseView = getViewForSelectors("Visibility", R.array.game_visibility, EventCreateDataModel.getInstance().getVisibility(), gameVisibilitySelectionCallback);
        return baseView;
        //setViewParams(baseView, 0, 0, 0, 0);
    }

    private View renderGenderPrefCard() {
        View baseView = getViewForSelectors("Gender Preference", R.array.game_gender_type, EventCreateDataModel.getInstance().getGenderPreference(), genderPreferenceSelectionCallback);
        return baseView;
        //setViewParams(baseView, 0, 0, 0, 0);
    }

    private View getViewForSelectors(String title, int arrayId, int defaultPosition, AdapterView.OnItemSelectedListener callback) {
        View baseView = getActivityReference().getLayoutInflater().inflate(
                R.layout.value_selector_layout, null);

        TextView textView = (TextView) baseView.findViewById(R.id.template_title);
        textView.setText(title);

        Spinner spinner = (Spinner) baseView.findViewById(R.id.spinner_view);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                arrayId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(defaultPosition);
        spinner.setOnItemSelectedListener(callback);
        return baseView;
    }

    protected void setViewParams(View view, int leftMargin, int topMargin, int rightMargin,
                                 int bottomMargin) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        baseCardContainerView.addView(view, lp);
    }

    private void initIntervalList() {
        int maxValue = 10;
        int minValue = 0;
        double difference = maxValue - minValue;
        for (int iteration = 0; iteration <= difference; iteration++) {
            double value = minValue + iteration * difference;
            this.intervalList.add((int) value);
        }
    }

    @Override
    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {

        int minP = MIN_PLAYER + minValue;
        int maxP = MIN_PLAYER + maxValue;
        EventCreateDataModel.getInstance().setMinPlayer(minP);
        EventCreateDataModel.getInstance().setMaxPlayer(maxP);
        minPerson.setText("" + minP);
        maxPerson.setText("" + maxP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_description_card:
                getFragmentController().performOperation(IFragmentController.CREATE_EVENT_ADD_DESRIPTION, null);
                break;
            case R.id.back_press:
                getFragmentController().onBackPressed();
                break;
            case R.id.approval_required_view:
                handleApprovalRequiredViewClick();
                break;
            case R.id.create_event_button:
                handleCreateEventButtonClick();
                break;

        }
    }

    private void handleCreateEventButtonClick() {
        if (UserProfileManager.isLoggedIn()) {
            if (eventPostTask == null) {
                EventCreateDataModel eventCreateDataModel = EventCreateDataModel.getInstance();
                eventCreateDataModel.setUserId(UserProfileManager.getUserProfile().getId());
                JSONObject eventData = eventCreateDataModel.getJsonObject();
                eventPostTask = new EventPostTask(eventData, this);
                eventPostTask.execute();
            } else {
                showToastMessage("Already creating event, please wait!!");
            }
        } else {
            getFragmentController().performOperation(IFragmentController.LOGIN_SCREEN, null);
        }
    }

    private void handleApprovalRequiredViewClick() {
        isApprovalRequired = !isApprovalRequired;
        int resourceId;
        if (isApprovalRequired) {
            resourceId = R.drawable.ic_check_green_circle;
        } else {
            resourceId = R.drawable.ic_cross_green_circle;
        }
        EventCreateDataModel.getInstance().setIsApprovalRequired(isApprovalRequired);
        approvalRequiredIconView.setBackgroundResource(resourceId);
    }


    private AdapterView.OnItemSelectedListener gameTypeSelectionCallback = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d("manoj", "gameTypeSelectionCallback selected : " + position);
            EventCreateDataModel.getInstance().setGameType(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener gameRepeatModeSelectionCallback = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d("manoj", "gameRepeatModeSelectionCallback selected : " + position);
            EventCreateDataModel.getInstance().setFrequency(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener gameLevelSelectionCallback = new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d("manoj", "gameLevelSelectionCallback selected : " + position);
            EventCreateDataModel.getInstance().setPlayerLevel(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener gameVisibilitySelectionCallback = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d("manoj", "gameVisibilitySelectionCallback selected : " + position);
            EventCreateDataModel.getInstance().setVisibility(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener genderPreferenceSelectionCallback = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d("manoj", "genderPreferenceSelectionCallback selected : " + position);
            EventCreateDataModel.getInstance().setGenderPreference(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };


    private float getFloat(final float value, final float minValue, final float maxValue) {
        return Math.min(maxValue, Math.max(minValue, value));
    }

    @Override
    public void onScrollChanged(CustomScrollView v, int scrollX, int scrollY, int oldl, int oldt) {
        int maxScroll = basicCardInfoHeight - toolbarHeight;
        float scrollPos = getFloat(scrollY, 0, maxScroll);
        float scrollPrecent = ((float) maxScroll - scrollPos) /
                (float) maxScroll;
        float colorAlpha = (float) Math.max(0, (2 * (Math.min(1, scrollPrecent) - 0.50)));
        float bgColorAlpha = 1f - (float) Math.max(0, (2 * Math.min(0.5, scrollPrecent)));

        float halfTotal = getResources().getDimensionPixelSize(R.dimen.dimen_32);
        float titleScroll = scrollPos - (1 - scrollPrecent) * halfTotal;
        int greenFirstColor = getResources().getColor(R.color.color_primary_light);
        int whiteColor = getResources().getColor(R.color.white);

        float maxTitleScale = 0.30F;
        float titleScale = 0.7F + maxTitleScale * scrollPrecent;

        gameNameTextView.setScaleX(titleScale);
        gameNameTextView.setScaleY(titleScale);

        gameNameTextView.setTranslationY(titleScroll);
        gameDateTimeTextView.setTranslationY(titleScroll * titleScale);
        backPressView.setTranslationY(scrollPos);
        basicCardLayoutView.setTranslationY(-scrollPos);

        Drawable mDrawable = getResources().getDrawable(R.drawable.ic_map_location);
        mDrawable.setColorFilter(new PorterDuffColorFilter(Utils.getColorWithAlpha(colorAlpha, whiteColor), PorterDuff.Mode.MULTIPLY));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            loactionIcon.setBackground(mDrawable);
        } else {
            loactionIcon.setBackgroundDrawable(mDrawable);
        }

        overlayView.setBackgroundColor(Utils.getColorWithAlpha(bgColorAlpha, greenFirstColor));
        gameVenueAddressTextView.setTextColor(Utils.getColorWithAlpha(colorAlpha, whiteColor));
        gameDateTimeTextView.setTextColor(Utils.getColorWithAlpha(colorAlpha, whiteColor));
    }


    @Override
    public void onDataUploadSuccess() {
        eventPostTask = null;
        getActivityReference().finish();
    }

    @Override
    public void onDataUploadFailed() {
        eventPostTask = null;
        showToastMessage("Some error occure, please try again!!");
    }
}

