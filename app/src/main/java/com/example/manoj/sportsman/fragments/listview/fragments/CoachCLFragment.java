package com.example.manoj.sportsman.fragments.listview.fragments;

import android.view.View;
import android.widget.AdapterView;

import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.tasks.listview.CoachCLFetcherTask;

/**
 * Created by manoj on 26/12/15.
 */
public class CoachCLFragment extends BaseCLFragment {

    @Override
    public void fetchFirstPageData() {
        if (!getDataStore().isDataAvailable()) {
            new CoachCLFetcherTask(this, getListType()).execute();
        }
    }

    @Override
    public CardsListType getListType() {
        return CardsListType.COACHES_LIST;
    }

    @Override
    public void onChildViewButtonClick(int actionId, BaseCardModel eventModel, View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
