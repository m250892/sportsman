package com.example.manoj.sportsman;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.example.manoj.sportsman.utils.IPermissionsCallback;
import com.example.manoj.sportsman.utils.locationmanager.LocationChangeListener;

/**
 * Created by manoj on 30/10/15.
 */
public interface IFragmentController {

    int CREATE_EVENT_GAME_SELECTION_FRAGMENT = 1;
    int CREATE_EVENT_DATE_TIME_SELECTION_FRAGMENT = 2;
    int CREATE_EVENT_ADDRESS_SELECTION_FRAGMENT = 3;
    int CREATE_EVENT_MORE_OPTIONS_FRAGMENT = 4;
    int CREATE_EVENT_ADD_DESRIPTION = 5;

    int DATE_SELECTOR_FRAGMENT = 6;
    int TIME_SELECTOR_FRAGMENT = 7;
    int LOCATION_SELECTOR_FRAGMENT = 8;
    int EVENT_DETAIL_PAGE_FRAGMENT = 9;
    int USER_DETAIL_PAGE_FRAGMENT = 10;
    int PLAYGROUND_DETAIL_PAGE_FRAGMENT = 11;
    int FILTER_FRAGMENT = 12;


    int EDIT_PROFILE = 15;
    int CREATE_NEW_EVENT = 16;
    int EVENTS_FRAGMENT = 17;
    int LOGIN_SCREEN = 18;

    void performOperation(final int operation, Object input);

    Fragment getCurrentFragment();

    Context getBaseContext();

    void onBackPressed();

    Fragment getTopFragmentInBackStack();

    void popBackStackIfForeground();

    void popBackStack();

    boolean isForeground();

    void removeCurrentFragment();

    ActionBar getSupportActionBarM();

    void setSupportActionBarM(Toolbar toolbar);

    ViewGroup getFragmentContainer();

    void fetchCurrentLocation(LocationChangeListener callback);

    void unregisterLocationChangeListenerCallback();

    void intigrateToolbarWithDrawer(Toolbar toolbar);

    boolean checkPermissionGranted(String permission);

    void requestPermission(int requestCode, IPermissionsCallback permissionsCallback);

    void unregisterForPermissionRequest(int requestCode);

    boolean showRequestPermissionRationaleDialog(String permission);
}
