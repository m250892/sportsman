package com.example.manoj.sportsman.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 01/11/15.
 */
public class SearchDatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version

    private boolean isDBupdateInProgress = false;

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "searchManager";

    // Search table name
    private static final String TABLE_SEARCH = "search";

    private static final String KEY_ID = "id";
    private static final String KEY_COUNTRY_NAME = "country_name";
    private static final String KEY_CITY_NAME = "city_name";
    private static final String KEY_AREA_NAME = "area_name";
    private static final String KEY_LOCALITY_NAME = "locality_name";
    private static final String KEY_SUB_LOCALITY_NAME = "sub_locality_name";
    private static final String KEY_LOCALITY_TYPE = "locality_type";

    private static SearchDatabaseHandler searchDatabaseHandlerInstance;

    public static SearchDatabaseHandler getInstance() {
        return searchDatabaseHandlerInstance;
    }

    public SearchDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_SEARCH_TABLE = "CREATE TABLE " + TABLE_SEARCH + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_COUNTRY_NAME + " TEXT,"
                + KEY_CITY_NAME + " TEXT,"
                + KEY_AREA_NAME + " TEXT,"
                + KEY_LOCALITY_NAME + " TEXT,"
                + KEY_SUB_LOCALITY_NAME + " TEXT,"
                + KEY_LOCALITY_TYPE + " INTEGER" + ")";
        db.execSQL(CREATE_SEARCH_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCH);
        // Create tables again
        onCreate(db);
    }

    public void insertLocalitysInTable(JSONObject jsonObject) {
        if (jsonObject != null) {
            List<SearchItemModel> searchItemModelList = new ArrayList<>();
            try {
                JSONArray cityList = jsonObject.getJSONArray("city");
                for (int index = 0; index < cityList.length(); index++) {
                    String cityName = cityList.get(index).toString();
                    JSONArray areaList = jsonObject.getJSONArray(cityName);
                    for (int areaIndex = 0; areaIndex < areaList.length(); areaIndex++) {
                        String areaName = areaList.get(areaIndex).toString();
                        //searchItemModelList.add(new SearchItemModel(cityName, areaName));
                    }
                }

                for (SearchItemModel searchItemModel : searchItemModelList) {
                    addSearchItem(searchItemModel);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // Adding new contact
    public boolean addSearchItem(SearchItemModel searchItemModel) {
        //Log.d("nagar", searchItemModel.toString());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, getSearchCount() + 1);
        values.put(KEY_COUNTRY_NAME, searchItemModel.getCountryName());
        values.put(KEY_CITY_NAME, searchItemModel.getCityName());
        values.put(KEY_AREA_NAME, searchItemModel.getLocalityName());

        // Inserting Row
        db.insert(TABLE_SEARCH, null, values);
        db.close(); // Closing database connection
        return true;
    }

    // Getting All Contacts
    public List<SearchItemModel> getAllSearchResult() {
        List<SearchItemModel> result = new ArrayList<SearchItemModel>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SEARCH;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SearchItemModel searchItemModel = new SearchItemModel();
                //searchItemModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                searchItemModel.setCountryName(cursor.getString(cursor.getColumnIndex(KEY_COUNTRY_NAME)));
                searchItemModel.setCityName(cursor.getString(cursor.getColumnIndex(KEY_CITY_NAME)));
                searchItemModel.setLocalityName(cursor.getString(cursor.getColumnIndex(KEY_AREA_NAME)));
                /*searchItemModel.setLocalityName(cursor.getString(cursor.getColumnIndex(KEY_LOCALITY_NAME)));
                searchItemModel.setSubLocalityName(cursor.getString(cursor.getColumnIndex(KEY_SUB_LOCALITY_NAME)));
                searchItemModel.setLocalityType(cursor.getInt(cursor.getColumnIndex(KEY_LOCALITY_TYPE)));*/

                // Adding contact to list
                result.add(searchItemModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return result;
    }

    // Getting All Contacts
    public List<SearchItemModel> getSearchSuggestion(String input) {
        List<SearchItemModel> result = new ArrayList<SearchItemModel>();

        if (isDBupdateInProgress) {
            return result;
        }
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SEARCH + " WHERE ";
        String[] keyword = input.split("\\s+");
        for (int index = 0; index < keyword.length; index++) {
            if (index > 0) {
                selectQuery += " AND ";
            }
            selectQuery += "(" + KEY_CITY_NAME + " LIKE '%" + keyword[index] + "%' OR " + KEY_AREA_NAME + " LIKE '%" + keyword[index] + "%')";
        }
        selectQuery += " LIMIT 20";

        Log.d("SearchDatabaseHandler", "getSearchSuggestion query : " + selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SearchItemModel searchItemModel = new SearchItemModel();
                //searchItemModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                searchItemModel.setCountryName(cursor.getString(cursor.getColumnIndex(KEY_COUNTRY_NAME)));
                searchItemModel.setCityName(cursor.getString(cursor.getColumnIndex(KEY_CITY_NAME)));
                searchItemModel.setLocalityName(cursor.getString(cursor.getColumnIndex(KEY_AREA_NAME)));
                /*searchItemModel.setLocalityName(cursor.getString(cursor.getColumnIndex(KEY_LOCALITY_NAME)));
                searchItemModel.setSubLocalityName(cursor.getString(cursor.getColumnIndex(KEY_SUB_LOCALITY_NAME)));
                searchItemModel.setLocalityType(cursor.getInt(cursor.getColumnIndex(KEY_LOCALITY_TYPE)));*/

                // Adding contact to list
                result.add(searchItemModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return result;
    }


    // Getting search Count
    public long getSearchCount() {
        long totalSearchCount;
        String countQuery = "SELECT  * FROM " + TABLE_SEARCH;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        totalSearchCount = cursor.getCount();
        cursor.close();
        return totalSearchCount;
    }

    // Updating single search
    public int updateSearch(SearchItemModel searchItemModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COUNTRY_NAME, searchItemModel.getCountryName());
        //TODO for other values

        // updating row
        /*return db.update(TABLE_SEARCH, values, KEY_ID + " = ?",
                new String[]{String.valueOf(searchItemModel.getId())});*/
        return 0;
    }

    // Deleting single search
    public void deleteSearch(SearchItemModel searchItemModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*db.delete(TABLE_SEARCH, KEY_ID + " = ?",
                new String[]{String.valueOf(searchItemModel.getId())});
        db.close();*/
    }

    public void clearSearchTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_SEARCH);
        db.close();
    }

    public boolean isDBupdateInProgress() {
        return isDBupdateInProgress;
    }

    public void setDBupdateInProgress(boolean state) {
        this.isDBupdateInProgress = state;
    }
}
