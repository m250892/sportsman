package com.example.manoj.sportsman.filters;

import java.util.LinkedHashMap;

/**
 * Created by manoj on 05/01/16.
 */
public class FilterConstants {

    public static final LinkedHashMap<Integer, String> EVENT_GENDER_MAP
            = new LinkedHashMap<Integer, String>() {
        {
            put(1, "Male");
            put(2, "Female");
        }
    };

    public static final LinkedHashMap<Integer, String> PLAYER_GENDER_MAP
            = new LinkedHashMap<Integer, String>() {
        {
            put(1, "Male");
            put(2, "Female");
        }
    };


    public static final LinkedHashMap<Integer, String> EVENT_PRICE_MAP
            = new LinkedHashMap<Integer, String>() {
        {
            put(1, "Free");
            put(2, "Paid");
        }
    };

    public static final LinkedHashMap<Integer, String> EVENT_GAME_LEVEL_MAP
            = new LinkedHashMap<Integer, String>() {
        {
            put(1, "BEGINNER");
            put(2, "INTERMEDIATE");
            put(3, "ADVANCE");
        }
    };
}
