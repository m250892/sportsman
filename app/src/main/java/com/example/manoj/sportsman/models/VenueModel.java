package com.example.manoj.sportsman.models;

/**
 * Created by manoj on 23/11/15.
 */
public class VenueModel {
    public static String venueName = "The Thane Club";
    public static String aboutVenue = "Here is introducing a new-age lifestyle club that carves its own path, provides amenities and services that surpass your imagination and truly boasts of the high-class lifestyle. That's The Thane Club.\n" +
            "A complete one roof club with amenities unmatched by others, The Thane Club is reserved for the discerning few who believe that life is a celebration and their body is a temple which must be worshiped everyday.";
    public static String weekdaysOpenTime = "09:00am";
    public static String weekdaysCloseTime = "10:00pm";
    public static String weekendOpenTime = "11:00am";
    public static String weekendCloseTime = "06:00pm";
    public static String address = "Mohan Koppikar Road, Teen Hath Naka, Opposite Raheja Garden, Thane, Maharashtra";

    public static String getWeekdaysTiming() {
        return weekdaysOpenTime + " to " + weekdaysCloseTime;
    }

    public static String getWeekendTiming() {
        return weekendOpenTime + " to " + weekendCloseTime;
    }
}
