package com.example.manoj.sportsman.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.databasemanager.SearchDatabaseHandler;

/**
 * Created by manoj on 30/10/15.
 */
public class BaseFragment extends Fragment implements View.OnClickListener {

    protected Activity activity;
    private IFragmentController fragmentControllerActivity;
    private SearchDatabaseHandler searchDatabaseHandler;
    private ProgressDialog progressDialog;

    public Context getContext() {
        return activity;
    }

    public void setToolbar(Toolbar toolbar) {
        getFragmentController().setSupportActionBarM(toolbar);
        getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(false);
        getFragmentController().getSupportActionBarM().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = activity;
            fragmentControllerActivity = (IFragmentController) activity;

        } catch (ClassCastException cce) {
            throw new ClassCastException(
                    getName() + " Fragment's parent activity: " + activity.toString() +
                            "must implement IFragmentController");
        }
    }

    public String getName() {
        return this.getClass().getCanonicalName();
    }

    public IFragmentController getFragmentController() {
        if (fragmentControllerActivity == null) {
            fragmentControllerActivity = (IFragmentController) getActivity();
        }
        return fragmentControllerActivity;
    }

    protected Activity getActivityReference() {
        if (activity == null) {
            activity = getActivity();
        }
        return activity;
    }

    protected void hideSoftKeyBoard(EditText et) {
        if (null != et && getActivityReference() != null) {
            InputMethodManager imm = (InputMethodManager) getActivityReference()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

    protected void showSoftKeyBoard(EditText et) {
        if (null != et && getActivityReference() != null) {
            InputMethodManager imm = (InputMethodManager) getActivityReference()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public boolean isAttachedToActivity() {
        return (activity == null) ? false : true;
    }

    public boolean handleBackPress() {
        return false;
    }

    @Override
    public void onDetach() {
        this.activity = null;
        this.fragmentControllerActivity = null;
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentController().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showToastMessage(String message) {
        Toast.makeText(getActivityReference(), message, Toast.LENGTH_SHORT).show();
    }

    protected void showProgressBar() {
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(getActivityReference());
            progressDialog.setMessage("Please wait...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }
    }

    protected void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroyView() {
        hideProgressBar();
        super.onDestroyView();
    }

    public int getStatusBarHeight() {
        int statusBarHeight = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int resourceId = getActivity().getBaseContext().getResources()
                    .getIdentifier("status_bar_height", "dimen",
                            "android");
            if (resourceId > 0) {
                statusBarHeight = getActivity().getBaseContext().getResources()
                        .getDimensionPixelSize(resourceId);
            }
        } else {
            statusBarHeight = 0;
        }

        return statusBarHeight;
    }
}
