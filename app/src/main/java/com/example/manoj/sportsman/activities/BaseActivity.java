package com.example.manoj.sportsman.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.utils.IPermissionsCallback;
import com.example.manoj.sportsman.utils.SharedPrefsManager;
import com.example.manoj.sportsman.utils.locationmanager.CustomLocationManager;
import com.example.manoj.sportsman.utils.locationmanager.LocationChangeListener;

import java.util.HashMap;

/**
 * Created by manoj on 04/12/15.
 */
public abstract class BaseActivity extends AppCompatActivity implements IFragmentController, CustomLocationManager.LocationManagerCallback {

    private FragmentTransaction fragmentTransaction;
    private HashMap<Integer, IPermissionsCallback> requestCodeToCallbackMap = new HashMap<>();

    @Override
    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.mainFrameLayout);
    }

    @Override
    public Fragment getTopFragmentInBackStack() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount <= 0) {
            return null;
        }
        FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount - 1);
        String fragmentTag = backStackEntry.getName();
        if (TextUtils.isEmpty(fragmentTag)) {
            throw new IllegalStateException("Fragment added without a tag");
        }
        return getSupportFragmentManager().findFragmentByTag(fragmentTag);
    }

    @Override
    public void popBackStackIfForeground() {
        if (getSupportFragmentManager() != null && isForeground()) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void popBackStack() {
        if (getSupportFragmentManager() != null) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean isForeground() {
        return false;
    }

    @Override
    public void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = getCurrentFragment();
        if (currentFrag != null) {
            transaction.remove(currentFrag);
        }
        transaction.commit();
    }

    @Override
    public ActionBar getSupportActionBarM() {
        return getSupportActionBar();
    }

    @Override
    public void setSupportActionBarM(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }


    @Override
    public void fetchCurrentLocation(LocationChangeListener callback) {

    }

    @Override
    public void unregisterLocationChangeListenerCallback() {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
        // Allow state loss by default
        addFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                           boolean addToBackStack) {
        // Allow state loss by default
        addFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack,
                                           boolean allowStateLoss) {
        if (!getSupportFragmentManager().isDestroyed()) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction
                    .add(R.id.mainFrameLayout, fragmentToBeLoaded, fragmentToBeLoaded.getName());
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
            }

            Fragment currFragment = getCurrentFragment();
            if (null != currFragment) {
                currFragment.onPause();
            }
            if (allowStateLoss) {
                fragmentTransaction.commitAllowingStateLoss();
            } else {
                fragmentTransaction.commit();
            }
        }
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
        // Allow state loss by default
        replaceFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                               boolean addToBackStack) {
        // Allow state loss by default
        replaceFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                               boolean addToBackStack, boolean allowStateLoss) {
        if (!getSupportFragmentManager().isDestroyed()) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
            fragmentTransaction.replace(R.id.mainFrameLayout, fragmentToBeLoaded,
                    fragmentToBeLoaded.getName());
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
            }
            if (allowStateLoss) {
                fragmentTransaction.commitAllowingStateLoss();
            } else {
                fragmentTransaction.commit();
            }
        }
    }

    protected int getStatusBarHeight() {
        int statusBarHeight = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
        }
        return statusBarHeight;
    }

    public void intigrateToolbarWithDrawer(Toolbar toolbar) {
        //implement this method for add icon in toolbar to open drawer
    }

    public void clearBackStack(boolean isInclusive) {
        if (getSupportFragmentManager() != null) {
            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            if (backStackEntryCount <= 0) {
                return;
            }
            FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager()
                    .getBackStackEntryAt(0);
            String fragmentTag = backStackEntry.getName();
            if (isInclusive) {
                getSupportFragmentManager()
                        .popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else {
                getSupportFragmentManager().popBackStack(fragmentTag, 0);
            }
        }
    }

    public void requestPermission(int requestCode, IPermissionsCallback permissionsCallback) {

        String permission = null;
        switch (requestCode) {
            case IPermissionsCallback.GET_ACCOUNTS:
                permission = Manifest.permission.GET_ACCOUNTS;
                break;
            case IPermissionsCallback.GET_LOCATION:
                permission = Manifest.permission.ACCESS_FINE_LOCATION;
                break;
            default:
                new RuntimeException("Permission not find in callbacks");
        }

        if (checkPermissionGranted(permission)) {
            permissionsCallback.onPermissionRequestResult(requestCode, true);
        } else {
            requestCodeToCallbackMap.put(requestCode, permissionsCallback);
            SharedPrefsManager.getInstance().setBoolean(permission, true);
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    public boolean checkPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public void unregisterForPermissionRequest(int requestCode) {
        requestCodeToCallbackMap.remove(requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        IPermissionsCallback permissionsCallback = requestCodeToCallbackMap.get(requestCode);
        if (permissionsCallback != null) {
            unregisterForPermissionRequest(requestCode);
            permissionsCallback.onPermissionRequestResult(requestCode, grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean showRequestPermissionRationaleDialog(String permission) {
        if (checkPermissionGranted(permission)) {
            return false;
        }
        boolean haveAskedBefore = SharedPrefsManager.getInstance().getBoolean(permission);
        boolean showRationale = ActivityCompat
                .shouldShowRequestPermissionRationale(this, permission);

        return haveAskedBefore && !showRationale;
    }

}
