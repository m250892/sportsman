package com.example.manoj.sportsman.filters.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 03/01/16.
 */
public abstract class BaseSearchAndSelectAdpater extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<String> resultList;

    public BaseSearchAndSelectAdpater(Context mContext) {
        this.mContext = mContext;
        this.resultList = searchResult(null);
    }

    @Override
    public int getCount() {
        return Math.min(getMaxResult(), resultList.size());
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(resultList.get(position));
        return convertView;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                String inputString = null;
                if (constraint != null) {
                    inputString = constraint.toString();
                }
                List<String> searchResult = searchResult(inputString);
                // Assign the data to the FilterResults
                filterResults.values = searchResult;
                filterResults.count = searchResult.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    setResultList((List<String>) results.values);
                }
            }
        };
        return filter;
    }

    public void setResultList(List<String> resultList) {
        if (resultList == null) {
            resultList = new ArrayList<>();
        }
        this.resultList = resultList;
        notifyDataSetChanged();
    }

    abstract int getMaxResult();

    abstract List<String> searchResult(String searchText);
}
