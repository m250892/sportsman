package com.example.manoj.sportsman.fragments.userprofile;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.dialogs.DialogCallbacks;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;
import com.example.manoj.sportsman.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by manoj on 31/12/15.
 */
public abstract class BaseProfileFragment extends BaseFragment implements DialogCallbacks {

    private View baseView;
    protected ViewGroup horizontalView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        baseView = inflater.inflate(getLayoutId(), container, false);
        initViews(baseView);
        return baseView;
    }

    protected abstract void initViews(View baseView);

    protected abstract int getLayoutId();

    protected abstract SignInResponse getProfileData();

    protected View getBaseView() {
        return baseView;
    }

    protected void refreshFavGameCard() {
        if (getProfileData() == null) return;

        List<UserFavSportModel> favSports = getProfileData().getFavSports();
        horizontalView.removeAllViews();
        if (Utils.isCollectionFilled(favSports)) {
            for (int index = 0; index < favSports.size(); index++) {
                horizontalView.addView(getFavGameCard(favSports.get(index)));
            }
        }
    }

    protected void initBasicProfileViews(View baseView) {
        if (getProfileData() == null) return;
        TextView nameView = (TextView) baseView.findViewById(R.id.name_textview);
        TextView genderAgeView = (TextView) baseView.findViewById(R.id.gender_age_textview);
        TextView remarksView = (TextView) baseView.findViewById(R.id.remarke_textview);
        ImageView profileImage = (ImageView) baseView.findViewById(R.id.profile_image);

        if (getProfileData().getName() != null) {
            nameView.setText(getProfileData().getName());
        }

        genderAgeView.setText(getProfileData().getGenderAgeString());
        Uri uri = Uri.parse(getProfileData().getProfilePicUrl());
        Log.d("manoj", "profile pic url : " + uri);
        Picasso.with(getContext()).load(uri).into(profileImage);
        remarksView.setVisibility(View.GONE);
    }

    protected void initFavGameCard(View baseView) {
        horizontalView = (ViewGroup) baseView.findViewById(R.id.user_fav_game_view);
    }

    protected View getFavGameCard(final UserFavSportModel userFavSportModel) {
        View singleGameView = getActivityReference().getLayoutInflater().inflate(
                R.layout.user_profile_single_game_view_layout, null);
        ImageView gameIcon = (ImageView) singleGameView.findViewById(R.id.game_icon);
        TextView gameName = (TextView) singleGameView.findViewById(R.id.game_name);
        RatingBar ratingBar = (RatingBar) singleGameView.findViewById(R.id.game_rating);

        gameIcon.setImageResource(Constants.getGameCircleIconId(userFavSportModel.getGameId()));
        gameName.setText(userFavSportModel.getGameName());
        ratingBar.setRating(userFavSportModel.getRating());
        ratingBar.setIsIndicator(userFavSportModel.isEditable());
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d("manoj", "Rating bar change : " + rating);
                userFavSportModel.setRating(rating);
            }
        });
        return singleGameView;
    }

    @Override
    public void onDateSelected(int requestCode, int year, int month, int day) {

    }

    @Override
    public void onTimeSelected(int requestCode, int hourOfDay, int minute) {

    }

    @Override
    public void onGameSelected(int gameId) {

    }
}
