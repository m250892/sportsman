package com.example.manoj.sportsman.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.example.manoj.sportsman.R;

import java.util.Calendar;

/**
 * Created by manoj on 26/10/15.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public static final int START_DATE = 1;
    public static final int END_DATE = 2;
    private DialogCallbacks callback;
    private int requestCode;

    public DatePickerFragment(DialogCallbacks callback, int requestCode) {
        this.callback = callback;
        this.requestCode = requestCode;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DateTimePickerTheme, this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(c.getTime().getTime() - 1000);
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (callback != null) {
            callback.onDateSelected(requestCode, year, month, day);
        }
    }
}