package com.example.manoj.sportsman.utils.customvalueselecter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manoj.sportsman.R;

import java.util.ArrayList;

/**
 * Created by manoj on 26/11/15.
 */
public class CustomSelectorAdapter extends RecyclerView.Adapter<CustomSelectorAdapter.ViewHolder> {
    private ArrayList<CustomSelectionItem> mDataset;
    private Context mContext;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;

        public ViewHolder(View baseView) {
            super(baseView);
            mTextView = (TextView) baseView.findViewById(R.id.text_view);
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomSelectorAdapter(Context mContext, ArrayList<CustomSelectionItem> myDataset) {
        this.mContext = mContext;
        this.mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomSelectorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.date_selecter_single_item, parent, false));
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        CustomSelectionItem customSelectionItem = this.mDataset.get(position);
        holder.mTextView.setText(customSelectionItem.value);
        int textColor = this.mContext.getResources().getColor(R.color.white);
        float textSize = 20.0F;
        float textAlpha = 0.87F;
        if (customSelectionItem.isSelected) {
            textColor = this.mContext.getResources().getColor(R.color.black);
            textSize = 32.0F;
            textAlpha = 1.0F;
        }
        holder.mTextView.setTextColor(textColor);
        holder.mTextView.setTextSize(textSize);
        holder.mTextView.setAlpha(textAlpha);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
