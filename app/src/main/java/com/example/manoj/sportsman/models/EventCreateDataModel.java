package com.example.manoj.sportsman.models;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manoj on 27/11/15.
 */
public class EventCreateDataModel {

    public static EventCreateDataModel eventCreateDataModel;

    private String userId;
    private int gameId;
    private String gameName;
    private DateTimeModel dateTimeModel;
    private SearchItemModel searchItemModel;

    private String eventDescription;
    private int minPlayer;
    private int maxPlayer;
    private boolean isFree;
    private int costPerPerson;
    private boolean isApprovalRequired;

    private int playerLevel;
    private int gameType;
    private int frequency;
    private int visibility;
    private int genderPreference;

    private EventCreateDataModel() {
        gameId = -1;
        gameName = null;
        playerLevel = Constants.GAME_LEVEL_INTERMEDIATE;
        gameType = Constants.GAME_TYPE_FRIENDLY;
        frequency = Constants.GAME_REPEAT_NONE;
        visibility = Constants.GAME_VISIBILITY_PUBLIC;
        genderPreference = Constants.GAME_GENDER_NONE;
        dateTimeModel = new DateTimeModel();
        minPlayer = 2;
        maxPlayer = 12;
        costPerPerson = -1;
        isFree = true;
        isApprovalRequired = false;
    }

    public static synchronized EventCreateDataModel getInstance() {
        if (eventCreateDataModel == null) {
            eventCreateDataModel = new EventCreateDataModel();
        }
        return eventCreateDataModel;
    }

    public boolean isGameSelected() {
        if (gameId == -1) {
            return false;
        }
        return true;
    }

    public boolean isDateTimeSelected() {
        if (dateTimeModel == null || dateTimeModel.isDateSelected() == false || dateTimeModel.isTimeSelected() == false) {
            return false;
        }
        return true;
    }

    public boolean isLocalitySelected() {
        if (searchItemModel != null) {
            return true;
        }
        return false;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public DateTimeModel getDateTimeModel() {
        return dateTimeModel;
    }

    public void setDateTimeModel(DateTimeModel dateTimeModel) {
        this.dateTimeModel = dateTimeModel;
    }

    public SearchItemModel getSearchItemModel() {
        return searchItemModel;
    }

    public void setSearchItemModel(SearchItemModel searchItemModel) {
        this.searchItemModel = searchItemModel;
    }

    public int getPlayerLevel() {
        return playerLevel;
    }

    public void setPlayerLevel(int playerLevel) {
        this.playerLevel = playerLevel;
    }

    public int getGameType() {
        return gameType;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int getMinPlayer() {
        return minPlayer;
    }

    public void setMinPlayer(int minPlayer) {
        this.minPlayer = minPlayer;
    }

    public int getMaxPlayer() {
        return maxPlayer;
    }

    public void setMaxPlayer(int maxPlayer) {
        this.maxPlayer = maxPlayer;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = isFree;
    }

    public int getCostPerPerson() {
        return costPerPerson;
    }

    public void setCostPerPerson(int costPerPerson) {
        this.costPerPerson = costPerPerson;
    }

    public boolean isApprovalRequired() {
        return isApprovalRequired;
    }

    public void setIsApprovalRequired(boolean isApprovalRequired) {
        this.isApprovalRequired = isApprovalRequired;
    }

    public int getGenderPreference() {
        return genderPreference;
    }

    public void setGenderPreference(int genderPreference) {
        this.genderPreference = genderPreference;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("user_id", getUserId());

            if (isGameSelected()) {
                jsonObject.put("sport_id", getGameId());
            }

            if (isDateTimeSelected()) {
                jsonObject.put("epochtime", dateTimeModel.getEpochTimeInSec());
            }

            if (getEventDescription() != null) {
                jsonObject.put("description", eventDescription);
            }

            if (isLocalitySelected()) {
                jsonObject.put("address", searchItemModel.getJsonObject());
            }

            jsonObject.put("min_player", getMinPlayer());
            jsonObject.put("max_player", getMaxPlayer());

            jsonObject.put("is_free", isFree());
            jsonObject.put("cost_per_person", getCostPerPerson());


            jsonObject.put("level", getPlayerLevel());
            jsonObject.put("event_type", getGameType());
            jsonObject.put("frequency", getFrequency());
            jsonObject.put("gender_preference", getGenderPreference());
            jsonObject.put("visibility", getVisibility());
            jsonObject.put("approval_require", isApprovalRequired());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
