package com.example.manoj.sportsman.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

/**
 * Created by manoj on 30/10/15.
 */
public class HomeFragment extends BaseFragment {

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.home_fragment_layout, container, false);
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        getFragmentController().intigrateToolbarWithDrawer(toolbar);

        baseView.findViewById(R.id.search_game).setOnClickListener(this);
        baseView.findViewById(R.id.search_sportground).setOnClickListener(this);
        baseView.findViewById(R.id.search_player).setOnClickListener(this);
        return baseView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_game:
                onEventSearchClick();
                break;
            case R.id.search_sportground:
                onSportGroundSearchClick();
                break;
            case R.id.search_player:
                onPlayerSearchClick();
                break;
        }
    }

    private void onEventSearchClick() {
        Log.d("manoj", "onEventSearchClick called");
        getFragmentController().performOperation(IFragmentController.EVENTS_FRAGMENT, CardsListType.EVENTS_LIST);
    }

    private void onSportGroundSearchClick() {
        Log.d("manoj", "onSportGroundSearchClick called");
        getFragmentController().performOperation(IFragmentController.EVENTS_FRAGMENT, CardsListType.PLAYGROUNDS_LIST);
    }

    private void onCreateNewGameClick() {
        Log.d("manoj", "onCreateNewGameClick called");
        getFragmentController().performOperation(IFragmentController.CREATE_NEW_EVENT, null);
    }

    private void onPlayerSearchClick() {
        Log.d("manoj", "onPlayerSearchClick called");
        getFragmentController().performOperation(IFragmentController.EVENTS_FRAGMENT, CardsListType.PLAYERS_LIST);
    }

    private void onCoachSearchClick() {
        Log.d("manoj", "onCoachSearchClick called");
        showToastMessage("Currently Not Available");
    }
}
