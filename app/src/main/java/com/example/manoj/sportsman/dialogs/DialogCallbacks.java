package com.example.manoj.sportsman.dialogs;

/**
 * Created by manoj on 31/10/15.
 */
public interface DialogCallbacks {

    void onDateSelected(int requestCode, int year, int month, int day);

    void onTimeSelected(int requestCode, int hourOfDay, int minute);

    void onGameSelected(int gameId);
}
