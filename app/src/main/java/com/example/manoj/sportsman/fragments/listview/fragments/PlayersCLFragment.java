package com.example.manoj.sportsman.fragments.listview.fragments;

import android.view.View;
import android.widget.AdapterView;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.tasks.listview.PlayersCLFetcherTask;

/**
 * Created by manoj on 25/12/15.
 */
public class PlayersCLFragment extends BaseCLFragment {

    public static PlayersCLFragment newInstance() {
        return new PlayersCLFragment();
    }

    @Override
    public void fetchFirstPageData() {
        if (!getDataStore().isDataAvailable()) {
            new PlayersCLFetcherTask(this, getListType()).execute();
        }
    }

    @Override
    public CardsListType getListType() {
        return CardsListType.PLAYERS_LIST;
    }

    @Override
    public void onChildViewButtonClick(int actionId, BaseCardModel eventModel, View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getFragmentController().performOperation(IFragmentController.USER_DETAIL_PAGE_FRAGMENT, parent.getAdapter().getItem(position));
    }


}
