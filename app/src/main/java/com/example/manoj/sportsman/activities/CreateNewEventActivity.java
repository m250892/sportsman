package com.example.manoj.sportsman.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.eventpostingflow.EventAddDesriptionFragment;
import com.example.manoj.sportsman.fragments.eventpostingflow.EventAddressSelectionFragment;
import com.example.manoj.sportsman.fragments.eventpostingflow.EventDateTimeSelectionFragment;
import com.example.manoj.sportsman.fragments.eventpostingflow.EventMoreEditOptionFragment;
import com.example.manoj.sportsman.fragments.eventpostingflow.GameSelectionFragment;
import com.example.manoj.sportsman.fragments.eventpostingflow.LocationSelectionFragment;
import com.example.manoj.sportsman.utils.Utils;
import com.example.manoj.sportsman.utils.locationmanager.CustomLocationManager;
import com.example.manoj.sportsman.utils.locationmanager.LocationChangeListener;

public class CreateNewEventActivity extends BaseActivity {

    private FrameLayout frameLayout;
    private LocationChangeListener locationChangeListener;
    protected CustomLocationManager customLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_event);
        customLocationManager = new CustomLocationManager(this, this);
        frameLayout = (FrameLayout) findViewById(R.id.mainFrameLayout);
        loadInitialFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (customLocationManager != null) {
            customLocationManager.connectGoogleApiClient();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == customLocationManager.REQUEST_RESOLVE_ERROR) {
            customLocationManager.mResolvingError = false;
            if (resultCode == RESULT_OK) {
                customLocationManager.connectGoogleApiClient();
            }
        }
    }

    @Override
    public void performOperation(int operation, Object input) {
        switch (operation) {
            case IFragmentController.CREATE_EVENT_GAME_SELECTION_FRAGMENT:
                replaceFragmentInDefaultLayout(GameSelectionFragment.newInstance());
                break;
            case IFragmentController.CREATE_EVENT_DATE_TIME_SELECTION_FRAGMENT:
                replaceFragmentInDefaultLayout(EventDateTimeSelectionFragment.newInstance());
                break;
            case IFragmentController.CREATE_EVENT_ADDRESS_SELECTION_FRAGMENT:
                replaceFragmentInDefaultLayout(EventAddressSelectionFragment.newInstance());
                break;
            case IFragmentController.CREATE_EVENT_MORE_OPTIONS_FRAGMENT:
                replaceFragmentInDefaultLayout(EventMoreEditOptionFragment.newInstance());
                break;
            case IFragmentController.CREATE_EVENT_ADD_DESRIPTION:
                replaceFragmentInDefaultLayout(EventAddDesriptionFragment.newInstance());
                break;
            case IFragmentController.LOCATION_SELECTOR_FRAGMENT:
                replaceFragmentInDefaultLayout(LocationSelectionFragment.newInstance());
                break;
            case IFragmentController.LOGIN_SCREEN:
                launchUserProfileActivity();
                break;
        }
    }

    private void launchUserProfileActivity() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }


    @Override
    public void fetchCurrentLocation(LocationChangeListener callback) {
        if (Utils.isGPSEnabled()) {
            locationChangeListener = callback;
            customLocationManager.startLocationUpdates();
        } else {
            callback.onLocationChange(null);
            showSettingsAlert();
        }
    }

    private void loadInitialFragment() {
        addFragmentInDefaultLayout(GameSelectionFragment.newInstance(), false);
    }

    @Override
    public ViewGroup getFragmentContainer() {
        return frameLayout;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationChangeListener != null) {
            locationChangeListener.onLocationChange(location);
            locationChangeListener = null;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public void unregisterLocationChangeListenerCallback() {
        locationChangeListener = null;
    }

    @Override
    protected void onStop() {
        if (customLocationManager != null) {
            customLocationManager.disconnectGoogleApiClient();
        }
        super.onStop();
    }
}
