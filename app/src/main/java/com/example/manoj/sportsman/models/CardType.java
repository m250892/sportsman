package com.example.manoj.sportsman.models;

import com.example.manoj.sportsman.Constants;

/**
 * Created by manoj on 24/12/15.
 */
public enum CardType {
    EVENT_CARD(Constants.EVENT),
    PLAYER_CARD(Constants.PLAYER),
    PLAYGROUND_CARD(Constants.PLAYGROUND),
    COACH_CARD(Constants.COACH);

    private String text;

    CardType(String text) {
        this.text = text;
    }

    public String getName() {
        return text;
    }

    public static CardType fromString(String text) {
        if (text != null) {
            for (CardType cardType : CardType.values()) {
                if (text.equalsIgnoreCase(cardType.text)) {
                    return cardType;
                }
            }
        }
        return null;
    }
}

