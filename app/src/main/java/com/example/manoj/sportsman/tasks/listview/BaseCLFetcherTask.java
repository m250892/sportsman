package com.example.manoj.sportsman.tasks.listview;

import android.os.AsyncTask;

import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.utils.NetworkUtils;

import java.util.List;

/**
 * Created by manoj on 03/11/15.
 */
public abstract class BaseCLFetcherTask extends AsyncTask<Void, Void, List<BaseCardModel>> {

    private FetchCardListTaskCallback callback;
    private CardsListType cardsListType;

    public BaseCLFetcherTask(FetchCardListTaskCallback callback, CardsListType cardsListType) {
        this.callback = callback;
        this.cardsListType = cardsListType;
    }

    @Override
    protected List<BaseCardModel> doInBackground(Void... params) {
        try {
            return fetchData(getBaseRequestUrl());
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(List<BaseCardModel> result) {
        if (callback != null) {
            if (result != null) {
                callback.onCLDataFetchSuccess(result);
            } else {
                callback.onCLDataFetchFailure();
            }
        }
    }


    public interface FetchCardListTaskCallback {
        void onCLDataFetchSuccess(List<BaseCardModel> data);

        void onCLDataFetchFailure();
    }

    protected abstract String getBaseRequestUrl();

    protected abstract List<BaseCardModel> fetchData(String flatDetailsRequest) throws Exception;

    protected class CardListResponseWrapper<BCM extends BaseCardModel> {
        public List<BCM> data;

        public List<BCM> getObjectList() {
            return data;
        }
    }

}
