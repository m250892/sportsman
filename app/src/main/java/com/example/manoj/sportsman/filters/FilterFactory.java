package com.example.manoj.sportsman.filters;

import android.app.Activity;

import com.example.manoj.sportsman.filters.core.BaseFilterGroup;
import com.example.manoj.sportsman.filters.event.EventGameSearch;
import com.example.manoj.sportsman.filters.event.EventGenderFilters;
import com.example.manoj.sportsman.filters.event.EventLevelFilters;
import com.example.manoj.sportsman.filters.event.EventPriceFilters;
import com.example.manoj.sportsman.filters.player.PlayerGameSearch;
import com.example.manoj.sportsman.filters.player.PlayerGender;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 07/01/16.
 */
public class FilterFactory {

    public static List<BaseFilterGroup> getFilters(Activity activity, CardsListType cardsListType) {
        switch (cardsListType) {
            case EVENTS_LIST:
                return getEventListFilters(activity);
            case PLAYERS_LIST:
                return getPlayersFilters(activity);
            case PLAYGROUNDS_LIST:
                return getPlaygroundFilters(activity);
            case COACHES_LIST:
                return getCoachFilters(activity);
        }
        return null;
    }

    private static List<BaseFilterGroup> getEventListFilters(Activity activity) {
        List<BaseFilterGroup> filterGroups = new ArrayList<>();
        List<FilterType> filterTypes = getEventFilterTypes();
        for (FilterType item : filterTypes) {
            filterGroups.add(getEventFilterView(activity, item));
        }
        return filterGroups;
    }

    private static List<BaseFilterGroup> getPlayersFilters(Activity activity) {
        List<BaseFilterGroup> filterGroups = new ArrayList<>();
        List<FilterType> filterTypes = getPlayerFilterTypes();
        for (FilterType item : filterTypes) {
            filterGroups.add(getPlayerFilterView(activity, item));
        }
        return filterGroups;
    }

    private static List<BaseFilterGroup> getPlaygroundFilters(Activity activity) {
        List<BaseFilterGroup> filterGroups = new ArrayList<>();
        return filterGroups;
    }

    private static List<BaseFilterGroup> getCoachFilters(Activity activity) {
        List<BaseFilterGroup> filterGroups = new ArrayList<>();
        return filterGroups;
    }


    private static List<FilterType> getEventFilterTypes() {
        List<FilterType> filterTypes = new ArrayList<>();
        filterTypes.add(FilterType.EVENT_GAME);
        filterTypes.add(FilterType.EVENT_GENDER);
        filterTypes.add(FilterType.EVENT_LEVEL);
        filterTypes.add(FilterType.EVENT_PRICE);
        return filterTypes;
    }

    private static List<FilterType> getPlayerFilterTypes() {
        List<FilterType> filterTypes = new ArrayList<>();
        filterTypes.add(FilterType.PLAYER_GAME);
        filterTypes.add(FilterType.PLAYER_GENDER);
        return filterTypes;
    }


    private static BaseFilterGroup getEventFilterView(Activity activity, FilterType filterType) {
        switch (filterType) {
            case EVENT_GAME:
                return new EventGameSearch(activity);
            case EVENT_GENDER:
                return new EventGenderFilters(activity);
            case EVENT_LEVEL:
                return new EventLevelFilters(activity);
            case EVENT_PRICE:
                return new EventPriceFilters(activity);
        }
        return null;
    }

    private static BaseFilterGroup getPlayerFilterView(Activity activity, FilterType filterType) {
        switch (filterType) {
            case PLAYER_GAME:
                return new PlayerGameSearch(activity);
            case PLAYER_GENDER:
                return new PlayerGender(activity);
        }
        return null;
    }

}
