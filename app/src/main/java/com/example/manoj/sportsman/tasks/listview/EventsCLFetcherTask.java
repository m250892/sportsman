package com.example.manoj.sportsman.tasks.listview;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.listview.EventCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 25/12/15.
 */
public class EventsCLFetcherTask extends BaseCLFetcherTask {

    public EventsCLFetcherTask(FetchCardListTaskCallback callback, CardsListType cardsListType) {
        super(callback, cardsListType);
    }

    @Override
    protected String getBaseRequestUrl() {
        String url = Constants.BASE_URL + "/create_event/get";
        return url;
    }

    @Override
    protected List<BaseCardModel> fetchData(String flatDetailsRequest) throws Exception {
        ServerResponse<EventsCLResponseWrapper> serverResponse = NetworkUtils
                .doGetCall(flatDetailsRequest, EventsCLResponseWrapper.class);
        return parseResponse(serverResponse);
    }

    private List<BaseCardModel> parseResponse(ServerResponse<EventsCLResponseWrapper> serverResponse) {
        if (null != serverResponse) {
            EventsCLResponseWrapper data = serverResponse.getData();
            List<BaseCardModel> result = new ArrayList<>();
            result.addAll(data.getObjectList());
            return result;
        }
        return null;
    }

    private class EventsCLResponseWrapper extends CardListResponseWrapper<EventCardModel> {
    }

}
