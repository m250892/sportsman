package com.example.manoj.sportsman.utils.locationmanager;

import android.location.Location;

/**
 * Created by manoj on 28/11/15.
 */
public interface LocationChangeListener {
    void onLocationChange(Location location);
}
