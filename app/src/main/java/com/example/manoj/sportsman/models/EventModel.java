package com.example.manoj.sportsman.models;

import android.net.Uri;
import android.util.Log;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.utils.Model;

import java.io.Serializable;

/**
 * Created by manoj on 01/11/15.
 */
public class EventModel extends Model implements Serializable {
    private String id;
    private String sport_name;
    private int sport_id;
    private String description;
    private String epochtime;
    private SearchItemModel address;

    private int min_player;
    private int max_player;
    private int level;
    private int event_type;
    private int visibilty;
    private int frequency;
    private int gender_preference;
    private boolean is_free;
    private int cost_per_person;
    private boolean approval_require;

    public String getId() {
        return id;
    }

    public String getSportName() {
        return sport_name;
    }

    public int getSportId() {
        return sport_id;
    }

    public String getDescription() {
        return description;
    }

    public String getEventEpochTime() {
        return epochtime;
    }

    public String getAddress() {
        String result = "";
        if (address != null) {
            result = address.getAddressDetail();
        }
        return result;
    }

    public int getMinPlayer() {
        return min_player;
    }

    public int getMaxPlayer() {
        return max_player;
    }

    public String getEventLevel() {
        return MainApplication.getContext().getResources().getStringArray(R.array.game_level)[level];

    }

    public String getEventType() {
        return MainApplication.getContext().getResources().getStringArray(R.array.game_type)[event_type];
    }

    public String getEventVisibilty() {
        return MainApplication.getContext().getResources().getStringArray(R.array.game_visibility)[visibilty];
    }

    public String getRepeatMode() {
        return MainApplication.getContext().getResources().getStringArray(R.array.game_repeat)[frequency];
    }

    public String getPriceType() {
        int position;
        if (is_free) {
            position = 0;
        } else {
            position = 1;
        }
        return MainApplication.getContext().getResources().getStringArray(R.array.price_type)[position];
    }

    public String getCostPerPersonString() {
        String result;
        if (is_free) {
            result = MainApplication.getContext().getResources().getStringArray(R.array.price_type)[0];
        } else {
            result = MainApplication.getContext().getResources().getString(R.string.rupee_symbol) + getCostPerPerson() + "/person";
        }
        return result;
    }

    public boolean isFree() {
        return is_free;
    }

    public int getCostPerPerson() {
        return cost_per_person;
    }

    public boolean isJoinApprovalRequire() {
        return approval_require;
    }

    public void printData() {
        Log.d("EventModel", "Object ID : " + id + ", title : " + sport_name);
    }

    public String getDuration() {
        return "2 hour 30 min";
    }


    public String getStaticMapUrl() {
        String baseUrl = "https://maps.googleapis.com/maps/api/staticmap";
        Uri.Builder builder = Uri.parse(baseUrl).buildUpon();
        builder.appendQueryParameter("center",
                getLatLongString());
        builder.appendQueryParameter("zoom", "14");
        builder.appendQueryParameter("scale", "2");
        builder.appendQueryParameter("size", "600x300");
        builder.appendQueryParameter("maptype", "roadmap");
        builder.appendQueryParameter("key", Constants.GOOGLE_API_SERVER_KEY);
        builder.appendQueryParameter("markers", "size:large|color:red|" + getLatLongString());
        return builder.build().toString();
    }

    public String getLatLongString() {
        return address.getLatitude() + "," + address.getLongitude();
    }
}
