package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.adapters.SearchAutoCompleteAdapter;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;
import com.example.manoj.sportsman.utils.locationmanager.AddressSuggestionRequestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 28/11/15.
 */
public class LocationSelectionFragment extends BaseFragment implements AdapterView.OnItemClickListener, AddressSuggestionRequestController.LocationResquestControllerCallback {

    private View crossButton;
    private EditText searchInputView;
    private ListView autoCompleteListView;
    private SearchAutoCompleteAdapter adapter;

    public static LocationSelectionFragment newInstance() {
        return new LocationSelectionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.location_selection_fragment_layout, container, false);
        initViews(baseView);
        setListener();
        return baseView;
    }

    @Override
    public void onResume() {
        super.onResume();
        searchInputView.requestFocus();
        showSoftKeyBoard(searchInputView);
    }

    private void initViews(View baseView) {
        searchInputView = (EditText) baseView.findViewById(R.id.search_input_view);
        autoCompleteListView = (ListView) baseView.findViewById(R.id.auto_complete_list_view);
        crossButton = baseView.findViewById(R.id.cross_button);
        baseView.findViewById(R.id.back_press).setOnClickListener(this);
        crossButton.setOnClickListener(this);
        adapter = new SearchAutoCompleteAdapter(getContext(), new ArrayList<SearchItemModel>());
        autoCompleteListView.setAdapter(adapter);
        autoCompleteListView.setOnItemClickListener(this);
    }

    private void setListener() {
        searchInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onSearchTextChange(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onSearchTextChange(String userTypeStr) {
        String inputString = userTypeStr.toString();
        if (inputString.trim().isEmpty()) {
            crossButton.setVisibility(View.INVISIBLE);
            onSuggestionLoaded(null, "");
            return;
        } else {
            crossButton.setVisibility(View.VISIBLE);
        }
        refreshList(inputString);
    }

    private void refreshList(final String input) {
        String inputString = input;
        if (TextUtils.isEmpty(inputString)) {
            crossButton.setVisibility(View.GONE);
            onSuggestionLoaded(null, "");
            return;
        } else {
            inputString = inputString.toLowerCase();
            crossButton.setVisibility(View.VISIBLE);
            AddressSuggestionRequestController.getInstance().getSuggestionAreas(inputString, this);
        }
    }

    private void showRecentSearch() {
        if (adapter != null) {
            adapter.updateListItems(new ArrayList<SearchItemModel>());
        }
    }

    private void updateSearchAdapter(List dataList) {
        if (adapter != null) {
            adapter.updateListItems(dataList);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cross_button:
                searchInputView.setText("");
                searchInputView.requestFocus();
                onSuggestionLoaded(null, "");
                break;
            case R.id.back_press:
                getFragmentController().onBackPressed();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SearchItemModel searchItemModel = adapter.getItem(position);
        hideSoftKeyBoard(searchInputView);
        EventCreateDataModel.getInstance().setSearchItemModel(searchItemModel);
        getFragmentController().onBackPressed();
    }

    @Override
    public void onSuggestionLoaded(List results, String searchText) {
        MainApplication.printLog("onSuggestionLoaded for text " + searchText);
        Editable inputString = searchInputView.getText();
        if (TextUtils.isEmpty(inputString.toString())) {
            showRecentSearch();
        } else if (inputString.toString().equalsIgnoreCase(searchText)) {
            updateSearchAdapter(results);
        } else {
            //The result may be for a text which was entered earlier,
            // In that case just ignore.
        }
    }

    @Override
    public void onCurrentLocationAddressLoaded(SearchItemModel searchItemModel) {

    }

    @Override
    public void onDestroyView() {
        hideSoftKeyBoard(searchInputView);
        super.onDestroyView();
    }
}
