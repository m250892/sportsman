package com.example.manoj.sportsman.databasemanager;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

/**
 * Created by manoj on 01/11/15.
 */
public class DBUpdateTask extends AsyncTask {

    private SearchDatabaseHandler searchDatabaseHandler;
    private JSONObject jsonObject;
    private DBUpdateCallback callback;

    public DBUpdateTask(SearchDatabaseHandler searchDatabaseHandler, JSONObject jsonObject, DBUpdateCallback callback) {
        this.searchDatabaseHandler = searchDatabaseHandler;
        this.jsonObject = jsonObject;
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        Log.d("manoj", "DB clear start");
        searchDatabaseHandler.clearSearchTable();
        Log.d("manoj", "DB clear end");
        searchDatabaseHandler.insertLocalitysInTable(jsonObject);
        Log.d("manoj", "DB insert end");
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if (callback != null) {
            callback.onDBUpdateFinish();
        }
    }

    public interface DBUpdateCallback {
        void onDBUpdateFinish();
    }
}
