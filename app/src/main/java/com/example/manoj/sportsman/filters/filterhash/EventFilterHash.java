package com.example.manoj.sportsman.filters.filterhash;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by manoj on 07/01/16.
 */
public class EventFilterHash extends BaseFilterHash {

    private ArrayList<String> genderTypes = new ArrayList<>();

    private ArrayList<String> eventLevels = new ArrayList<>();

    private ArrayList<String> priceTypes = new ArrayList<>();


    public EventFilterHash() {
    }

    public void reset() {
        genderTypes.clear();
        eventLevels.clear();
        priceTypes.clear();
    }

    public void printFilters() {
        Log.d("manoj", "Gender : " + genderTypes.toString() + ",Level : " + eventLevels.toString() + ",Price : " + priceTypes.toString());
    }

    public ArrayList<String> getGenderTypes() {
        return genderTypes;
    }

    public void setGenderTypes(ArrayList<String> genderTypes) {
        this.genderTypes = genderTypes;
    }

    public ArrayList<String> getEventLevels() {
        return eventLevels;
    }

    public void setEventLevels(ArrayList<String> eventLevels) {
        this.eventLevels = eventLevels;
    }

    public ArrayList<String> getPriceTypes() {
        return priceTypes;
    }

    public void setPriceTypes(ArrayList<String> priceTypes) {
        this.priceTypes = priceTypes;
    }
}
