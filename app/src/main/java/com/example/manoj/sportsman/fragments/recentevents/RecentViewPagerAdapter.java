package com.example.manoj.sportsman.fragments.recentevents;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 31/12/15.
 */
public class RecentViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> pageTitles = new ArrayList<>();

    public RecentViewPagerAdapter(FragmentManager manager) {
        super(manager);
        pageTitles.add("All Events");
        pageTitles.add("Upcomming");
        pageTitles.add("Completed");
    }


    @Override
    public Fragment getItem(int position) {
        return MyRecentEvents.newInstance();
    }

    @Override
    public int getCount() {
        return pageTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles.get(position);
    }
}