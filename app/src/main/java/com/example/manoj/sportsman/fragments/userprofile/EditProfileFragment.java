package com.example.manoj.sportsman.fragments.userprofile;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.dialogs.GameSelecter;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;
import com.example.manoj.sportsman.tasks.core.GeneralCallTaskCallback;
import com.example.manoj.sportsman.tasks.core.PostCallAsyncTask;
import com.example.manoj.sportsman.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 09/12/15.
 */
public class EditProfileFragment extends BaseProfileFragment {

    protected ScrollView scrollView;
    protected TextView tvPhoneNumber;
    protected TextView tvCollageName;
    protected AutoCompleteTextView tvCurrentLocation;
    private SignInResponse oldProfileData;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.user_profile_fragment_layout;
    }

    @Override
    protected SignInResponse getProfileData() {
        return UserProfileManager.getInstance().getUserProfile();
    }

    protected void initViews(View baseView) {
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        toolbar.setTitle(getToolbarTitle());
        scrollView = (ScrollView) baseView.findViewById(R.id.scroll_view);

        initBasicProfileViews(baseView);
        initFavGameCard(baseView);
        initDetailInputCard(baseView);
        baseView.findViewById(R.id.save_button).setOnClickListener(this);

        try {
            oldProfileData = (SignInResponse) UserProfileManager.getUserProfile().clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private void initDetailInputCard(View baseView) {
        tvPhoneNumber = (TextView) baseView.findViewById(R.id.phone_number);
        tvCollageName = (TextView) baseView.findViewById(R.id.collage_name);
        tvCurrentLocation = (AutoCompleteTextView) baseView.findViewById(R.id.current_location);

        UPLocationSuggestionAdpater adapter =
                new UPLocationSuggestionAdpater(getContext());
        tvCurrentLocation.setAdapter(adapter);
        tvCurrentLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("manoj", "OnItem Clicked at position : " + position);
                SearchItemModel searchItemModel = (SearchItemModel) parent.getAdapter().getItem(position);
                tvCurrentLocation.setText(searchItemModel.getAddressDetail());
                UserProfileManager.getUserProfile().setCurrentLocation(searchItemModel);
            }
        });

        SignInResponse signInResponse = UserProfileManager.getInstance().getUserProfile();
        if (null != signInResponse.getPhoneNumber()) {
            tvPhoneNumber.setText(signInResponse.getPhoneNumber());
        }
        if (null != signInResponse.getCollageName()) {
            tvCollageName.setText(signInResponse.getCollageName());
        }
        if (null != signInResponse.getCurrentLocation()) {
            tvCurrentLocation.setText(signInResponse.getCurrentLocation().getAddressDetail());
        }
        baseView.findViewById(R.id.choose_current_location_button).setOnClickListener(this);
    }

    protected void initFavGameCard(View baseView) {
        super.initFavGameCard(baseView);
        baseView.findViewById(R.id.add_fav_sport).setOnClickListener(this);
        refreshFavGameCard();
    }


    @Override
    public void onResume() {
        super.onResume();
        scrollView.smoothScrollTo(0, 0);
    }

    protected String getToolbarTitle() {
        return "Edit Profile";
    }

    private void onProfileSaveButtonClick() {
        SignInResponse signInResponse = UserProfileManager.getInstance().getUserProfile();
        CharSequence phoneNumber = tvPhoneNumber.getText();
        CharSequence collageName = tvCollageName.getText();
        String newPhoneNumber = null != phoneNumber ? phoneNumber.toString() : null;
        String newCollageName = null != collageName ? collageName.toString() : null;
        signInResponse.setPhoneNumber(newPhoneNumber);
        signInResponse.setCollageName(newCollageName);
        JSONObject modifiedData = getProfileChangesJson();
        if (modifiedData != null) {
            PostCallAsyncTask<SignInResponse> task = new PostCallAsyncTask<>(getProfileUpdateUrl(), modifiedData, SignInResponse.class, new GeneralCallTaskCallback<SignInResponse>() {
                @Override
                public void onSuccess(SignInResponse model) {
                    getActivity().finish();
                }

                @Override
                public void onFailure() {
                    showToastMessage("Some Problem Occure please try again!!");
                }
            });
            task.execute();
        } else {
            getActivity().finish();
        }
    }

    private JSONObject getProfileChangesJson() {
        JSONObject result = new JSONObject();
        SignInResponse newProfileData = UserProfileManager.getUserProfile();

        if (!Utils.compareString(oldProfileData.getPhoneNumber(), newProfileData.getPhoneNumber())) {
            try {
                result.put("mobile_number", newProfileData.getPhoneNumber());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!Utils.compareString(oldProfileData.getCollageName(), newProfileData.getCollageName())) {
            try {
                result.put("occupation", newProfileData.getCollageName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if ((oldProfileData.getCurrentLocation() == null && newProfileData.getCurrentLocation() != null) ||
                (oldProfileData.getCurrentLocation() != null && !oldProfileData.getCurrentLocation().compare(newProfileData.getCurrentLocation()))) {
            try {
                result.put("address", newProfileData.getCurrentLocation().getJsonObject());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        List<UserFavSportModel> newAddedGames = new ArrayList<>();
        if (!oldProfileData.getFavSports().containsAll(newProfileData.getFavSports())) {
            for (UserFavSportModel newGame : newProfileData.getFavSports()) {
                if (!oldProfileData.getFavSports().contains(newGame)) {
                    newAddedGames.add(newGame);
                }
            }
        }

        if (Utils.isCollectionFilled(newAddedGames)) {
            try {
                JSONArray jsonArray = new JSONArray((new Gson()).toJson(newAddedGames));
                result.put("sport_details", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (result != null) {
            try {
                result.put("id", UserProfileManager.getUserProfile().getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private void openGameSelecterDialog() {
        GameSelecter gameSelecter = new GameSelecter(this);
        gameSelecter.setContext(getContext());
        gameSelecter.show(getChildFragmentManager(), gameSelecter.getClass().getCanonicalName());
    }

    public String getProfileUpdateUrl() {
        return Constants.BASE_URL + "/login/update";
    }

    @Override
    public void onGameSelected(int gameId) {
        Log.d("manoj", "game selecred : " + gameId);
        UserProfileManager.getUserProfile().addFavSports(new UserFavSportModel(gameId));
        refreshFavGameCard();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_fav_sport:
                openGameSelecterDialog();
                break;
            case R.id.choose_current_location_button:
                //TODO: fetch current location and set
                break;
            case R.id.save_button:
                onProfileSaveButtonClick();
                break;
        }
    }
}
