package com.example.manoj.sportsman.fragments.listview.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.adapters.CardsListAdapter;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.fragments.listview.CardsListDataStore;
import com.example.manoj.sportsman.fragments.listview.CardsListDataStructure;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.tasks.listview.BaseCLFetcherTask;

import java.util.List;

/**
 * Created by manoj on 03/11/15.
 * name of this fragment is BaseCardsListFragment
 */
public abstract class BaseCLFragment extends BaseFragment implements BaseCLFetcherTask.FetchCardListTaskCallback, AdapterView.OnItemClickListener, IChildViewClickListener {

    private static final String GAME_ID_KEY = "game_id_key";
    private ListView listView;
    private View zeroResultView;
    private View retryView;
    private int currentVisibilityState = 1; //0->zero result, 1->list visible, 2->error
    private CardsListAdapter listAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_cards_list_layout, container, false);
        initViews(baseView);
        return baseView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fetchFirstPageData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void initViews(View baseView) {
        listAdapter = new CardsListAdapter(getActivityReference(), getListType(), this);
        listView = (ListView) baseView.findViewById(R.id.list_view);
        zeroResultView = baseView.findViewById(R.id.empty_layout);
        retryView = baseView.findViewById(R.id.retry_layout);
        baseView.findViewById(R.id.retry_button).setOnClickListener(this);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
    }

    private void showZeroResultView() {
        if (currentVisibilityState != 0) {
            listView.setVisibility(View.GONE);
            zeroResultView.setVisibility(View.VISIBLE);
            currentVisibilityState = 0;
        }
    }

    private void showRetryView() {
        if (currentVisibilityState != 2) {
            listView.setVisibility(View.GONE);
            zeroResultView.setVisibility(View.GONE);
            retryView.setVisibility(View.VISIBLE);
            currentVisibilityState = 2;
        }
    }

    private void showListView() {
        if (currentVisibilityState != 1) {
            currentVisibilityState = 1;
            listView.setVisibility(View.VISIBLE);
            zeroResultView.setVisibility(View.GONE);
            retryView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCLDataFetchSuccess(List<BaseCardModel> data) {
        Log.d("manoj", "EventsDataFetchSuccess : " + data.size());
        hideProgressBar();
        getDataStore().addCardsInList(data);

        if (getDataStore().isDataAvailable()) {
            showListView();
            if (listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }
        } else {
            showZeroResultView();
        }
    }

    @Override
    public void onCLDataFetchFailure() {
        hideProgressBar();
        if (!getDataStore().isDataAvailable()) {
            showRetryView();
        } else {
            //nothing do, some data available
        }
    }

    protected CardsListDataStructure getDataStore() {
        return CardsListDataStore.getInstance().getGameDataStructure(getListType());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry_button:
                Log.d("manoj", "Retry button click");
                fetchFirstPageData();
                break;
        }
    }

    public abstract void fetchFirstPageData();

    public abstract CardsListType getListType();
}
