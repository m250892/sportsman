package com.example.manoj.sportsman.fragments.listview.cards.core;

import android.view.View;

import com.example.manoj.sportsman.models.listview.BaseCardModel;

/**
 * Created by manoj on 23/12/15.
 */
public interface IChildViewClickListener {
    int JOIN_EVENT = 1;

    void onChildViewButtonClick(int actionId, BaseCardModel eventModel, View view);
}
