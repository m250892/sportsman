package com.example.manoj.sportsman.utils;

/**
 * Created by manoj on 23/12/15.
 */
public interface IPermissionsCallback {
    int GET_ACCOUNTS = 1;
    int GET_LOCATION = 2;

    void onPermissionRequestResult(int requestCode, boolean result);
}
