package com.example.manoj.sportsman.tasks;

import android.os.AsyncTask;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

import org.json.JSONObject;

/**
 * Created by manoj on 01/11/15.
 */
public class EventPostTask extends AsyncTask {

    private JSONObject jsonData;
    private EventPostTaskCallback callback;

    public EventPostTask(JSONObject jsonData, EventPostTaskCallback callback) {
        this.jsonData = jsonData;
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            ServerResponse response = NetworkUtils.doPostCall(getUrl(), getJsonData(), null);
            return response;
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if (callback != null) {
            callback.onDataUploadSuccess();
        }
    }

    public String getUrl() {
        String url = Constants.BASE_URL + "/create_event/create";
        return url;
    }

    public JSONObject getJsonData() {
        return jsonData;
    }

    public interface EventPostTaskCallback {
        void onDataUploadSuccess();

        void onDataUploadFailed();
    }

}

