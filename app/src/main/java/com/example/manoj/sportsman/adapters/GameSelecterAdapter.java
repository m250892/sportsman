package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;

import java.util.List;

/**
 * Created by manoj on 28/10/15.
 */
public class GameSelecterAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Integer> gameIds;

    public GameSelecterAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        gameIds = Constants.getAvailableGameIds();
    }

    @Override
    public int getCount() {
        return gameIds.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.single_game_view_layout, null);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.game_image);
        TextView textView = (TextView) convertView.findViewById(R.id.game_name);
        imageView.setBackgroundResource(Constants.getGameCircleIconId(gameIds.get(position)));
        textView.setText(Constants.getGameName(gameIds.get(position)));
        return convertView;
    }
}
