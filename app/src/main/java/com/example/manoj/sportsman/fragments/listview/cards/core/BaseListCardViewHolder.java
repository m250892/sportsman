package com.example.manoj.sportsman.fragments.listview.cards.core;

import android.view.View;

/**
 * Created by manoj on 24/12/15.
 */
public abstract class BaseListCardViewHolder {
    private View baseView;

    public BaseListCardViewHolder(View baseView) {
        this.baseView = baseView;
    }

    public View getBaseView() {
        return baseView;
    }
}
