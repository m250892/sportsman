package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.utils.customvalueselecter.CustomSelectionItem;

import java.util.List;

/**
 * Created by manoj on 01/12/15.
 */
public class CenterLockGalleryAdapter extends BaseAdapter {
    private List<CustomSelectionItem> mDataset;
    private Context mContext;
    private LayoutInflater inflater;

    private int colorUnselected;
    private int colorSelected;
    private int margin_8 = 0;

    public CenterLockGalleryAdapter(Context mContext, List<CustomSelectionItem> mDataset) {
        this.mDataset = mDataset;
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        colorSelected = mContext.getResources().getColor(R.color.color_primary_second);
        colorUnselected = mContext.getResources().getColor(R.color.black_57pc);
        margin_8 = (int) mContext.getResources().getDimension(R.dimen.dimen_12);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.center_lock_gallery_single_item, parent, false);
            convertView.setTag(new HolderView(convertView));
        }
        HolderView holderView = (HolderView) convertView.getTag();
        setViews(holderView, mDataset.get(position));
        return convertView;
    }

    private void setViews(HolderView holderView, CustomSelectionItem customSelectionItem) {
        holderView.textView.setText(customSelectionItem.value);
        if (customSelectionItem.isSelected) {
            holderView.textView.setScaleX(1.5f);
            holderView.textView.setScaleY(1.5f);
            holderView.textView.setPadding(holderView.textView.getPaddingLeft(), holderView.textView.getPaddingTop() + margin_8, holderView.textView.getPaddingRight(), holderView.textView.getPaddingBottom());
            holderView.textView.setTextColor(colorSelected);
        } else {
            holderView.textView.setScaleX(0.9f);
            holderView.textView.setScaleY(0.9f);
            holderView.textView.setPadding(holderView.textView.getPaddingLeft(), holderView.textView.getPaddingTop() - margin_8, holderView.textView.getPaddingRight(), holderView.textView.getPaddingBottom());
            holderView.textView.setTextColor(colorUnselected);
        }
    }

    public class HolderView {

        public TextView textView;

        public HolderView(View baseView) {
            textView = (TextView) baseView.findViewById(R.id.text_view);
        }
    }
}
