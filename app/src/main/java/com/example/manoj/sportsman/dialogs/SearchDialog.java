package com.example.manoj.sportsman.dialogs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.adapters.SearchAutoCompleteAdapter;
import com.example.manoj.sportsman.databasemanager.SearchDatabaseHandler;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by manoj on 31/10/15.
 */
public class SearchDialog extends DialogFragment implements AdapterView.OnItemClickListener {

    private EditText searchInputView;
    private ListView autoCompleteListView;
    private SearchAutoCompleteAdapter adapter;
    private Context context;
    private SearchDatabaseHandler searchDatabaseHandler;
    private Activity activity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        this.context = activity.getApplicationContext();
        searchDatabaseHandler = new SearchDatabaseHandler(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.search_fragment_layout, container, false);
        initViews(baseView);
        setListener();
        return baseView;
    }

    private void setListener() {
        searchInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null) {
                    onSearchTextChange(s.toString());
                } else {
                    showRecentSearch();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void showRecentSearch() {
        if (adapter != null) {
            adapter.updateListItems(new ArrayList<SearchItemModel>());
        }
    }

    private void onSearchTextChange(String inputString) {
        if (adapter != null) {
            Log.d("nagar", " onSearchTextChange " + inputString);
            List<SearchItemModel> newList = getSearchDatabaseHandler().getSearchSuggestion(inputString);
            Log.d("nagar", " newList " + newList);
            adapter.updateListItems(newList);
        }
    }

    private void initViews(View baseView) {
        searchInputView = (EditText) baseView.findViewById(R.id.search_input_view);
        autoCompleteListView = (ListView) baseView.findViewById(R.id.auto_complete_list_view);
        adapter = new SearchAutoCompleteAdapter(context, new ArrayList<SearchItemModel>());
        autoCompleteListView.setAdapter(adapter);
        autoCompleteListView.setOnItemClickListener(this);
    }


    public SearchDatabaseHandler getSearchDatabaseHandler() {
        return searchDatabaseHandler;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SearchItemModel searchItemModel = adapter.getItem(position);
        hideSoftKeyBoard(searchInputView);
        if (getParentFragment() instanceof ISearchAutoCompleteSelect) {
            ((ISearchAutoCompleteSelect) getParentFragment())
                    .onAutoCompleteItemSelect(searchItemModel);
        }
        this.dismiss();
    }

    protected void hideSoftKeyBoard(EditText et) {
        if (null != et && context != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

}
