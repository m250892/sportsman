package com.example.manoj.sportsman.utils.customvalueselecter;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;

public class CenterLockListener extends OnScrollListener {
    int SCREEN_CENTER;
    Activity activity;
    boolean autoSet;
    private CustomSelecterCallbacks mCallback;
    int position;
    View view;

    public CenterLockListener(Activity activity, CustomSelecterCallbacks customSelecterCallbacks) {
        this.autoSet = true;
        this.activity = activity;
        this.mCallback = customSelecterCallbacks;
    }

    public CenterLockListener(Activity activity, int i, CustomSelecterCallbacks customSelecterCallbacks) {
        this(activity, customSelecterCallbacks);
        this.SCREEN_CENTER = i;
    }

    public CenterLockListener(int i) {
        this.autoSet = true;
        this.SCREEN_CENTER = i;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        super.onScrollStateChanged(recyclerView, i);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (this.SCREEN_CENTER == 0) {
            this.SCREEN_CENTER = linearLayoutManager.getOrientation() == 0 ? recyclerView.getLeft() + recyclerView.getRight() : recyclerView.getTop() + recyclerView.getBottom();
        }
        if (!this.autoSet && i == 0) {
            this.view = findCenterView(linearLayoutManager);
            int left = (linearLayoutManager.getOrientation() == 0 ? (this.view.getLeft() + this.view.getRight()) / 2 : (this.view.getTop() + this.view.getBottom()) / 2) - this.SCREEN_CENTER;
            if (linearLayoutManager.getOrientation() == 0) {
                recyclerView.smoothScrollBy(left, 0);
            } else {
                recyclerView.smoothScrollBy(0, left);
            }
            this.autoSet = true;
            if (this.mCallback != null) {
                this.mCallback.handlePositionChanged(this.position, recyclerView.getId());
            }
        }
        if (i == 1 || i == 2) {
            this.autoSet = false;
        }
    }

    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        super.onScrolled(recyclerView, i, i2);
    }

    private View findCenterView(LinearLayoutManager linearLayoutManager) {
        this.position = -1;
        int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
        int i = 0;
        Object obj = 1;
        View view = null;
        while (findFirstVisibleItemPosition <= linearLayoutManager.findLastVisibleItemPosition()) {
            int i2;
            View findViewByPosition = linearLayoutManager.findViewByPosition(findFirstVisibleItemPosition);
            int abs = Math.abs(this.SCREEN_CENTER - (linearLayoutManager.getOrientation() == 0 ? (findViewByPosition.getLeft() + findViewByPosition.getRight()) / 2 : (findViewByPosition.getTop() + findViewByPosition.getBottom()) / 2));
            if (abs <= i || findFirstVisibleItemPosition == linearLayoutManager.findFirstVisibleItemPosition()) {
                this.position = findFirstVisibleItemPosition;
                view = findViewByPosition;
                i2 = abs;
            } else {
                obj = null;
                i2 = i;
            }
            findFirstVisibleItemPosition++;
            i = i2;
        }
        return view;
    }

    public String getViewText() {
        return "manoj";
    }

    public int getSelectedPosition() {
        return this.position;
    }
}
