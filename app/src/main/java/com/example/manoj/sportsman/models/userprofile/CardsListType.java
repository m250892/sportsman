package com.example.manoj.sportsman.models.userprofile;

import com.example.manoj.sportsman.Constants;

/**
 * Created by manoj on 25/12/15.
 */
public enum CardsListType {
    EVENTS_LIST(Constants.EVENTS),
    PLAYERS_LIST(Constants.PLAYERS),
    PLAYGROUNDS_LIST(Constants.PLAYGROUNDS),
    COACHES_LIST(Constants.COACHES);

    private String text;

    CardsListType(String text) {
        this.text = text;
    }

    public String getName() {
        return text;
    }

    public static CardsListType fromString(String text) {
        if (text != null) {
            for (CardsListType cardsListType : CardsListType.values()) {
                if (text.equalsIgnoreCase(cardsListType.text)) {
                    return cardsListType;
                }
            }
        }
        return null;
    }
}
