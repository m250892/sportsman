package com.example.manoj.sportsman.fragments.listview.cards.events;

import android.view.View;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewHolder;

/**
 * Created by manoj on 24/12/15.
 */
public class EventCardViewHolder extends BaseListCardViewHolder {
    public View backgroundView;
    public TextView priceMode;
    public TextView gameName;
    public TextView gameInfo;
    public TextView dateTimeView;
    public TextView addressView;
    public View joinEventButton;

    public EventCardViewHolder(View baseView) {
        super(baseView);
        backgroundView = baseView.findViewById(R.id.card_bg);
        priceMode = (TextView) baseView.findViewById(R.id.price_mode);
        gameName = (TextView) baseView.findViewById(R.id.game_name);
        gameInfo = (TextView) baseView.findViewById(R.id.game_info);
        dateTimeView = (TextView) baseView.findViewById(R.id.game_date_and_time);
        addressView = (TextView) baseView.findViewById(R.id.game_venue_address);
        joinEventButton = baseView.findViewById(R.id.join_event_button);
    }
}
