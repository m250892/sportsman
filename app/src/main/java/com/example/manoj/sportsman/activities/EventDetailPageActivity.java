package com.example.manoj.sportsman.activities;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.EventDetailPage;
import com.example.manoj.sportsman.models.EventModel;
import com.example.manoj.sportsman.tasks.core.GeneralCallTaskCallback;
import com.example.manoj.sportsman.tasks.core.GetCallAsyncTask;

/**
 * Created by manoj on 20/12/15.
 */
public class EventDetailPageActivity extends BaseActivity {

    private FrameLayout frameLayout;
    public static final String EVENT_ID_KEY = "event_id_key";
    private EventModel eventModel;
    private String eventId;
    private GetCallAsyncTask<EventModel> eventGetDetailTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        frameLayout = (FrameLayout) findViewById(R.id.mainFrameLayout);
        Bundle bundle = getIntent().getExtras();
        eventId = bundle.getString(EVENT_ID_KEY);
        fetchEventData();
    }

    private void fetchEventData() {
        if (eventGetDetailTask == null) {
            eventGetDetailTask = new GetCallAsyncTask<>(getUrl(), EventModel.class, new GeneralCallTaskCallback<EventModel>() {
                @Override
                public void onSuccess(EventModel model) {
                    eventModel = model;
                    eventGetDetailTask = null;
                    loadInitialFragment();
                }

                @Override
                public void onFailure() {
                    eventGetDetailTask = null;
                }
            });
            eventGetDetailTask.execute();
        }
    }

    public String getUrl() {
        return Constants.BASE_URL + "/create_event/event?id=" + eventId;
    }

    private void loadInitialFragment() {
        addFragmentInDefaultLayout(EventDetailPage.newInstance(eventModel), false);
    }

    @Override
    public void performOperation(int operation, Object input) {

    }

    @Override
    public ViewGroup getFragmentContainer() {
        return frameLayout;
    }

    @Override
    protected void onStop() {
        if (eventGetDetailTask != null) {
            eventGetDetailTask.unregisterCallback();
        }
        super.onStop();
    }
}
