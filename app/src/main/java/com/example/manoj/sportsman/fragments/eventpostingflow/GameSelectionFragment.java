package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;

/**
 * Created by manoj on 26/11/15.
 */
public class GameSelectionFragment extends BaseFragment {

    private static int MARGIN_16 = 0;
    private ViewGroup horizontalView;
    private View navigationNextButton;
    private int selectedGameID = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        selectedGameID = EventCreateDataModel.getInstance().getGameId();
        View baseView = inflater.inflate(R.layout.game_selection_fragment_layout, container, false);
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        horizontalView = (ViewGroup) baseView.findViewById(R.id.horizontal_view);
        navigationNextButton = baseView.findViewById(R.id.navigation_next_button);
        navigationNextButton.setOnClickListener(this);
        MARGIN_16 = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        insertGames();
        return baseView;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.navigation_next_button:
                launchNextFragment();
                break;
        }
    }

    private void launchNextFragment() {
        if (EventCreateDataModel.getInstance().isGameSelected()) {
            getFragmentController().performOperation(IFragmentController.CREATE_EVENT_DATE_TIME_SELECTION_FRAGMENT, null);
        } else {
            showToastMessage("please select a game!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MainApplication.printLog("Selected Game ID  : " + selectedGameID);
    }


    public static GameSelectionFragment newInstance() {
        return new GameSelectionFragment();
    }

    private void insertGames() {
        int totalCount = Constants.getAvailableGameIds().size();
        int index = 0;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                (float) 1.0);

        while (index < totalCount) {
            LinearLayout linearLayout = getBlankLinearLayout();
            int gamesInSingleColumn = 0;
            for (; index < totalCount && gamesInSingleColumn < 2;
                 index++, gamesInSingleColumn++) {
                View childLayout = getGameCard(Constants.getAvailableGameIds().get(index));
                if (gamesInSingleColumn == 1) {
                    childLayout.setPadding(childLayout.getPaddingLeft(), childLayout.getPaddingTop() + MARGIN_16, childLayout.getPaddingRight(), childLayout.getPaddingBottom());
                }
                if (index == 0) {
                    linearLayout.setPadding(linearLayout.getPaddingLeft() + MARGIN_16, linearLayout.getPaddingTop(), linearLayout.getPaddingRight(), linearLayout.getPaddingBottom());
                }
                linearLayout.addView(childLayout, params);
            }
            horizontalView.addView(linearLayout);
        }
    }

    private LinearLayout getBlankLinearLayout() {
        LinearLayout linearLayout = new LinearLayout(getActivityReference());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        return linearLayout;
    }

    private View getGameCard(final int gameId) {
        View childLayout = LayoutInflater.from(getActivityReference()).inflate(
                R.layout.horizontal_game_selecter_single_item, null);

        int resourceID;
        if (gameId != selectedGameID) {
            resourceID = Constants.getGameCircleIconId(gameId);
        } else {
            resourceID = Constants.getGameCircleSelectedIconId(gameId);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            childLayout.findViewById(R.id.game_icon).setBackground(getResources().getDrawable(resourceID));
        } else {
            childLayout.findViewById(R.id.game_icon).setBackgroundDrawable(getResources().getDrawable(resourceID));
        }
        ((TextView) childLayout.findViewById(R.id.game_name)).setText(Constants.getGameName(gameId));
        childLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.printLog("set Game Id  : " + gameId);
                EventCreateDataModel.getInstance().setGameId(gameId);
                EventCreateDataModel.getInstance().setGameName(Constants.getGameName(gameId));
                launchNextFragment();
            }
        });
        return childLayout;
    }
}
