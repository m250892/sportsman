package com.example.manoj.sportsman.filters.core;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 03/01/16.
 */
public class GameSearchAndSelectAdapter extends BaseSearchAndSelectAdpater {

    private List<String> allResults;

    public GameSearchAndSelectAdapter(Context mContext, List<String> allResults) {
        super(mContext);
        this.allResults = allResults;
    }

    @Override
    int getMaxResult() {
        return 20;
    }

    @Override
    List<String> searchResult(String searchText) {
        if (searchText == null) {
            return allResults;
        }
        List<String> suggestions = new ArrayList<>();
        for (String item : allResults) {
            if (item.toLowerCase().startsWith(searchText.toLowerCase())) {
                suggestions.add(item);
            }
        }
        return suggestions;
    }

}
