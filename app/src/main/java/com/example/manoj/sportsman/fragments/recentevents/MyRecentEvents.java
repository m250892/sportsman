package com.example.manoj.sportsman.fragments.recentevents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 31/12/15.
 */
public class MyRecentEvents extends BaseFragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static MyRecentEvents newInstance() {
        Bundle args = new Bundle();
        MyRecentEvents fragment = new MyRecentEvents();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.recent_events_fragment_layout, container, false);
        initViews(baseView);
        return baseView;
    }

    private void initViews(View baseView) {
        mRecyclerView = (RecyclerView) baseView.findViewById(R.id.recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<EventModel> mDataset = new ArrayList<>();
        //TODO
        // specify an adapter (see also next example)
        mAdapter = new RecentEventAdapter(mDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}
