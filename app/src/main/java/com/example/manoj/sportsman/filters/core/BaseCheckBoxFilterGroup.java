package com.example.manoj.sportsman.filters.core;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.filters.FilterUtils;
import com.example.manoj.sportsman.filters.filterhash.BaseFilterHash;
import com.example.manoj.sportsman.views.FilterCheckBox;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by manoj on 04/01/16.
 */
public abstract class BaseCheckBoxFilterGroup<T extends BaseFilterHash> extends BaseFilterGroup<T> implements View.OnClickListener {

    private View view;
    private LinearLayout checkBoxLayout;

    public BaseCheckBoxFilterGroup(Activity activity) {
        super(activity);
        init();
    }

    protected void init() {
        this.checkBoxLayout = new LinearLayout(this.getActivity());
        this.checkBoxLayout.setOrientation(LinearLayout.VERTICAL);
        FilterUtils.populateCheckBoxLayout(getLinkedHashMap(), this.checkBoxLayout,
                this.getNumberOfElementsPerRow());
        if (getLabel() != null) {
            LinearLayout linearLayout = new LinearLayout(getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            TextView label = (TextView) LayoutInflater.from(this.getActivity())
                    .inflate(R.layout.filters_label, linearLayout,
                            false);
            label.setText(getLabel());
            linearLayout.addView(label);
            linearLayout.addView(this.checkBoxLayout);
            this.view = linearLayout;
        } else {
            this.view = this.checkBoxLayout;
        }
    }

    @Override
    public View getView() {
        return this.view;
    }

    protected LinearLayout getCheckBoxLayout() {
        return this.checkBoxLayout;
    }

    protected void setCheckBoxesFromStringSet(Set<String> valueSet) {
        for (int outerIndex = 0; outerIndex < this.checkBoxLayout.getChildCount(); outerIndex++) {
            LinearLayout row = (LinearLayout) this.checkBoxLayout.getChildAt(outerIndex);
            for (int innerIndex = 0; innerIndex < row.getChildCount(); innerIndex++) {
                View view = row.getChildAt(innerIndex);
                if (view instanceof Checkable) {
                    boolean isChecked = valueSet.contains(view.getTag());
                    ((Checkable) view).setChecked(isChecked);
                    view.setOnClickListener(this);
                }
            }
        }
    }

    @Override
    public boolean isActivated() {
        for (int outerIndex = 0; outerIndex < this.checkBoxLayout.getChildCount(); outerIndex++) {
            LinearLayout row = (LinearLayout) this.checkBoxLayout.getChildAt(outerIndex);
            for (int innerIndex = 0; innerIndex < row.getChildCount(); innerIndex++) {
                View checkBox = row.getChildAt(innerIndex);
                if (checkBox.getVisibility() == View.VISIBLE && ((CheckBox) checkBox).isChecked()) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void writeToFilterHash(T filterHash) {
        ArrayList<String> typesList = new ArrayList<>();
        for (int outerIndex = 0; outerIndex < this.getCheckBoxLayout().getChildCount();
             outerIndex++) {
            View view = this.getCheckBoxLayout().getChildAt(outerIndex);
            if (view instanceof LinearLayout) {
                LinearLayout row = (LinearLayout) view;
                for (int innerIndex = 0; innerIndex < row.getChildCount(); innerIndex++) {
                    View checkBox = row.getChildAt(innerIndex);
                    if (checkBox.getVisibility() == View.VISIBLE &&
                            ((FilterCheckBox) checkBox).isChecked()) {
                        typesList.add(checkBox.getTag().toString());
                    }
                }
            }
        }

        setFilterHashList(filterHash, typesList);
    }

    @Override
    public void readFromFilterHash(T filterHash) {
        ArrayList<String> list = getKeyListFromFilterHash(filterHash);
        if (list != null) {
            Set<String> keySet = new HashSet<>();
            for (String key : list) {
                keySet.add(key);
            }
            setCheckBoxesFromStringSet(keySet);
        }
    }

    @Override
    public void onClick(View v) {
        Log.d("manoj", "Check box on click called");
    }

    abstract protected Map getLinkedHashMap();

    abstract protected int getNumberOfElementsPerRow();

    abstract protected String getLabel();

    protected abstract void setFilterHashList(T filterHash, ArrayList<String> typeList);

    protected abstract ArrayList<String> getKeyListFromFilterHash(T filterHash);


}
