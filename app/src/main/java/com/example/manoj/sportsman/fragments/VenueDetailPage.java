package com.example.manoj.sportsman.fragments;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.VenueModel;

/**
 * Created by manoj on 20/11/15.
 */
public class VenueDetailPage extends BaseFragment {

    private LinearLayout baseViewContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.venue_detail_page_layout, container, false);
        initViews(baseView);
        return baseView;
    }

    private void initViews(View baseView) {
        initToolbar(baseView);
        baseViewContainer = (LinearLayout) baseView
                .findViewById(R.id.base_view_container);
        renderUI();
    }

    private void renderUI() {
        renderAboutLocality();
        renderAddressMapCard();
        renderOpeningTimeCard();
        renderPaymentMethodCard();
        renderSubscriptionTypeCard();
        renderGameAvailableCard();
        renderUpcommingEventsCard();
    }


    private void renderAboutLocality() {
        View aboutVenueView = getActivityReference().getLayoutInflater().inflate(
                R.layout.about_venue_card, null);
        TextView aboutClubText = (TextView) aboutVenueView.findViewById(R.id.about_club_text);
        aboutClubText.setText(VenueModel.aboutVenue);
        setViewParams(aboutVenueView, 0, 0, 0, 0);
    }

    private void renderAddressMapCard() {
        View addressCardView = getActivityReference().getLayoutInflater().inflate(
                R.layout.address_venue_card, null);
        TextView addressView = (TextView) addressCardView.findViewById(R.id.address);
        addressView.setText(VenueModel.address);
        ImageView staticMapView = (ImageView) addressCardView.findViewById(R.id.address_static_map);

        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(addressCardView, 0, MARGIN_8, 0, 0);
    }

    private void renderOpeningTimeCard() {
        View openingTimesView = getActivityReference().getLayoutInflater().inflate(
                R.layout.open_time_venue_card, null);
        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        TextView weekdayTiming = (TextView) openingTimesView.findViewById(R.id.weekday_timing);
        weekdayTiming.setText(VenueModel.getWeekdaysTiming());
        TextView weekendTiming = (TextView) openingTimesView.findViewById(R.id.weekend_timing);
        weekendTiming.setText(VenueModel.getWeekendTiming());
        setViewParams(openingTimesView, 0, MARGIN_8, 0, 0);
    }

    private void renderPaymentMethodCard() {
        View paymentView = getActivityReference().getLayoutInflater().inflate(
                R.layout.payment_method_venue_card, null);
        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(paymentView, 0, MARGIN_8, 0, 0);
    }

    private void renderSubscriptionTypeCard() {
        View subscriptionView = getActivityReference().getLayoutInflater().inflate(
                R.layout.subscription_type_venue_card, null);
        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(subscriptionView, 0, MARGIN_8, 0, 0);
    }

    private void renderGameAvailableCard() {
        View gameView = getActivityReference().getLayoutInflater().inflate(
                R.layout.game_available_venue_card, null);
        ViewGroup horizontalView = (ViewGroup) gameView.findViewById(R.id.horizontal_view);
        for (int index = 0; index < 5; index++) {
            View singleGameView = getActivityReference().getLayoutInflater().inflate(
                    R.layout.base_game_available_layout, null);
            horizontalView.addView(singleGameView);
        }

        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(gameView, 0, MARGIN_8, 0, 0);
    }

    private void renderUpcommingEventsCard() {
        View upcommingEventView = getActivityReference().getLayoutInflater().inflate(
                R.layout.upcomming_events_venue_card, null);

        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(upcommingEventView, 0, MARGIN_8, 0, 0);
    }

    protected void setViewParams(View view, int leftMargin, int topMargin, int rightMargin,
                                 int bottomMargin) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        baseViewContainer.addView(view, lp);
    }

    private void initToolbar(View baseView) {
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) baseView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(VenueModel.venueName);
    }

    public static VenueDetailPage newInstance() {
        return new VenueDetailPage();
    }
}
