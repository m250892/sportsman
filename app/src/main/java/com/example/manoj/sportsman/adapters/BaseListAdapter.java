package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;

/**
 * Created by manoj on 03/11/15.
 */
public abstract class BaseListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;

    public BaseListAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public Context getContext() {
        return context;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public View getLayoutView(int id) {
        return getInflater().inflate(id, null);
    }
}
