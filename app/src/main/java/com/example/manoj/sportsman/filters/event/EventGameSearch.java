package com.example.manoj.sportsman.filters.event;

import android.app.Activity;

import com.example.manoj.sportsman.filters.core.BaseSearchAndSelectCard;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 08/01/16.
 */
public class EventGameSearch extends BaseSearchAndSelectCard<EventFilterHash> {

    public EventGameSearch(Activity activity) {
        super(activity);
    }


    @Override
    protected String getLabel() {
        return "GAME";
    }

    @Override
    protected List<String> allResults() {
        List<String> data = new ArrayList<>();
        data.add("Football");
        data.add("Cricket");
        data.add("BasketBall");
        data.add("Tennis");
        return data;
    }
}
