package com.example.manoj.sportsman.filters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.views.FilterCheckBox;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by manoj on 04/01/16.
 */
public class FilterUtils {

    public static void addSeparator(ViewGroup parent) {
        int margin = (int) parent.getContext().getResources()
                .getDimension(R.dimen.dimen_1);
        View view = new View(parent.getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.setMargins(margin, margin, margin, margin);
        view.setLayoutParams(params);
        view.setVisibility(View.INVISIBLE);
        parent.addView(view);
    }

    public static void populateCheckBoxLayout(Map map, ViewGroup parent, int elementsPerRow) {
        int noOfRows = map.size() / elementsPerRow;
        if (map.size() % elementsPerRow > 0) {
            noOfRows++;
        }

        Iterator iterator = map.entrySet().iterator();
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        int margin = (int) parent.getContext().getResources()
                .getDimension(R.dimen.dimen_1);
        for (int row = 0; row < noOfRows; row++) {
            LinearLayout rowLayout = new LinearLayout(parent.getContext());
            LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            rowLayout.setLayoutParams(rowParams);
            for (int column = 0; column < elementsPerRow; column++) {
                if (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    FilterCheckBox filterCheckBox = (FilterCheckBox) layoutInflater
                            .inflate(R.layout.filters_checkbox, rowLayout, false);
                    filterCheckBox.setTag(pair.getKey().toString());
                    filterCheckBox.setText(pair.getValue().toString());
                    rowLayout.addView(filterCheckBox);
                } else {
                    View view = new View(parent.getContext());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.weight = 1;
                    params.setMargins(margin, margin, margin, margin);
                    view.setLayoutParams(params);
                    view.setVisibility(View.INVISIBLE);
                    rowLayout.addView(view);
                }
            }
            parent.addView(rowLayout);
        }
    }

}

