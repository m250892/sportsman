package com.example.manoj.sportsman.fragments.unusedcode;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.dialogs.DatePickerFragment;
import com.example.manoj.sportsman.dialogs.DialogCallbacks;
import com.example.manoj.sportsman.dialogs.GameSelecter;
import com.example.manoj.sportsman.dialogs.ISearchAutoCompleteSelect;
import com.example.manoj.sportsman.dialogs.SearchDialog;
import com.example.manoj.sportsman.dialogs.TimePickerFragment;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventModel;
import com.example.manoj.sportsman.tasks.EventPostTask;
import com.example.manoj.sportsman.utils.Utils;
import com.example.manoj.sportsman.views.RangeSeekBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created by manoj on 30/10/15.
 */
public class CreateEventFragment extends BaseFragment implements DialogCallbacks, ISearchAutoCompleteSelect, RangeSeekBar.OnRangeSeekBarChangeListener<Integer>, EventPostTask.EventPostTaskCallback {


    private EditText titleInputView;
    private EditText costPerPersonInputView;
    private TextView startDateView, endDateView;
    private TextView startTimeView, endTimeView;
    private TextView gameTypeFriendly, gameTypePractice, gameTypeCompete;
    private TextView priceFree, pricePaid, pricePerPersonHeading;
    private EditText pricePerPersonInput;
    private TextView minPerson, maxPerson;
    private TextView localityName;
    private ProgressDialog progressDialog;

    private RangeSeekBar<Integer> seekBar;
    private final ArrayList<Integer> intervalList = new ArrayList<>();
    private EventModel eventModel;

    private EventPostTask eventPostTask;

    public static CreateEventFragment newInstance() {
        return new CreateEventFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.create_event_fragment_layout, container, false);
        initViews(baseView);
        setListeners();
        setDefaultValues();
        openGameSelecterDialog();
        return baseView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void openGameSelecterDialog() {
        GameSelecter gameSelecter = new GameSelecter(this);
        gameSelecter.setContext(getContext());
        gameSelecter.show(getChildFragmentManager(), gameSelecter.getClass().getCanonicalName());
    }

    public EventModel getCreateEventModelInstance() {
        if (eventModel == null) {
            eventModel = new EventModel();
        }
        return eventModel;
    }

    private void setDefaultValues() {
        handlePriceTypeSelected(Constants.PRICE_TYPE_FREE);
        handleGameTypeSelected(Constants.GAME_TYPE_FRIENDLY);
        seekBar.setSelectedMinValue(0);
        seekBar.setSelectedMaxValue(10);
    }

    private void initViews(View baseView) {
        titleInputView = (EditText) baseView.findViewById(R.id.title_input);

        startDateView = (TextView) baseView.findViewById(R.id.start_date);
        endDateView = (TextView) baseView.findViewById(R.id.end_date);
        startTimeView = (TextView) baseView.findViewById(R.id.start_time);
        endTimeView = (TextView) baseView.findViewById(R.id.end_time);


        gameTypeFriendly = (TextView) baseView.findViewById(R.id.game_type_friendly);
        gameTypePractice = (TextView) baseView.findViewById(R.id.game_type_practice);
        gameTypeCompete = (TextView) baseView.findViewById(R.id.game_type_compete);
        priceFree = (TextView) baseView.findViewById(R.id.price_free);
        pricePaid = (TextView) baseView.findViewById(R.id.price_paid);
        pricePerPersonInput = (EditText) baseView.findViewById(R.id.cost_per_person_input);
        pricePerPersonHeading = (TextView) baseView.findViewById(R.id.cost_per_person_input_heading);
        minPerson = (TextView) baseView.findViewById(R.id.value_min);
        maxPerson = (TextView) baseView.findViewById(R.id.value_max);
        localityName = (TextView) baseView.findViewById(R.id.locality_name);
        costPerPersonInputView = (EditText) baseView.findViewById(R.id.cost_per_person_input);
        baseView.findViewById(R.id.submit_button).setOnClickListener(this);

        ViewGroup seekBarLayout = (ViewGroup) baseView.findViewById(R.id.number_of_player_selecter);
        initIntervalList();
        this.seekBar = new RangeSeekBar<>(0, this.intervalList.size() - 1, this.getActivity());
        this.seekBar.setOnRangeSeekBarChangeListener(this);
        seekBarLayout.addView(seekBar);
    }

    private void setListeners() {
        startDateView.setOnClickListener(this);
        endDateView.setOnClickListener(this);
        startTimeView.setOnClickListener(this);
        endTimeView.setOnClickListener(this);
        gameTypeFriendly.setOnClickListener(this);
        gameTypePractice.setOnClickListener(this);
        gameTypeCompete.setOnClickListener(this);
        priceFree.setOnClickListener(this);
        pricePaid.setOnClickListener(this);
        localityName.setOnClickListener(this);
    }

    private void initIntervalList() {
        int maxValue = 10;
        int minValue = 0;
        double difference = maxValue - minValue;
        for (int iteration = 0; iteration <= difference; iteration++) {
            double value = minValue + iteration * difference;
            this.intervalList.add((int) value);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_date:
                handleStartDateViewClick();
                break;
            case R.id.end_date:
                handleEndDateViewClick();
                break;
            case R.id.start_time:
                handleStartTimeClick();
                break;
            case R.id.end_time:
                handleEndTimeClick();
                break;
            case R.id.game_type_friendly:
                handleGameTypeSelected(Constants.GAME_TYPE_FRIENDLY);
                break;
            case R.id.game_type_practice:
                handleGameTypeSelected(Constants.GAME_TYPE_PRACTICE);
                break;
            case R.id.game_type_compete:
                handleGameTypeSelected(Constants.GAME_TYPE_COMPETE);
                break;
            case R.id.price_free:
                handlePriceTypeSelected(Constants.PRICE_TYPE_FREE);
                break;
            case R.id.price_paid:
                handlePriceTypeSelected(Constants.PRICE_TYPE_PAID);
                break;
            case R.id.locality_name:
                handleLocalityNameClick();
                break;
            case R.id.submit_button:
                handleSubmitButtonClick();
                break;
        }
    }

    private void handleSubmitButtonClick() {
        Editable title = titleInputView.getText();
        Editable costPerPerson = costPerPersonInputView.getText();
        /*if (title != null) {
            getCreateEventModelInstance().setTitle(title.toString());
        }

        if (getCreateEventModelInstance().getPriceMode() == Constants.PRICE_TYPE_PAID) {
            if (TextUtils.isEmpty(costPerPerson) == false) {
                getCreateEventModelInstance().setCostPerPerson(Integer.parseInt(costPerPerson.toString()));
            }
        } else {
            getCreateEventModelInstance().setCostPerPerson(-1);
        }*/

        String jsonString = Utils.createJSONStringFromObject(getCreateEventModelInstance());
        JSONObject eventObject = null;
        if (!TextUtils.isEmpty(jsonString)) {
            try {
                eventObject = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        progressDialog = new ProgressDialog(getActivityReference());
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.show();
        new EventPostTask(eventObject, this).execute();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void handleLocalityNameClick() {
        SearchDialog searchDialog = new SearchDialog();
        searchDialog.show(getChildFragmentManager(), searchDialog.getClass().getName());
    }

    private void handlePriceTypeSelected(int priceType) {
        priceFree.setTextColor(getResources().getColor(R.color.gray));
        pricePaid.setTextColor(getResources().getColor(R.color.gray));
        //getCreateEventModelInstance().setPriceType(priceType);
        switch (priceType) {
            case Constants.PRICE_TYPE_FREE:
                priceFree.setTextColor(getResources().getColor(R.color.purple_heart));
                pricePerPersonHeading.setVisibility(View.GONE);
                pricePerPersonInput.setVisibility(View.GONE);
                break;
            case Constants.PRICE_TYPE_PAID:
                pricePaid.setTextColor(getResources().getColor(R.color.purple_heart));
                pricePerPersonHeading.setVisibility(View.VISIBLE);
                pricePerPersonInput.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void handleGameTypeSelected(int gameType) {
        gameTypeFriendly.setTextColor(getResources().getColor(R.color.gray));
        gameTypePractice.setTextColor(getResources().getColor(R.color.gray));
        gameTypeCompete.setTextColor(getResources().getColor(R.color.gray));
        //getCreateEventModelInstance().setGameType(gameType);
        switch (gameType) {
            case Constants.GAME_TYPE_FRIENDLY:
                gameTypeFriendly.setTextColor(getResources().getColor(R.color.purple_heart));
                break;
            case Constants.GAME_TYPE_PRACTICE:
                gameTypePractice.setTextColor(getResources().getColor(R.color.purple_heart));
                break;
            case Constants.GAME_TYPE_COMPETE:
                gameTypeCompete.setTextColor(getResources().getColor(R.color.purple_heart));
                break;
        }
    }

    private void handleEndTimeClick() {
        TimePickerFragment timePickerFragment = new TimePickerFragment(this, TimePickerFragment.END_TIME);
        timePickerFragment.show(getFragmentManager(), "Start Time Picker");
    }

    private void handleStartTimeClick() {
        TimePickerFragment timePickerFragment = new TimePickerFragment(this, TimePickerFragment.START_TIME);
        timePickerFragment.show(getFragmentManager(), "End Time Picker");
    }

    private void handleStartDateViewClick() {
        DatePickerFragment datePickerFragment = new DatePickerFragment(this, DatePickerFragment.START_DATE);
        datePickerFragment.show(getFragmentManager(), "Start Date Picker");
    }

    private void handleEndDateViewClick() {
        DatePickerFragment datePickerFragment = new DatePickerFragment(this, DatePickerFragment.END_DATE);
        datePickerFragment.show(getFragmentManager(), "End Date Picker");
    }


    @Override
    public void onDateSelected(int requestCode, int year, int month, int day) {
        if (requestCode == DatePickerFragment.START_DATE) {
            startDateView.setText(getDateToDisplay(year, month, day));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.set(year, month, day);
            //  getCreateEventModelInstance().setStartDate(calendar.getTimeInMillis());
        } else if (requestCode == DatePickerFragment.END_DATE) {
            endDateView.setText(getDateToDisplay(year, month, day));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.set(year, month, day);
            //getCreateEventModelInstance().setEndDate(calendar.getTimeInMillis());
        }


    }

    private String getDateToDisplay(int year, int month, int day) {
        return Utils.getDateString(day, month, year);
    }

    @Override
    public void onTimeSelected(int requestCode, int hourOfDay, int minute) {
        if (requestCode == TimePickerFragment.START_TIME) {
            startTimeView.setText(getTimeToDisplay(hourOfDay, minute));
            //getCreateEventModelInstance().setStartTime(hourOfDay * 24 + minute);
        } else if (requestCode == TimePickerFragment.END_TIME) {
            endTimeView.setText(getTimeToDisplay(hourOfDay, minute));
            // getCreateEventModelInstance().setEndTime(hourOfDay * 24 + minute);
        }
    }

    private String getTimeToDisplay(int hourOfDay, int minute) {
        String result = "";
        result += hourOfDay + " : " + minute;
        return result;
    }

    @Override
    public void onGameSelected(int id) {
        // getCreateEventModelInstance().setGameId(id);
    }

    @Override
    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
        minPerson.setText("" + minValue);
        maxPerson.setText("" + maxValue);
        //getCreateEventModelInstance().setMinPlayer(minValue);
        //getCreateEventModelInstance().setMaxPlayer(maxValue);
    }

    @Override
    public void onAutoCompleteItemSelect(SearchItemModel searchItemModel) {
        if (searchItemModel != null) {
            localityName.setText(searchItemModel.getAddressDetail());
        }
    }

    @Override
    public void onDataUploadSuccess() {
        Log.d("nagar", "onDataUploadSuccess called");
        progressDialog.dismiss();
    }

    @Override
    public void onDataUploadFailed() {

    }
}
