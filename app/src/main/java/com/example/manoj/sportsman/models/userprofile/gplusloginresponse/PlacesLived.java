package com.example.manoj.sportsman.models.userprofile.gplusloginresponse;

public class PlacesLived {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClassPojo [value = " + value + "]";
    }
}