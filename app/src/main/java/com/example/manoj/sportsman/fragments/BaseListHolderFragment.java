package com.example.manoj.sportsman.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

/**
 * Created by manoj on 31/12/15.
 */
public abstract class BaseListHolderFragment extends BaseFragment {
    protected ViewPager viewPager;
    protected TabLayout tabLayout;
    protected Toolbar toolbar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View baseView = inflater.inflate(getViewId(), container, false);
        initView(baseView);
        return baseView;
    }

    protected void initView(View baseView) {
        toolbar = (Toolbar) baseView.findViewById(R.id.tabanim_toolbar);
        setToolbar(toolbar);
        viewPager = (ViewPager) baseView.findViewById(R.id.tabanim_viewpager);
        setViewPagerAdapter();
        tabLayout = (TabLayout) baseView.findViewById(R.id.tabanim_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    protected abstract void setViewPagerAdapter();

    protected int getViewId() {
        return R.layout.events_list_view_layout;
    }

    protected CardsListType getCurrentListType() {
        return CardsListType.EVENTS_LIST;
    }
}
