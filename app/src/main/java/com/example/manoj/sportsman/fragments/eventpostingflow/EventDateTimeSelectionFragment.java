package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.dialogs.DatePickerFragment;
import com.example.manoj.sportsman.dialogs.DialogCallbacks;
import com.example.manoj.sportsman.dialogs.TimePickerFragment;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;

import java.util.Calendar;

/**
 * Created by manoj on 27/11/15.
 */
public class EventDateTimeSelectionFragment extends BaseFragment implements DialogCallbacks {

    private TextView dateHeadingView, dateValueView;
    private TextView timeHeadingView, timeValueView;

    public static EventDateTimeSelectionFragment newInstance() {
        return new EventDateTimeSelectionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_date_time_selection_fragment_layout, container, false);
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        initViews(baseView);
        return baseView;
    }

    @Override
    public void onResume() {
        super.onResume();
        onTimeChangeRefreshUI();
        onDateChangeRefreshUI();
    }

    private void initViews(View baseView) {
        dateHeadingView = (TextView) baseView.findViewById(R.id.date_heading_view);
        dateValueView = (TextView) baseView.findViewById(R.id.date_value_view);
        timeHeadingView = (TextView) baseView.findViewById(R.id.time_heading_view);
        timeValueView = (TextView) baseView.findViewById(R.id.time_value_view);

        baseView.findViewById(R.id.today).setOnClickListener(this);
        baseView.findViewById(R.id.tomorrow).setOnClickListener(this);
        baseView.findViewById(R.id.saturday).setOnClickListener(this);
        baseView.findViewById(R.id.sunday).setOnClickListener(this);

        dateValueView.setOnClickListener(this);
        timeValueView.setOnClickListener(this);
        baseView.findViewById(R.id.navigation_next_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d("manoj", "onClick called");
        switch (v.getId()) {
            case R.id.date_value_view:
                handleDateSelectionViewClick();
                break;
            case R.id.time_value_view:
                handleTimeSelectionViewClick();
                break;
            case R.id.navigation_next_button:
                launchNextFragment();
                break;
            case R.id.today:
                handleTodayButtonClick();
                break;
            case R.id.tomorrow:
                handleTomorrowButtonClick();
                break;
            case R.id.saturday:
                handleSaturdayButtonClick();
                break;
            case R.id.sunday:
                handleSundayButtonClick();
                break;
        }
    }

    private void handleSundayButtonClick() {
        Calendar calendar = nextDayOfWeek(Calendar.SATURDAY);
        calendar.add(Calendar.DATE, 1);
        setDate(calendar);
    }

    private void handleSaturdayButtonClick() {
        setDate(nextDayOfWeek(Calendar.SATURDAY));
    }

    private void handleTomorrowButtonClick() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        setDate(calendar);
    }

    private void handleTodayButtonClick() {
        setDate(Calendar.getInstance());
    }

    private void setDate(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        onDateSelected(DatePickerFragment.START_DATE, year, month, day);
    }

    private Calendar nextDayOfWeek(int day) {
        Calendar date = Calendar.getInstance();
        int diff = day - date.get(Calendar.DAY_OF_WEEK);
        if (!(diff > 0)) {
            diff += 7;
        }
        date.add(Calendar.DAY_OF_MONTH, diff);
        return date;
    }

    private void handleDateSelectionViewClick() {
        DatePickerFragment datePickerFragment = new DatePickerFragment(this, DatePickerFragment.START_DATE);
        datePickerFragment.show(getFragmentManager(), "Start Date Picker");
    }

    private void handleTimeSelectionViewClick() {
        TimePickerFragment timePickerFragment = new TimePickerFragment(this, TimePickerFragment.START_TIME);
        timePickerFragment.show(getFragmentManager(), "Start Time Picker");
    }

    @Override
    public void onDateSelected(int requestCode, int year, int month, int day) {
        EventCreateDataModel.getInstance().getDateTimeModel().setDate(day, month, year);
        onDateChangeRefreshUI();
    }

    private void onDateChangeRefreshUI() {
        String headingString;
        String valueString;
        if (EventCreateDataModel.getInstance().getDateTimeModel().isDateSelected()) {
            headingString = "Date";
            valueString = EventCreateDataModel.getInstance().getDateTimeModel().getDayMonthString();
        } else {
            headingString = "Select a";
            valueString = "DATE..";
        }
        dateHeadingView.setText(headingString);
        dateValueView.setText(valueString);
    }

    private void onTimeChangeRefreshUI() {
        String headingString;
        String valueString;
        if (EventCreateDataModel.getInstance().getDateTimeModel().isTimeSelected()) {
            headingString = "Time";
            valueString = EventCreateDataModel.getInstance().getDateTimeModel().getTimeStringFormat();
        } else {
            headingString = "Choose a";
            valueString = "TIME..";
        }
        timeHeadingView.setText(headingString);
        timeValueView.setText(valueString);
    }

    private void launchNextFragment() {
        if (EventCreateDataModel.getInstance().isDateTimeSelected()) {
            getFragmentController().performOperation(IFragmentController.CREATE_EVENT_ADDRESS_SELECTION_FRAGMENT, null);
        } else {
            String errorMessage;
            if (!EventCreateDataModel.getInstance().getDateTimeModel().isDateSelected()) {
                errorMessage = "please choose a date!!";
            } else {
                errorMessage = "please select a time!!";
            }
            showToastMessage(errorMessage);
        }
    }

    @Override
    public void onTimeSelected(int requestCode, int hourOfDay, int minute) {
        EventCreateDataModel.getInstance().getDateTimeModel().setTime(hourOfDay, minute);
        onTimeChangeRefreshUI();
    }

    @Override
    public void onGameSelected(int gameId) {

    }
}