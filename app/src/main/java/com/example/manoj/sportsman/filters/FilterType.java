package com.example.manoj.sportsman.filters;

/**
 * Created by manoj on 08/01/16.
 */
public enum FilterType {
    EVENT_GENDER,
    EVENT_GAME,
    EVENT_LEVEL,
    EVENT_PRICE,
    PLAYER_GAME,
    PLAYER_GENDER
}
