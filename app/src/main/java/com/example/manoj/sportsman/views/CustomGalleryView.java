package com.example.manoj.sportsman.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

/**
 * Created by manoj on 01/12/15.
 */
public class CustomGalleryView extends Gallery {
    private float FLING_SCALE_DOWN_FACTOR = 0.6f;

    public CustomGalleryView(Context context) {
        super(context);
    }

    public CustomGalleryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomGalleryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return super.onFling(e1, e2, velocityX * FLING_SCALE_DOWN_FACTOR, velocityY * FLING_SCALE_DOWN_FACTOR);
    }
}
