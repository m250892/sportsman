package com.example.manoj.sportsman.databasemanager;

import android.location.Address;
import android.text.TextUtils;

import com.example.manoj.sportsman.utils.Model;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by manoj on 31/10/15.
 */
public class SearchItemModel extends Model implements Serializable {

    @SerializedName("source_type")
    private int sourceType;
    private Address address;

    @SerializedName("premisses")
    private String premissesName;
    @SerializedName("sub_locality")
    private String subLocalityName;
    @SerializedName("locality")
    private String localityName;
    @SerializedName("city")
    private String cityName;
    @SerializedName("state")
    private String stateName;
    @SerializedName("country")
    private String countryName;
    @SerializedName("postal_code")
    private String postalCode;
    private double latitude;
    private double longitude;

    @SerializedName("address_detail")
    private String addressDetail;

    public SearchItemModel() {
    }

    public SearchItemModel(Address address) {
        this.address = address;
        this.premissesName = address.getPremises();
        this.subLocalityName = address.getFeatureName();
        this.localityName = address.getSubLocality();
        this.cityName = address.getLocality();
        this.stateName = address.getAdminArea();
        this.countryName = address.getCountryName();
        this.latitude = address.getLatitude();
        this.longitude = address.getLongitude();
        this.postalCode = address.getPostalCode();
    }

    public SearchItemModel(String address_detail) {
        this.addressDetail = address_detail;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAddressDetail() {
        if (address != null && addressDetail == null) {
            addressDetail = "";
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                String data = address.getAddressLine(i);
                if (!TextUtils.isEmpty(data)) {
                    if (i != 0) {
                        addressDetail += ", ";
                    }
                    addressDetail += data;
                }
            }
        }
        return addressDetail;
    }


    public String getSubLocalityName() {
        return subLocalityName;
    }


    public String getPremissesName() {
        return premissesName;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }


    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public boolean isContain(String firstString, String secondString) {
        if (firstString == null || secondString == null) {
            return false;
        }
        return firstString.toLowerCase().contains(secondString.toLowerCase());
    }

    public int getSourceType() {
        return sourceType;
    }

    public String toString() {
        String result = "";
        if (getSubLocalityName() != null && !isContain(result, getSubLocalityName())) {
            result += "Namae : " + getSubLocalityName() + "\n";
        }
        if (getLocalityName() != null && !isContain(result, getLocalityName())) {
            result += "Area Name : " + getLocalityName() + "\n";
        }
        if (getCityName() != null && !isContain(result, getCityName())) {
            result += "City Name : " + getCityName() + "\n";
        }
        if (getStateName() != null && !isContain(result, getStateName())) {
            result += "State Name : " + getStateName() + "\n";
        }
        if (getCountryName() != null && !isContain(result, getCountryName())) {
            result += "Country Name : " + getCountryName() + "\n";
        }
        result += "Latitude : " + latitude + ", Longitude : " + longitude + "\n";
        return result;
    }

    public JSONObject getJsonObject() {
        JSONObject result = null;
        try {
            result = new JSONObject((new Gson()).toJson(this));
            result.remove("address");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean compare(SearchItemModel searchItemModel) {
        if (searchItemModel == null) {
            return false;
        }
        if (getLatitude() != searchItemModel.getLatitude()) {
            return false;
        }
        if (getLongitude() != searchItemModel.getLongitude()) {
            return false;
        }
        if (this.toString() != searchItemModel.toString()) {
            return false;
        }
        return true;
    }
}
