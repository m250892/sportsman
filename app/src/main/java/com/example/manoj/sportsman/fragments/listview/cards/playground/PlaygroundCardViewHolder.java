package com.example.manoj.sportsman.fragments.listview.cards.playground;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewHolder;

/**
 * Created by manoj on 26/12/15.
 */
public class PlaygroundCardViewHolder extends BaseListCardViewHolder {

    public TextView nameView;
    public TextView areaView;
    public ImageView thumnailImage;
    public View callButtonView;
    public LinearLayout availableGamesView;

    public PlaygroundCardViewHolder(View baseView) {
        super(baseView);
        nameView = (TextView) baseView.findViewById(R.id.name);
        areaView = (TextView) baseView.findViewById(R.id.area_name);
        thumnailImage = (ImageView) baseView.findViewById(R.id.club_thumnail_image);
        callButtonView = baseView.findViewById(R.id.call_button);
        availableGamesView = (LinearLayout) baseView.findViewById(R.id.glevel_view);
    }
}
