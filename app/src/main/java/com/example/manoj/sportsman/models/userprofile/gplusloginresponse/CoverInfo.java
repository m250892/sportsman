package com.example.manoj.sportsman.models.userprofile.gplusloginresponse;

public class CoverInfo {
    private String leftImageOffset;

    private String topImageOffset;

    public String getLeftImageOffset() {
        return leftImageOffset;
    }

    public void setLeftImageOffset(String leftImageOffset) {
        this.leftImageOffset = leftImageOffset;
    }

    public String getTopImageOffset() {
        return topImageOffset;
    }

    public void setTopImageOffset(String topImageOffset) {
        this.topImageOffset = topImageOffset;
    }

    @Override
    public String toString() {
        return "ClassPojo [leftImageOffset = " + leftImageOffset + ", topImageOffset = " + topImageOffset + "]";
    }
}

		