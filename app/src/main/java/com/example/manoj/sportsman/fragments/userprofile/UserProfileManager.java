package com.example.manoj.sportsman.fragments.userprofile;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.models.listview.EventCardModel;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.utils.SharedPrefsManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 11/12/15.
 */
public class UserProfileManager {
    private static UserProfileManager userProfileManager;

    private SignInResponse userProfile;
    private boolean isLoggedIn;
    private List<EventCardModel> joinEvents;

    private UserProfileManager() {
        joinEvents = new ArrayList<>();
        if (SharedPrefsManager.getInstance().getBoolean(Constants.PREF_USER_LOGGED_IN)) {
            isLoggedIn = true;
            userProfile = SharedPrefsManager.getInstance().getObject(Constants.PREF_USER_PROFILE_JSON_STRING, SignInResponse.class);
        } else {
            isLoggedIn = false;
        }
    }

    public synchronized static UserProfileManager getInstance() {
        if (null == userProfileManager) {
            userProfileManager = new UserProfileManager();
        }
        return userProfileManager;
    }

    public static SignInResponse getUserProfile() {
        return getInstance().userProfile;
    }

    public static void setUserProfile(SignInResponse userProfile) {
        getInstance().userProfile = userProfile;
    }

    public static boolean isLoggedIn() {
        return getInstance().isLoggedIn;
    }


    public void onLoginSuccess(SignInResponse userDetail) {
        UserProfileManager.setUserProfile(userDetail);
        UserProfileManager.handleLogin();
    }

    public static void handleLogin() {
        getInstance().isLoggedIn = true;
        SharedPrefsManager.getInstance().setBoolean(Constants.PREF_USER_LOGGED_IN, getInstance().isLoggedIn);
        SharedPrefsManager.getInstance().setObject(Constants.PREF_USER_PROFILE_JSON_STRING, getUserProfile());
    }

    public static void handleLogout() {
        SharedPrefsManager.getInstance().removeKey(Constants.PREF_USER_LOGGED_IN);
        SharedPrefsManager.getInstance().removeKey(Constants.PREF_USER_PROFILE_JSON_STRING);
        setUserProfile(null);
        getInstance().isLoggedIn = false;
    }

    public List<EventCardModel> getJoinEvents() {
        return joinEvents;
    }

    public void addEvent(EventCardModel eventCardModel) {
        this.joinEvents.add(eventCardModel);
    }
}
