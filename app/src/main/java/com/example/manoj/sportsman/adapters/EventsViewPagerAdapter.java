package com.example.manoj.sportsman.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.manoj.sportsman.fragments.listview.fragments.BaseCLFragment;
import com.example.manoj.sportsman.fragments.listview.fragments.CoachCLFragment;
import com.example.manoj.sportsman.fragments.listview.fragments.EventsCLFragment;
import com.example.manoj.sportsman.fragments.listview.fragments.PlayersCLFragment;
import com.example.manoj.sportsman.fragments.listview.fragments.PlaygroundCLFragment;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 25/11/15.
 */
public class EventsViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> pageTitles = new ArrayList<>();
    private List<CardsListType> cardsListTypeList = new ArrayList<>();

    public EventsViewPagerAdapter(FragmentManager manager, List<Integer> gameIds) {
        super(manager);
        pageTitles.add("Games");
        pageTitles.add("Players");
        pageTitles.add("Playgrounds");
        //pageTitles.add("Coaches");
        cardsListTypeList.add(CardsListType.EVENTS_LIST);
        cardsListTypeList.add(CardsListType.PLAYERS_LIST);
        cardsListTypeList.add(CardsListType.PLAYGROUNDS_LIST);
        //cardsListTypeList.add(CardsListType.COACHES_LIST);
    }

    public int getCardsListPositionInViewPager(CardsListType cardsListType) {
        for (int index = 0; index < cardsListTypeList.size(); index++) {
            if (cardsListTypeList.get(index) == cardsListType) {
                return index;
            }
        }
        return 0;
    }

    public CardsListType getCardsListType(int position) {
        if (position < cardsListTypeList.size()) {
            return cardsListTypeList.get(position);
        }
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        return getFragment(cardsListTypeList.get(position));
    }

    @Override
    public int getCount() {
        return cardsListTypeList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles.get(position);
    }

    public BaseCLFragment getFragment(CardsListType cardsListType) {
        switch (cardsListType) {
            case EVENTS_LIST:
                return new EventsCLFragment();
            case PLAYERS_LIST:
                return new PlayersCLFragment();
            case PLAYGROUNDS_LIST:
                return new PlaygroundCLFragment();
            case COACHES_LIST:
                return new CoachCLFragment();
        }
        return null;
    }
}
