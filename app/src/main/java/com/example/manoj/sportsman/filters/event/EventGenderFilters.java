package com.example.manoj.sportsman.filters.event;

import android.app.Activity;
import android.util.Log;

import com.example.manoj.sportsman.filters.FilterConstants;
import com.example.manoj.sportsman.filters.core.BaseCheckBoxFilterGroup;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by manoj on 05/01/16.
 */
public class EventGenderFilters extends BaseCheckBoxFilterGroup<EventFilterHash> {

    private static final int ELEMENTS_PER_ROW = 1;

    public EventGenderFilters(Activity activity) {
        super(activity);
        Log.d("manoj", "Event Gender Filter Called");
    }

    @Override
    protected Map getLinkedHashMap() {
        return FilterConstants.EVENT_GENDER_MAP;
    }

    @Override
    protected int getNumberOfElementsPerRow() {
        return ELEMENTS_PER_ROW;
    }

    @Override
    protected String getLabel() {
        return "GENDER";
    }

    @Override
    protected void setFilterHashList(EventFilterHash filterHash, ArrayList<String> typeList) {
        filterHash.setGenderTypes(typeList);
    }

    @Override
    protected ArrayList<String> getKeyListFromFilterHash(EventFilterHash filterHash) {
        return filterHash.getGenderTypes();
    }
}
