package com.example.manoj.sportsman.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.EventModel;
import com.example.manoj.sportsman.utils.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by manoj on 25/11/15.
 */
public class EventDetailPage extends BaseFragment {

    private LinearLayout baseViewContainer;
    private final static String EVENT_MODEL_DATE_KEY = "event model data key";
    private EventModel eventModel;
    private TextView descriptionTextView;

    public static EventDetailPage newInstance(EventModel input) {
        EventDetailPage eventDetailPage = new EventDetailPage();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EVENT_MODEL_DATE_KEY, input);
        eventDetailPage.setArguments(bundle);
        return eventDetailPage;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventModel = (EventModel) getArguments().getSerializable(EVENT_MODEL_DATE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_detail_page_layout, container, false);
        initViews(baseView);
        return baseView;
    }

    private void initViews(View baseView) {
        ImageView gameImageBg = (ImageView) baseView.findViewById(R.id.game_image);
        gameImageBg.setImageResource(Constants.getBackgroundId(eventModel.getSportId()));
        initToolbar(baseView);
        baseViewContainer = (LinearLayout) baseView
                .findViewById(R.id.base_view_container);
        renderUI();
    }

    private void renderUI() {
        if (eventModel == null) {
            new RuntimeException("Event data model is null");
        }
        //renderAboutLocality();
        renderTimingInfoCard();
        renderPriceCard();
        renderGeneralInfoCard();
        renderAddressMapCard();
    }


    private void renderGeneralInfoCard() {
        View baseView = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_detail_general_info_card_layout, null);
        TextView playerLevel = (TextView) baseView.findViewById(R.id.player_level_view);
        TextView gameType = (TextView) baseView.findViewById(R.id.game_type_view);
        TextView joinApproval = (TextView) baseView.findViewById(R.id.join_approval_view);
        TextView genderView = (TextView) baseView.findViewById(R.id.gender_type);
        TextView minPlayer = (TextView) baseView.findViewById(R.id.min_player_view);
        TextView maxPlater = (TextView) baseView.findViewById(R.id.max_player_view);

        playerLevel.setText(eventModel.getEventLevel());
        gameType.setText(eventModel.getEventType());
        joinApproval.setText("" + eventModel.isJoinApprovalRequire());
        //TODO gender
        minPlayer.setText("" + eventModel.getMinPlayer());
        maxPlater.setText("" + eventModel.getMaxPlayer());

        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(baseView, 0, MARGIN_8, 0, 0);
    }

    private void renderPriceCard() {
        View baseView = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_detail_price_info_card_layout, null);
        TextView priceType = (TextView) baseView.findViewById(R.id.price_type);
        priceType.setText(eventModel.getCostPerPersonString());
        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        setViewParams(baseView, 0, MARGIN_8, 0, 0);
    }

    private void renderTimingInfoCard() {
        View baseView = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_detail_timing_info_card_layout, null);
        TextView dateView = (TextView) baseView.findViewById(R.id.date_text_view);
        TextView timeView = (TextView) baseView.findViewById(R.id.time_text_view);
        TextView dayView = (TextView) baseView.findViewById(R.id.day_text_view);
        TextView durationView = (TextView) baseView.findViewById(R.id.duration_text_view);

        long timestamp = 1000L * Long.parseLong(eventModel.getEventEpochTime());
        dateView.setText(Utils.getCompleteDateFormattedString(timestamp));
        timeView.setText(Utils.getTimeString(timestamp));
        dayView.setText(Utils.getDayName(timestamp));
        durationView.setText(eventModel.getDuration());

        setViewParams(baseView, 0, 0, 0, 0);
    }

    private void renderAboutLocality() {
        if (eventModel.getDescription() != null) {
            View aboutVenueView = getActivityReference().getLayoutInflater().inflate(
                    R.layout.event_detail_description_card_layout, null);
            TextView descriptionTextView = (TextView) aboutVenueView.findViewById(R.id.event_description_text_view);
            descriptionTextView.setText(eventModel.getDescription());
            setViewParams(aboutVenueView, 0, 0, 0, 0);
        }
    }

    private void renderAddressMapCard() {
        View addressCardView = getActivityReference().getLayoutInflater().inflate(
                R.layout.event_detail_address_card_layout, null);
        TextView addressView = (TextView) addressCardView.findViewById(R.id.address);
        addressView.setText(eventModel.getAddress());
        ImageView staticMapView = (ImageView) addressCardView.findViewById(R.id.address_static_map);
        int MARGIN_8 = getContext().getResources()
                .getDimensionPixelSize(R.dimen.dimen_8);
        Log.d("manoj", "static map url : " + eventModel.getStaticMapUrl());
        Picasso.with(getContext()).load(eventModel.getStaticMapUrl()).into(staticMapView);
        setViewParams(addressCardView, 0, MARGIN_8, 0, 0);
        addressCardView.findViewById(R.id.explore_nearby_button).setOnClickListener(this);
    }

    protected void setViewParams(View view, int leftMargin, int topMargin, int rightMargin,
                                 int bottomMargin) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        baseViewContainer.addView(view, lp);
    }

    private void initToolbar(View baseView) {
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) baseView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(eventModel.getSportName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.explore_nearby_button:
                openGoogleMapApplication();
                break;
        }
    }

    private void openGoogleMapApplication() {
        Uri gmmIntentUri = Uri.parse("geo:0,0?z=12&q=" + eventModel.getLatLongString());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
