package com.example.manoj.sportsman.utils.locationmanager;

/**
 * Created by manoj on 28/11/15.
 */
public class LocaionManagerConstants {

    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationaddress";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    public static double lowerLeftLatitude = 18.878676;
    public static double lowerLeftLongitude = 72.773438;
    public static double upperRightLatitude = 20.934507;
    public static double upperRightLongitude = 73.531494;
}
