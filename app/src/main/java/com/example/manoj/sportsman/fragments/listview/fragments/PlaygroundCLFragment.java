package com.example.manoj.sportsman.fragments.listview.fragments;

import android.view.View;
import android.widget.AdapterView;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.tasks.listview.PlaygroundCLFetcherTask;

/**
 * Created by manoj on 26/12/15.
 */
public class PlaygroundCLFragment extends BaseCLFragment {

    @Override
    public void fetchFirstPageData() {
        if (!getDataStore().isDataAvailable()) {
            new PlaygroundCLFetcherTask(this, getListType()).execute();
        }
    }

    @Override
    public CardsListType getListType() {
        return CardsListType.PLAYGROUNDS_LIST;
    }

    @Override
    public void onChildViewButtonClick(int actionId, BaseCardModel eventModel, View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getFragmentController().performOperation(IFragmentController.PLAYGROUND_DETAIL_PAGE_FRAGMENT, null);
    }
}
