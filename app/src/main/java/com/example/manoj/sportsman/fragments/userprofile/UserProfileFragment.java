package com.example.manoj.sportsman.fragments.userprofile;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.tasks.core.GeneralCallTaskCallback;
import com.example.manoj.sportsman.tasks.core.GetCallAsyncTask;

/**
 * Created by manoj on 31/12/15.
 */
public class UserProfileFragment extends BaseProfileFragment {

    private SignInResponse userProfileData;
    private GetCallAsyncTask<SignInResponse> task;
    private static String USER_ID_KEY = "user_id_key";
    private String userId;

    public static UserProfileFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(USER_ID_KEY, userId);
        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.player_profile_fragment_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(USER_ID_KEY);
        }
        if (userProfileData == null) {
            task = new GetCallAsyncTask<>(getUrl(), SignInResponse.class, new GeneralCallTaskCallback<SignInResponse>() {
                @Override
                public void onSuccess(SignInResponse model) {
                    task = null;
                    userProfileData = model;
                    updateView();
                }

                @Override
                public void onFailure() {
                    task = null;
                }
            });
            task.execute();
        }
    }

    @Override
    protected SignInResponse getProfileData() {
        return userProfileData;
    }

    @Override
    protected void initViews(View baseView) {
        initBasicProfileViews(baseView);
        initFavGameCard(baseView);

        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        toolbar.setTitle("Player Profile");
    }

    private void updateView() {
        initBasicProfileViews(getBaseView());
        refreshFavGameCard();
        initPlayerOtherInfo(getBaseView());
    }

    private void initPlayerOtherInfo(View baseView) {
        if (userProfileData == null) return;
        EditText collageName = (EditText) baseView.findViewById(R.id.collage_name);
        EditText currentLocation = (EditText) baseView.findViewById(R.id.current_location);

        collageName.setText(getProfileData().getCollageName());
        if (getProfileData().getCurrentLocation() != null) {
            currentLocation.setText(getProfileData().getCurrentLocation().getAddressDetail());
        }
    }

    public String getUrl() {
        return Constants.BASE_URL + "/login/get_user_detail?id=" + userId;
    }

    @Override
    public void onDestroyView() {
        if (task != null) {
            task.unregisterCallback();
        }
        super.onDestroyView();
    }
}
