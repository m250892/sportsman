package com.example.manoj.sportsman.fragments.userprofile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.utils.locationmanager.AddressSuggestionRequestController;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by manoj on 28/12/15.
 */
public class UPLocationSuggestionAdpater extends BaseAdapter implements Filterable, AddressSuggestionRequestController.LocationResquestControllerCallback {

    private static final int MAX_RESULTS = 5;
    private Context mContext;
    private List<SearchItemModel> resultList = new ArrayList<SearchItemModel>();

    public UPLocationSuggestionAdpater(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public int getCount() {
        return Math.min(MAX_RESULTS, resultList.size());
    }

    @Override
    public SearchItemModel getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(resultList.get(position).getAddressDetail());
        return convertView;
    }

    public void setResultList(List<SearchItemModel> resultList) {
        if (resultList == null) {
            this.resultList = new ArrayList<>();
        } else {
            this.resultList = resultList;
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<SearchItemModel> searchItemModels = findLocation(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = searchItemModels;
                    filterResults.count = searchItemModels.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<SearchItemModel>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    /**
     * Returns a search result for the given book title.
     */
    private List<SearchItemModel> findLocation(String locationName) {
        AddressSuggestionRequestController.getInstance().getSuggestionAreas(locationName, this);
        return resultList;
    }

    @Override
    public void onSuggestionLoaded(List results, String inputText) {
        setResultList(results);
    }

    @Override
    public void onCurrentLocationAddressLoaded(SearchItemModel searchItemModel) {

    }
}
