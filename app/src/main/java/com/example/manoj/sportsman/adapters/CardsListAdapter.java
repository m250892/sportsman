package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.fragments.listview.CardsListDataStore;
import com.example.manoj.sportsman.fragments.listview.CardsListDataStructure;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewSetter;
import com.example.manoj.sportsman.fragments.listview.cards.core.IChildViewClickListener;
import com.example.manoj.sportsman.fragments.listview.cards.core.ListCardViewFactory;
import com.example.manoj.sportsman.models.CardType;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

/**
 * Created by manoj on 03/11/15.
 */
public class CardsListAdapter extends BaseListAdapter {

    private CardsListType cardsListType;
    private IChildViewClickListener childViewClickListener;

    public CardsListAdapter(Context context, CardsListType cardsListType, IChildViewClickListener childViewClickListener) {
        super(context);
        this.cardsListType = cardsListType;
        this.childViewClickListener = childViewClickListener;
    }

    @Override
    public int getViewTypeCount() {
        return CardType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getCardType().ordinal();
    }

    @Override
    public int getCount() {
        return getDataStore().getCardsCount();
    }

    private CardsListDataStructure getDataStore() {
        return CardsListDataStore.getInstance().getGameDataStructure(cardsListType);
    }

    @Override
    public BaseCardModel getItem(int position) {
        return getDataStore().getCard(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseListCardViewSetter viewSetter = ListCardViewFactory
                .getListCardViewSetter(getItem(position), getContext(), childViewClickListener,
                        position);
        return viewSetter.getView(convertView, parent);
    }


}
