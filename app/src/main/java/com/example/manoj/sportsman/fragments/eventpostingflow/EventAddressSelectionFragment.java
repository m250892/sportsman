package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manoj.sportsman.IFragmentController;
import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;
import com.example.manoj.sportsman.utils.IPermissionsCallback;
import com.example.manoj.sportsman.utils.locationmanager.AddressSuggestionRequestController;
import com.example.manoj.sportsman.utils.locationmanager.LocationChangeListener;

import java.util.List;

/**
 * Created by manoj on 27/11/15.
 */
public class EventAddressSelectionFragment extends BaseFragment implements LocationChangeListener, AddressSuggestionRequestController.LocationResquestControllerCallback {
    private View selectedLocationLayoutView;
    private TextView selectedLocationTextView;
    private View navigationNextView;
    private Location currentLocation;
    private boolean isCurrentLocationRequestInProgress;
    private View currentLocationArrow;
    private View currentLocationProgress;

    public static EventAddressSelectionFragment newInstance() {
        return new EventAddressSelectionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_address_selection_fragment_layout, container, false);
        baseView.findViewById(R.id.get_current_location_view).setOnClickListener(this);
        baseView.findViewById(R.id.pick_a_location_view).setOnClickListener(this);
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setToolbar(toolbar);
        navigationNextView = baseView.findViewById(R.id.navigation_next_button);
        navigationNextView.setOnClickListener(this);
        selectedLocationLayoutView = baseView.findViewById(R.id.selected_location_view);
        selectedLocationTextView = (TextView) baseView.findViewById(R.id.selected_location_text);

        currentLocationProgress = baseView.findViewById(R.id.current_location_progress_bar);
        currentLocationArrow = baseView.findViewById(R.id.current_location_arrow);
        return baseView;
    }

    public void onResume() {
        super.onResume();
        handleLocationSelectedView();
    }

    private void handleLocationSelectedView() {
        if (EventCreateDataModel.getInstance().isLocalitySelected()) {
            selectedLocationLayoutView.setVisibility(View.VISIBLE);
            selectedLocationTextView.setText(EventCreateDataModel.getInstance().getSearchItemModel().getAddressDetail());
        } else {
            selectedLocationLayoutView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        MainApplication.printLog("onClick in fragment");
        switch (v.getId()) {
            case R.id.get_current_location_view:
                fetchCurrentLocation();
                break;
            case R.id.pick_a_location_view:
                getFragmentController().performOperation(IFragmentController.LOCATION_SELECTOR_FRAGMENT, null);
                break;
            case R.id.navigation_next_button:
                launchNextFragment();
                break;
        }
    }

    private void launchNextFragment() {
        if (EventCreateDataModel.getInstance().isLocalitySelected()) {
            getFragmentController().performOperation(IFragmentController.CREATE_EVENT_MORE_OPTIONS_FRAGMENT, null);
        }
    }

    public void fetchCurrentLocation() {
        Log.d("manoj", "fetchCurrentLocation called : " + isCurrentLocationRequestInProgress);
        getFragmentController().requestPermission(IPermissionsCallback.GET_LOCATION, new IPermissionsCallback() {
            @Override
            public void onPermissionRequestResult(int requestCode, boolean result) {
                if (result) {
                    if (!isCurrentLocationRequestInProgress) {
                        isCurrentLocationRequestInProgress = true;
                        getFragmentController().fetchCurrentLocation(EventAddressSelectionFragment.this);
                        updateCurrentLocationProgressBar(true);
                    }
                } else {
                    showToastMessage("Please provide permission to use location!!");
                }
            }
        });
    }

    private void updateCurrentLocationProgressBar(boolean isProgressBarVisible) {
        if (isProgressBarVisible) {
            currentLocationArrow.setVisibility(View.GONE);
            currentLocationProgress.setVisibility(View.VISIBLE);
        } else {
            currentLocationArrow.setVisibility(View.VISIBLE);
            currentLocationProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLocationChange(Location location) {
        Log.d("manoj", "onLocationChange called : " + isCurrentLocationRequestInProgress);
        if (isCurrentLocationRequestInProgress) {
            if (location != null) {
                currentLocation = location;
                AddressSuggestionRequestController.getInstance().getCurrentLocationAddress(location, this);
            } else {
                Log.d("manoj", "getting null location, maybe gps is not enabled");
                isCurrentLocationRequestInProgress = false;
            }
        }
    }

    @Override
    public void onDestroyView() {
        getFragmentController().unregisterLocationChangeListenerCallback();
        super.onDestroyView();
    }

    @Override
    public void onSuggestionLoaded(List results, String inputText) {
    }

    @Override
    public void onCurrentLocationAddressLoaded(SearchItemModel searchItemModel) {
        if (isCurrentLocationRequestInProgress) {
            updateCurrentLocationProgressBar(false);
            isCurrentLocationRequestInProgress = false;
            EventCreateDataModel.getInstance().setSearchItemModel(searchItemModel);
            handleLocationSelectedView();
        }
    }
}

