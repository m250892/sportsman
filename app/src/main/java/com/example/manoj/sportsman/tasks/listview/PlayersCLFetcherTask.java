package com.example.manoj.sportsman.tasks.listview;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.models.listview.BaseCardModel;
import com.example.manoj.sportsman.models.listview.PlayerCardModel;
import com.example.manoj.sportsman.models.userprofile.CardsListType;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 25/12/15.
 */
public class PlayersCLFetcherTask extends BaseCLFetcherTask {

    public PlayersCLFetcherTask(FetchCardListTaskCallback callback, CardsListType cardsListType) {
        super(callback, cardsListType);
    }

    @Override
    protected String getBaseRequestUrl() {
        return Constants.BASE_URL + "/login/get";
    }

    @Override
    protected List<BaseCardModel> fetchData(String flatDetailsRequest) throws Exception {
        ServerResponse<PlayersCLResponseWrapper> serverResponse = NetworkUtils
                .doGetCall(flatDetailsRequest, PlayersCLResponseWrapper.class);
        return parseResponse(serverResponse);
    }

    private List<BaseCardModel> parseResponse(ServerResponse<PlayersCLResponseWrapper> serverResponse) {
        if (null != serverResponse) {
            PlayersCLResponseWrapper data = serverResponse.getData();
            List<BaseCardModel> result = new ArrayList<>();
            result.addAll(data.getObjectList());
            return result;
        }
        return null;
    }

    private class PlayersCLResponseWrapper extends CardListResponseWrapper<PlayerCardModel> {
    }
}
