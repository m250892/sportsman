package com.example.manoj.sportsman;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.example.manoj.sportsman.utils.SharedPrefsManager;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by manoj on 25/11/15.
 */
public class MainApplication extends android.app.Application {


    private static MainApplication instance;
    private static Handler sMainThreadHandler;

    public MainApplication() {
        instance = this;
    }

    public static MainApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        Fresco.initialize(getContext());
        SharedPrefsManager.initialize(getApplicationContext());
        super.onCreate();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static void printLog(String text) {
        if (text != null) {
            Log.d("manoj", text);
        }
    }

}
