package com.example.manoj.sportsman.fragments.eventpostingflow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;
import com.example.manoj.sportsman.models.EventCreateDataModel;

/**
 * Created by manoj on 29/11/15.
 */
public class EventAddDesriptionFragment extends BaseFragment {


    private EditText descriptionTextView;

    public static BaseFragment newInstance() {
        return new EventAddDesriptionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.event_add_desription_layout, container, false);
        descriptionTextView = (EditText) baseView.findViewById(R.id.event_desription_text);
        baseView.findViewById(R.id.event_save_desription_button).setOnClickListener(this);
        Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
        setUpToolbar(toolbar);
        return baseView;
    }

    public void setUpToolbar(Toolbar toolbar) {
        setToolbar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(true);
        getFragmentController().getSupportActionBarM().setTitle("Event Description");
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivityReference().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        descriptionTextView.requestFocus();
        showSoftKeyBoard(descriptionTextView);
        String text = EventCreateDataModel.getInstance().getEventDescription();
        if (text != null) {
            descriptionTextView.setText(text);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.event_save_desription_button:
                handleSaveButtonClick();
                break;
        }
    }

    private void handleSaveButtonClick() {
        String text = null;
        if (descriptionTextView.getText() != null) {
            text = descriptionTextView.getText().toString();
        }
        EventCreateDataModel.getInstance().setEventDescription(text);
        getFragmentController().onBackPressed();
    }

    @Override
    public void onDestroyView() {
        getActivityReference().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        hideSoftKeyBoard(descriptionTextView);
        super.onDestroyView();
    }


}
