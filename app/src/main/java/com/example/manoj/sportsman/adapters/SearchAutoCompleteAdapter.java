package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;

import java.util.List;

/**
 * Created by manoj on 31/10/15.
 */
public class SearchAutoCompleteAdapter extends BaseListAdapter {

    private List<SearchItemModel> dataList;

    public SearchAutoCompleteAdapter(Context context, List<SearchItemModel> dataList) {
        super(context);
        this.dataList = dataList;
    }

    public void updateListItems(List<SearchItemModel> newList) {
        this.dataList.clear();
        if (newList != null) {
            this.dataList = newList;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public SearchItemModel getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = getLayoutView(R.layout.search_item);
        TextView areaName = (TextView) convertView.findViewById(R.id.area_name);
        areaName.setText(dataList.get(position).getAddressDetail());
        return convertView;
    }

}
