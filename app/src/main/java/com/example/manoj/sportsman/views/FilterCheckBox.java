package com.example.manoj.sportsman.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class FilterCheckBox extends CheckBox {

    private static final String FONT_NAME = "fonts/Roboto-Regular.ttf";

    public FilterCheckBox(Context context) {
        super(context);
        init();
    }

    public FilterCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FilterCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FilterCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        Typeface myTypeface = FontCache.get(FONT_NAME, getContext());
        setTypeface(myTypeface);
    }
}
