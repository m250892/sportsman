package com.example.manoj.sportsman.fragments.listview;

import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.HashMap;

/**
 * Created by manoj on 20/12/15.
 */
public class CardsListDataStore {

    private static CardsListDataStore instance;
    private static HashMap<CardsListType, CardsListDataStructure> CLTDSHashMap;

    private CardsListDataStore() {
        CLTDSHashMap = new HashMap<>();
    }

    public synchronized static CardsListDataStore getInstance() {
        if (null == instance) {
            instance = new CardsListDataStore();
        }
        return instance;
    }

    public void clearDataForGame(CardsListType cardsListType) {
        if (CLTDSHashMap.containsKey(cardsListType)) {
            CLTDSHashMap.get(cardsListType).clearCardList();
        }
    }

    public void deleteDataForGame(CardsListType cardsListType) {
        if (CLTDSHashMap.containsKey(cardsListType)) {
            CLTDSHashMap.remove(cardsListType);
        }
    }

    public CardsListDataStructure getGameDataStructure(CardsListType cardsListType) {
        if (!CLTDSHashMap.containsKey(cardsListType)) {
            CLTDSHashMap.put(cardsListType, new CardsListDataStructure(cardsListType));
        }
        return CLTDSHashMap.get(cardsListType);
    }

    public void clearDataStore() {
        CLTDSHashMap.clear();
        CLTDSHashMap = null;
        instance = null;
    }

}
