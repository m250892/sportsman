package com.example.manoj.sportsman.models.listview;

import com.example.manoj.sportsman.models.CardType;

/**
 * Created by manoj on 24/12/15.
 */
public class CoachCardModel extends BaseCardModel {

    @Override
    public CardType getCardType() {
        return CardType.COACH_CARD;
    }

    @Override
    public String getId() {
        return null;
    }
}
