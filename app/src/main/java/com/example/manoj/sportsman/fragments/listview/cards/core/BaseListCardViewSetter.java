package com.example.manoj.sportsman.fragments.listview.cards.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.models.listview.BaseCardModel;

public abstract class BaseListCardViewSetter<BCM extends BaseCardModel, CVH extends BaseListCardViewHolder>
        extends BaseCardViewSetter {

    private BCM baseCardModel;
    protected IChildViewClickListener childViewClickListener;

    public BaseListCardViewSetter(BCM baseCardModel, Context context,
                                  IChildViewClickListener childViewClickListener) {
        super(context);
        this.baseCardModel = baseCardModel;
        this.childViewClickListener = childViewClickListener;
    }

    @Override
    public View getView(View convertView, ViewGroup parent) {
        CVH holder = null;
        if (null == convertView) {
            convertView = buildView(LayoutInflater.from(getContext()), parent);
            holder = buildViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CVH) convertView.getTag();
        }
        if (holder != null) {
            setViews(holder);
        }
        return convertView;
    }

    protected abstract void setViews(CVH holder);

    protected abstract View buildView(LayoutInflater inflater, ViewGroup parent);

    protected abstract CVH buildViewHolder(View cardview);

}
