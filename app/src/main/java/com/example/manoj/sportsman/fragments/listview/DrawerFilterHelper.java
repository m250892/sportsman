package com.example.manoj.sportsman.fragments.listview;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.filters.FilterFactory;
import com.example.manoj.sportsman.filters.FilterManager;
import com.example.manoj.sportsman.filters.core.BaseFilterGroup;
import com.example.manoj.sportsman.filters.filterhash.BaseFilterHash;
import com.example.manoj.sportsman.fragments.BaseListHolderFragment;
import com.example.manoj.sportsman.utils.EventManager;
import com.example.manoj.sportsman.utils.events.DrawerOpenEvent;

import java.util.List;

/**
 * Created by manoj on 07/01/16.
 */
public abstract class DrawerFilterHelper extends BaseListHolderFragment {
    private View frame;
    protected LinearLayout filterLayout;
    private View mDrawerListRight;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private float lastTranslate = 0.0f;

    private BaseFilterHash copidFilterHash;
    private List<BaseFilterGroup> filterGroupList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        copidFilterHash = FilterManager.getInstance().getFilterHash();
        filterGroupList = FilterFactory.getFilters(getActivity(), getCurrentListType());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventManager.getDefaultEventBus().register(this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem menuItem = menu.add(0, 1, 1, "Filter");
        menuItem.setIcon(R.drawable.ic_filter);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                drawerLayout.openDrawer(mDrawerListRight);
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void initView(View baseView) {
        super.initView(baseView);
        frame = baseView.findViewById(R.id.view_layout);
        drawerLayout = (DrawerLayout) baseView.findViewById(R.id.drawer_layout);
        mDrawerListRight = baseView.findViewById(R.id.navRight);
        filterLayout = (LinearLayout) baseView.findViewById(R.id.filter_layout);
        baseView.findViewById(R.id.reset_button).setOnClickListener(this);
        baseView.findViewById(R.id.apply_button).setOnClickListener(this);
        updateFilterDrawer();
        updateFilterGroupsWithFilterHash();
        setupDrawer();
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(DrawerOpenEvent event) {
        Log.d("manoj", "Drawer open event in fragment");
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
    }


    protected void updateFilterDrawer() {
        copidFilterHash = FilterManager.getInstance().getFilterHash();
        filterGroupList = FilterFactory.getFilters(getActivity(), FilterManager.getInstance().getService());
        addFilters(getFilterGroup());
        updateFilterGroupsWithFilterHash();
    }

    protected List<BaseFilterGroup> getFilterGroup() {
        return filterGroupList;
    }

    private void addFilters(List<BaseFilterGroup> filterGroups) {
        LinearLayout.LayoutParams
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int dimen_16 = activity.getBaseContext().getResources().getDimensionPixelSize(R.dimen.dimen_16);
        params.setMargins(0, dimen_16, 0, 0);
        filterLayout.removeAllViews();
        for (BaseFilterGroup item : filterGroups) {
            filterLayout.addView(item.getView(), params);
        }
    }

    public void updateFilterHashWithFilterGroups() {
        for (BaseFilterGroup filterGroup : getFilterGroup()) {
            filterGroup.writeToFilterHash(getCopiedFilterHash());
        }
    }

    public void updateFilterGroupsWithFilterHash() {
        for (BaseFilterGroup filterGroup : getFilterGroup()) {
            filterGroup.readFromFilterHash(getCopiedFilterHash());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.apply_button:
                onFilterApply();
                break;
            case R.id.reset_button:
                onFilterReset();
                break;
            default:
                super.onClick(v);
        }
    }

    private void onFilterReset() {
        Log.d("manoj", "On Filter reset button click");
        getCopiedFilterHash().reset();
        updateFilterGroupsWithFilterHash();
    }

    private void onFilterApply() {
        updateFilterHashWithFilterGroups();
        getCopiedFilterHash().printFilters();
        if (getFragmentController() != null) {
            getFragmentController().onBackPressed();
        }
    }

    public boolean handleBackPress() {
        if (drawerLayout.isDrawerOpen(mDrawerListRight)) {
            drawerLayout.closeDrawer(mDrawerListRight);
            return true;
        }
        return false;
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, android.R.drawable.ic_dialog_alert, R.string.drawer_open, R.string.drawer_close) {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = -1 * (mDrawerListRight.getWidth() * slideOffset);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    frame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    frame.startAnimation(anim);
                    lastTranslate = moveFactor;
                }
            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onDestroyView() {
        EventManager.getDefaultEventBus().unregister(this);
        updateFilterHashWithFilterGroups();
        super.onDestroyView();
    }

    public BaseFilterHash getCopiedFilterHash() {
        return copidFilterHash;
    }
}
