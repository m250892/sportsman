package com.example.manoj.sportsman.models;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.utils.Utils;

import java.util.Calendar;

/**
 * Created by manoj on 27/11/15.
 */
public class DateTimeModel {
    private int day = -1;
    private int month = -1;
    private int year = -1;
    private int hour = -1;
    private int minutes = -1;
    private int dayOfWeek = -1;
    private long timestamp = -1;


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }


    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDateSelected() {
        if (day == -1 || month == -1 || year == -1) {
            return false;
        }
        return true;
    }

    public boolean isTimeSelected() {
        if (hour == -1 || minutes == -1) {
            return false;
        }
        return true;
    }

    public void setDate(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public void setTime(int hourOfDay, int minute) {
        setHour(hourOfDay);
        setMinutes(minute);
    }

    public String getDayMonthString() {
        if (!isDateSelected()) {
            return "";
        }
        return Utils.getDateMonthFormatString(getDay(), getMonth());
    }
    public String getDateStringFormat() {
        if (!isDateSelected()) {
            return "";
        }
        return Utils.getDateMonthYearFormatString(getDay(), getMonth(), getYear());
    }


    public String getTimeStringFormat() {
        if (!isTimeSelected()) {
            return "";
        }
        return Utils.getTimeString(getHour(), getMinutes());
    }

    public String getDateTimeFormatedString() {
        String timeString = getTimeStringFormat();
        String dateString = getDateStringFormat();
        Calendar calendar = Calendar.getInstance();
        calendar.set(getYear(), getMonth(), getDay());
        String dayName = Constants.getDayShortName(calendar.get(Calendar.DAY_OF_WEEK) - 1);
        String result = dateString + ", " + dayName + " at " + timeString;
        return result;
    }

    public long getEpochTimeInSec() {
        if (timestamp == -1 && isDateSelected() && isTimeSelected()) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day, hour, minutes);
            timestamp = calendar.getTimeInMillis() / 1000;
        }
        return timestamp;
    }
}
