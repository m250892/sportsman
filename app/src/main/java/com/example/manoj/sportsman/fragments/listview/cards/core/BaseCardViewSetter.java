package com.example.manoj.sportsman.fragments.listview.cards.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseCardViewSetter {

    private Context context;
    private LayoutInflater inflater;

    public BaseCardViewSetter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public Context getContext() {
        return context;
    }

    public abstract View getView(View convertView, ViewGroup parent);

    public LayoutInflater getInflater() {
        return inflater;
    }

    public View getLayoutView(int id) {
        return getInflater().inflate(id, null);
    }
}
