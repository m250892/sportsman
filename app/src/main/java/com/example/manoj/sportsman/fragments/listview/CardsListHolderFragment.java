package com.example.manoj.sportsman.fragments.listview;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.adapters.EventsViewPagerAdapter;
import com.example.manoj.sportsman.filters.FilterManager;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

/**
 * Created by manoj on 25/11/15.
 */
public class CardsListHolderFragment extends DrawerFilterHelper implements ViewPager.OnPageChangeListener {

    private EventsViewPagerAdapter pagerAdapter;
    private CardsListType currentListType = CardsListType.EVENTS_LIST;
    private static final String INPUT_KEY = "input_key";

    public static CardsListHolderFragment newInstance(CardsListType input) {
        Bundle args = new Bundle();
        args.putString(INPUT_KEY, input.getName());
        CardsListHolderFragment fragment = new CardsListHolderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentListType = CardsListType.fromString(getArguments().getString(INPUT_KEY));
            FilterManager.getInstance().setService(currentListType);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (null != pagerAdapter) {
            pagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void setViewPagerAdapter() {
        pagerAdapter = new EventsViewPagerAdapter(getChildFragmentManager(), Constants.getAvailableGameIds());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(pagerAdapter.getCardsListPositionInViewPager(currentListType));
        viewPager.setOnPageChangeListener(this);
    }

    protected CardsListType getCurrentListType() {
        return currentListType;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        currentListType = pagerAdapter.getCardsListType(position);
        FilterManager.getInstance().setService(currentListType);
        updateFilterDrawer();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    protected int getViewId() {
        return R.layout.base_list_holder_fragment_layout;
    }

    @Override
    public void onDestroyView() {
        CardsListDataStore.getInstance().clearDataStore();
        super.onDestroyView();
    }
}
