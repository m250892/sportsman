package com.example.manoj.sportsman.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.models.userprofile.SignInResponse;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by manoj on 11/12/15.
 */
public class NewUserLoginTask extends AsyncTask {


    private JSONObject userData;
    private ServerLoginCallback callback;

    public NewUserLoginTask(JSONObject userData, ServerLoginCallback callback) {
        this.userData = userData;
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            ServerResponse response = NetworkUtils.doPostCall(getUrl(), getJsonData(), SignInResponse.class);
            if (response != null) {
                return response.getData();
            }
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object object) {
        Log.d("manoj", "user log response");
        if (callback != null) {
            if (object != null && object instanceof SignInResponse) {
                SignInResponse signInResponse = (SignInResponse) object;
                Gson gson = new Gson();
                Log.d("manoj", "Reponse Data : " + gson.toJson(signInResponse));
                callback.onServerLoginSuccess(signInResponse);
            } else {
                callback.onServerLoginFailed("login failed");
            }
        }
    }

    public String getUrl() {
        String url = Constants.BASE_URL + "/login/create";
        return url;
    }

    public JSONObject getJsonData() {
        return userData;
    }

    public interface ServerLoginCallback {

        void onServerLoginSuccess(SignInResponse signInResponse);

        void onServerLoginFailed(String message);
    }
}
