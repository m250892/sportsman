package com.example.manoj.sportsman.tasks.core;

import com.example.manoj.sportsman.utils.Model;

public interface GeneralCallTaskCallback<M extends Model> {
    void onSuccess(M model);

    void onFailure();
}