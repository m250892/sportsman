package com.example.manoj.sportsman.utils.loginhelper;

import com.example.manoj.sportsman.models.userprofile.SignInResponse;

/**
 * Created by manoj on 07/12/15.
 */
public interface SignInRequestCallback {

    void onSignInSuccess(String SignInMethod, SignInResponse signInResponse);

    void onSignInFailed(String SignInMethod, String message);

}
