package com.example.manoj.sportsman.models.userprofile.gplusloginresponse;

public class AgeRange {
    private String min;

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "ClassPojo [min = " + min + "]";
    }
}
