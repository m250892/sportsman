package com.example.manoj.sportsman.tasks;

import android.os.AsyncTask;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.models.EventModel;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

/**
 * Created by manoj on 28/12/15.
 */
public class EventGetDetailTask extends AsyncTask<Void, Void, EventModel> {

    private EventGetDetailCallback callback;
    private String eventId;

    public EventGetDetailTask(String eventId, EventGetDetailCallback callback) {
        this.callback = callback;
        this.eventId = eventId;
    }

    @Override
    protected EventModel doInBackground(Void[] params) {
        ServerResponse<EventModel> response = null;
        try {
            response = NetworkUtils.doGetCall(getUrl(), EventModel.class);
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        }
        if (response != null) {
            return response.getData();
        }
        return null;
    }

    @Override
    protected void onPostExecute(EventModel eventModel) {
        if (callback != null) {
            if (null != eventModel) {
                callback.onDataFetchSuccess(eventModel);
            } else {
                callback.onDataFailed();
            }
        }
    }

    public String getUrl() {
        return Constants.BASE_URL + "/create_event/event?id=" + eventId;
    }

    public interface EventGetDetailCallback {
        void onDataFetchSuccess(EventModel eventModel);

        void onDataFailed();
    }
}
