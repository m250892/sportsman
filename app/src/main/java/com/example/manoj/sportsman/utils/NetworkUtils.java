package com.example.manoj.sportsman.utils;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class NetworkUtils {

    public static final String HEADER_APP_NAME = "app_name";
    public static final String HEADER_APP_VERSION = "app_version";
    public static final String HEADER_LOGIN_AUTH_TOKEN = "login_auth_token";
    public static final String CONTENT_TYPE_TAG = "Content-Type";
    public static final String HTTP_HEADER_JSON = "application/json";
    public static final String HTTP_HEADER_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String ENCODING = "charset=utf-8";
    public static final long CONNECTION_TIMEOUT_GET_VALUE = 15;
    public static final long READ_TIMEOUT_GET_VALUE = 15;
    public static final long CONNECTION_TIMEOUT_POST_VALUE = 20;
    public static final long READ_TIMEOUT_POST_VALUE = 20;
    private static final MediaType JSON = MediaType.parse(HTTP_HEADER_JSON + "; " +
            ENCODING);
    private static final MediaType URLENCODED = MediaType.parse(HTTP_HEADER_FORM_URLENCODED + "; " +
            ENCODING);
    public static final String UTF_8 = "UTF-8";
    public static final String ZERO = "0";
    public static final String MD5 = "MD5";

    public static <T> ServerResponse<T> doGetCall(String url, Class<T> responseClass)
            throws NetworkException {
        Response response = getResponseForGetCall(url);

        T responseObject = parseFromResponseToClass(response, responseClass);
        return new ServerResponse<T>(responseObject, response.message(), response.code());
    }


    private static Response getResponseForGetCall(String url) throws NetworkException {
        return getResponseForGetCall(url, null);
    }

    private static Response getResponseForGetCall(String url, Map<String, String> headerParams)
            throws NetworkException {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("Empty URL " + url);
        }
        Log.d("Network", "Get Call : " + url);
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(CONNECTION_TIMEOUT_GET_VALUE, TimeUnit.SECONDS);
        client.setReadTimeout(READ_TIMEOUT_GET_VALUE, TimeUnit.SECONDS);

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);
        if (headerParams != null) {
            for (String key : headerParams.keySet()) {
                requestBuilder.addHeader(key, headerParams.get(key));
            }
        }
        addApplicationHeaders(requestBuilder);
        Request request = requestBuilder.build();
        Call call = client.newCall(request);
        Response response;
        try {
            Log.d("network", "Url for GET request: " + url);
            response = call.execute();
            if (response.isSuccessful()) {
                return response;
            } else {
                String messageFromServer = response.message();
                Log.d("network", response.message());
                try {
                    ServerErrorResponse errorObject = parseFromResponseToClass(response,
                            ServerErrorResponse.class);
                    if (errorObject != null) {
                        //Update message from server
                        messageFromServer = errorObject.getMessage();
                    }
                } catch (IllegalStateException ise) {
                    Log.e("network", ise + "IllegalStateException thrown while trying to parse error " +
                            "response");
                } catch (JsonSyntaxException jse) {
                    Log.e("network", jse + "JsonSyntaxException thrown while trying to parse error " +
                            "response");
                }

                String errorMessage = String
                        .format("GET: Response is not successful, HTTP_STATUS_CODE: %d, message: %s",
                                response.code(), messageFromServer);
                NetworkException.NetworkExceptionBuilder nBuilder
                        = new NetworkException.NetworkExceptionBuilder();
                nBuilder.errorMessage(errorMessage);
                nBuilder.httpStatusCode(response.code());
                nBuilder.messageFromServer(messageFromServer);
                throw nBuilder.createNetworkException();
            }
        } catch (IOException e) {
            NetworkException.NetworkExceptionBuilder nBuilder
                    = new NetworkException.NetworkExceptionBuilder();
            nBuilder.errorMessage("IOException while executing GET request - " + e.getMessage());
            nBuilder.cause(e);
            throw nBuilder.createNetworkException();
        }
    }

    public static <T> ServerResponse<T> doPostCall(String url, JSONObject jsonData,
                                                   Class<T> responseClass) throws NetworkException {
        return doPostCall(url, jsonData, responseClass, null);
    }

    public static <T> ServerResponse<T> doPostCall(String url, JSONObject jsonData,
                                                   Class<T> responseClass,
                                                   Map<String, String> headers)
            throws NetworkException {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("Empty URL " + url);
        }

        if (responseClass == null) {
            responseClass = (Class<T>) Object.class;
        }

        RequestBody body;
        if (jsonData != null) {
            body = RequestBody.create(JSON, jsonData.toString());
        } else {
            body = RequestBody.create(JSON, "");
        }

        Log.d("manoj", "Url for POST request: " + url);
        Log.d("manoj", "JSON data for request:\n " + jsonData.toString());
        Request.Builder requestBuilder = new Request.Builder().url(url).header(CONTENT_TYPE_TAG,
                HTTP_HEADER_JSON)
                .post(body);
        addApplicationHeaders(requestBuilder);
        if (headers != null) {
            for (String headerKey : headers.keySet()) {
                String headerValue = headers.get(headerKey);
                if (!TextUtils.isEmpty(headerValue)) {
                    requestBuilder.addHeader(headerKey, headerValue);
                }
            }
        }
        Request request = requestBuilder.build();
        return postRequestAndHandleResponse(request, responseClass);
    }

    private static void addApplicationHeaders(Request.Builder builder) {
        builder.addHeader("Content-Type", "application/json");
        builder.addHeader("X-Parse-Application-Id", "3W1OcLypp4WZXZ4fNwaR6vnHB6MHJTtbM6Tniu2N");
        builder.addHeader("X-Parse-REST-API-Key", "U12BYUSOyhVn80SrQ1AEIgYQzYTh5TNr7AMKUsOA");
    }

    private static <T> ServerResponse<T> postRequestAndHandleResponse(Request request,
                                                                      Class<T> responseClass)
            throws NetworkException {

        Response response = null;
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(CONNECTION_TIMEOUT_POST_VALUE, TimeUnit.SECONDS);
        client.setReadTimeout(READ_TIMEOUT_POST_VALUE, TimeUnit.SECONDS);
        Call call = client.newCall(request);

        try {
            response = call.execute();
            if (response.isSuccessful()) {
                T responseObject = parseFromResponseToClass(response, responseClass);
                return new ServerResponse<T>(responseObject, response.message(), response.code());
            }

            String messageFromServer = response.message();
            try {
                ServerErrorResponse errorObject = parseFromResponseToClass(response,
                        ServerErrorResponse.class);
                if (errorObject != null) {
                    //Update message from server
                    messageFromServer = errorObject.getMessage();
                }
            } catch (IllegalStateException ise) {
                Log.e("network", "Illegal state exception thrown while trying to parse error " + "response" + ise);
            } catch (JsonSyntaxException jse) {
                Log.e("network", "Illegal state exception thrown while trying to parse error " + "response" +
                        jse);
            }

            String errorMessage = String
                    .format("POST: Response is not successful, HTTP_STATUS_CODE: %d, message: %s",
                            response.code(), messageFromServer);
            NetworkException.NetworkExceptionBuilder nBuilder
                    = new NetworkException.NetworkExceptionBuilder();
            nBuilder.errorMessage(errorMessage);
            nBuilder.httpStatusCode(response.code());
            nBuilder.messageFromServer(messageFromServer);
            throw nBuilder.createNetworkException();
        } catch (IOException ioe) {
            NetworkException.NetworkExceptionBuilder nBuilder
                    = new NetworkException.NetworkExceptionBuilder();
            nBuilder.errorMessage("Post request failed due to - " + ioe.getMessage());
            nBuilder.cause(ioe);
            throw nBuilder.createNetworkException();
        } catch (JsonParseException jpe) {
            NetworkException.NetworkExceptionBuilder nBuilder
                    = new NetworkException.NetworkExceptionBuilder();
            nBuilder.errorMessage("GSON: Parsing response failed due to - " + jpe.getMessage());
            nBuilder.cause(jpe);
            throw nBuilder.createNetworkException();
        }
    }

    private static <T> T parseFromResponseToClass(Response response, Class<T> tClass)
            throws NetworkException {
        Gson gson = new Gson();
        if (tClass == null) {
            tClass = (Class<T>) Object.class;
        }
        try {
            return (gson.fromJson(response.body().charStream(), tClass));
        } catch (Exception e) {
            throw wrapGsonExceptionIntoNetworkException(e);
        }
    }

    private static NetworkException wrapGsonExceptionIntoNetworkException(Exception e) {
        String errorMessage = String.format("Exception occurred while parsing success " +
                        "response from server through GSON: %s",
                e.getMessage());
        NetworkException.NetworkExceptionBuilder nBuilder
                = new NetworkException.NetworkExceptionBuilder();
        nBuilder.errorMessage(errorMessage);
        nBuilder.httpStatusCode(HttpURLConnection.HTTP_OK);
        nBuilder.messageFromServer(errorMessage);
        return nBuilder.createNetworkException();
    }

    public static class NetworkException extends Exception {

        public static final int HTTP_STATUS_NOT_AVAILABLE = -1;
        private String errorMessage;
        private int httpStatusCode;
        private String messageFromServer;

        private NetworkException(Throwable cause, int httpStatusCode, String errorMessage,
                                 String messageFromServer) {
            super(errorMessage, cause);
            this.errorMessage = errorMessage;
            this.httpStatusCode = httpStatusCode;
            this.messageFromServer = messageFromServer;
        }

        @Override
        public String getMessage() {
            return errorMessage;
        }

        public String getMessageFromServer() {
            return messageFromServer;
        }

        public int getHttpStatusCode() {
            return httpStatusCode;
        }

        public static class NetworkExceptionBuilder {

            private String errorMessage;
            private int httpStatusCode = HTTP_STATUS_NOT_AVAILABLE;
            private String messageFromServer;
            private Throwable cause;

            public NetworkExceptionBuilder() {
            }

            public NetworkExceptionBuilder errorMessage(String errorMessage) {
                this.errorMessage = errorMessage;
                return this;
            }

            public NetworkExceptionBuilder httpStatusCode(int httpStatusCode) {
                this.httpStatusCode = httpStatusCode;
                return this;
            }

            public NetworkExceptionBuilder messageFromServer(String messageFromServer) {
                this.messageFromServer = messageFromServer;
                return this;
            }

            public NetworkExceptionBuilder cause(Throwable th) {
                this.cause = th;
                return this;
            }

            public NetworkException createNetworkException() {
                return new NetworkException(cause, httpStatusCode, errorMessage, messageFromServer);
            }
        }
    }

    private class ServerErrorResponse {

        private Object message;

        public String getMessage() {
            if (message == null) {
                return "Couldn't fetch message from responseObject";
            }
            if (message instanceof String) {
                return message.toString();
            } else if (message instanceof List) {
                List<String> messageList = (List<String>) message;
                StringBuilder sb = new StringBuilder();
                for (String message : messageList) {
                    sb.append(message + ", ");
                }
                int messageLength = sb.length();
                if (messageLength >= 2) {
                    sb.delete(messageLength - 2, messageLength - 1);
                }
                return sb.toString();
            } else {
                return "Couldn't fetch message from responseObject";
            }
        }

        public void setMessage(Object messageObject) {
            this.message = messageObject;
        }

    }
}