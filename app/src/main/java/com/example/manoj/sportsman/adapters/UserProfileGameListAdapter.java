package com.example.manoj.sportsman.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;

import java.util.List;

/**
 * Created by manoj on 10/12/15.
 */
public class UserProfileGameListAdapter extends BaseListAdapter {

    private List<UserFavSportModel> data;

    public UserProfileGameListAdapter(Context context, List<UserFavSportModel> data) {
        super(context);
        this.data = data;
        Log.d("manoj", "total list size : " + data.size());
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("manoj", "get view position : " + position);
        if (convertView == null) {
            convertView = getLayoutView(R.layout.user_profile_single_game_view_layout);
        }
        return convertView;
    }
}
