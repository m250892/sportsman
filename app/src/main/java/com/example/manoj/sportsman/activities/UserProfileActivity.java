package com.example.manoj.sportsman.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.userprofile.LoginFragment;
import com.example.manoj.sportsman.fragments.userprofile.EditProfileFragment;
import com.example.manoj.sportsman.fragments.userprofile.UserProfileManager;
import com.example.manoj.sportsman.utils.loginhelper.FbLoginHelper;
import com.example.manoj.sportsman.utils.loginhelper.GPlusLoginHelper;
import com.facebook.FacebookSdk;

public class UserProfileActivity extends BaseActivity {

    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        frameLayout = (FrameLayout) findViewById(R.id.mainFrameLayout);
        initializeFbSdk();
        initializeLoginHelpers();
        loadInitialFragment();
    }

    private void initializeLoginHelpers() {
        GPlusLoginHelper.getInstance().setFragmentActivity(this);
        FbLoginHelper.getInstance().setFragmentActivity(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("manoj", "on Activity result load called in UserProfileAcitvity");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GPlusLoginHelper.GPLUS_SIGN_IN) {
            GPlusLoginHelper.getInstance().onActivityResult(requestCode, resultCode, data);
        } else {
            FbLoginHelper.getInstance().onActivityResult(requestCode, resultCode, data);
        }
    }

    //Initialize facebook sdk
    private void initializeFbSdk() {
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void loadInitialFragment() {
        if (UserProfileManager.isLoggedIn()) {
            addFragmentInDefaultLayout(EditProfileFragment.newInstance(), false);
        } else {
            addFragmentInDefaultLayout(LoginFragment.newInstance(), false);
        }
    }

    @Override
    public void performOperation(int operation, Object input) {
        switch (operation) {
            case EDIT_PROFILE:
                openEditProfile();
                break;
        }
    }

    //Only we come to this option from login fragment so first clear login
    private void openEditProfile() {
        clearBackStack(true);
        replaceFragmentInDefaultLayout(EditProfileFragment.newInstance(), false);
    }

    @Override
    public ViewGroup getFragmentContainer() {
        return frameLayout;
    }

    @Override
    protected void onDestroy() {

        GPlusLoginHelper.getInstance().disconnectGoogleApiClient();
        super.onDestroy();
    }
}
