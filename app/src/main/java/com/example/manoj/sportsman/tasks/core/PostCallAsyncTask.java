package com.example.manoj.sportsman.tasks.core;

import android.os.AsyncTask;

import com.example.manoj.sportsman.utils.Model;
import com.example.manoj.sportsman.utils.NetworkUtils;
import com.example.manoj.sportsman.utils.ServerResponse;

import org.json.JSONObject;

/**
 * Created by manoj on 30/12/15.
 */
public class PostCallAsyncTask<M extends Model> extends AsyncTask<Object, Object, M> {

    private GeneralCallTaskCallback callback;
    private String url;
    private JSONObject jsonData;
    private Class<M> responseClass;

    public PostCallAsyncTask(String url, JSONObject jsonData, Class<M> responseClass, GeneralCallTaskCallback callback) {
        this.callback = callback;
        this.url = url;
        this.jsonData = jsonData;
        this.responseClass = responseClass;
    }


    public void execute() {
        super.execute();
    }

    public PostCallAsyncTask(String url, JSONObject jsonData, GeneralCallTaskCallback callback) {
        this.callback = callback;
        this.url = url;
        this.jsonData = jsonData;
    }


    @Override
    protected M doInBackground(Object... params) {
        try {
            ServerResponse<M> response = NetworkUtils.doPostCall(url, jsonData, responseClass);
            if (response != null) {
                return response.getData();
            }
        } catch (NetworkUtils.NetworkException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(M model) {
        if (callback != null) {
            if (model != null) {
                callback.onSuccess(model);
            } else {
                callback.onFailure();
            }
        }
    }

    public void unregisterCallback() {
        callback = null;
    }

}
