package com.example.manoj.sportsman.utils.customvalueselecter;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by manoj on 27/11/15.
 */
public class TimeSelectorFragment extends BaseFragment implements CustomSelecterCallbacks {

    private ArrayList<CustomSelectionItem> mHoursArrayList;
    public RecyclerView mHourRecycleView;
    private CenterLockListener mHourCenterLock;
    private int mPrevSelectedHourPos = 0;
    public View hourHighlighterView;
    private RecyclerView.Adapter mHoursAdapter;
    private RecyclerView.LayoutManager mHoursLayoutManager;

    private ArrayList<CustomSelectionItem> mMinutesArrayList;
    public RecyclerView mMinutesRecycleView;
    private CenterLockListener mMinutesCenterLock;
    private int mPrevSelectedMinutesPos = 0;
    public View minutesHighlighterView;
    private RecyclerView.Adapter mMinutesAdapter;
    private RecyclerView.LayoutManager mMinutesLayoutManager;

    private ArrayList<CustomSelectionItem> mDayTimeArrayList;
    public RecyclerView mDayTimeRecycleView;
    private CenterLockListener mDayTimeCenterLock;
    private int mPrevSelectedDayTimePos = 0;
    public View dayTimeHighlighterView;
    private RecyclerView.Adapter mDayTimeAdapter;
    private RecyclerView.LayoutManager mDayTimeLayoutManager;

    public static TimeSelectorFragment newInstance() {
        return new TimeSelectorFragment();
    }

    public ArrayList<CustomSelectionItem> getHoursList() {
        ArrayList localArrayList = new ArrayList();
        for (int index = 0; index < 12; index++) {
            localArrayList.add(new CustomSelectionItem(String.valueOf(index), index, false));
        }
        return localArrayList;
    }

    public ArrayList<CustomSelectionItem> getMinutesList() {
        ArrayList localArrayList = new ArrayList();
        for (int index = 0; index < 60; index = index + 5) {
            localArrayList.add(new CustomSelectionItem(String.valueOf(index), index, false));
        }
        return localArrayList;
    }

    public ArrayList<CustomSelectionItem> getdayTimeList() {
        ArrayList localArrayList = new ArrayList();
        localArrayList.add(new CustomSelectionItem("AM", 1, false));
        localArrayList.add(new CustomSelectionItem("PM", 2, false));
        return localArrayList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(R.layout.select_time_fragment_layout, container, false);
        initViews(baseView);
        return baseView;
    }

    private void initViews(View baseView) {
        mHourRecycleView = (RecyclerView) baseView.findViewById(R.id.hour_recycle_view);
        initializeHourRecycleView();
        hourHighlighterView = baseView.findViewById(R.id.hour_highlighter_view);
        hourHighlighterView.getViewTreeObserver().addOnGlobalLayoutListener(new CustomHoursGlobalLayoutListener());

        mMinutesRecycleView = (RecyclerView) baseView.findViewById(R.id.minutes_recycle_view);
        initializeMinutesRecycleView();
        minutesHighlighterView = baseView.findViewById(R.id.minutes_highlighter_view);
        minutesHighlighterView.getViewTreeObserver().addOnGlobalLayoutListener(new CustoMinutesGlobalLayoutListener());

        mDayTimeRecycleView = (RecyclerView) baseView.findViewById(R.id.day_time_recycle_view);
        initializeDayTimeRecycleView();
        dayTimeHighlighterView = baseView.findViewById(R.id.day_time_highlighter_view);
        dayTimeHighlighterView.getViewTreeObserver().addOnGlobalLayoutListener(new CustoDayTimeGlobalLayoutListener());

    }

    public void initializeHourRecycleView() {
        mHoursLayoutManager = new LinearLayoutManager(getContext());
        mHourRecycleView.setLayoutManager(mHoursLayoutManager);
        mHoursArrayList = getHoursList();
        this.mHoursArrayList.get(this.mPrevSelectedHourPos).isSelected = true;
        mHoursAdapter = new CustomSelectorAdapter(getContext(), mHoursArrayList);
        mHourRecycleView.setAdapter(mHoursAdapter);
        mHourRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mHourRecycleView.smoothScrollToPosition(position);
                        handlePositionChanged(position, mHourRecycleView.getId());
                    }
                })
        );
    }

    public void initializeMinutesRecycleView() {
        mMinutesLayoutManager = new LinearLayoutManager(getContext());
        mMinutesRecycleView.setLayoutManager(mMinutesLayoutManager);
        mMinutesArrayList = getMinutesList();
        this.mMinutesArrayList.get(this.mPrevSelectedMinutesPos).isSelected = true;
        mMinutesAdapter = new CustomSelectorAdapter(getContext(), mMinutesArrayList);
        mMinutesRecycleView.setAdapter(mMinutesAdapter);
        mMinutesRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mMinutesRecycleView.smoothScrollToPosition(position);
                        handlePositionChanged(position, mMinutesRecycleView.getId());
                    }
                })
        );
    }

    public void initializeDayTimeRecycleView() {
        mDayTimeLayoutManager = new LinearLayoutManager(getContext());
        mDayTimeRecycleView.setLayoutManager(mDayTimeLayoutManager);
        mDayTimeArrayList = getdayTimeList();
        this.mDayTimeArrayList.get(this.mPrevSelectedDayTimePos).isSelected = true;
        mDayTimeAdapter = new CustomSelectorAdapter(getContext(), mDayTimeArrayList);
        mDayTimeRecycleView.setAdapter(mDayTimeAdapter);
        mDayTimeRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mDayTimeRecycleView.smoothScrollToPosition(position);
                        handlePositionChanged(position, mDayTimeRecycleView.getId());
                    }
                })
        );
    }

    @Override
    public void handlePositionChanged(int position, int viewId) {
        if (R.id.hour_recycle_view == viewId) {
            this.mHoursArrayList.get(mPrevSelectedHourPos).isSelected = false;
            this.mHoursArrayList.get(position).isSelected = true;
            mPrevSelectedHourPos = position;
            this.mHourRecycleView.getAdapter().notifyDataSetChanged();
        } else if (R.id.minutes_recycle_view == viewId) {
            this.mMinutesArrayList.get(mPrevSelectedMinutesPos).isSelected = false;
            this.mMinutesArrayList.get(position).isSelected = true;
            mPrevSelectedMinutesPos = position;
            this.mMinutesRecycleView.getAdapter().notifyDataSetChanged();
        } else if (R.id.day_time_recycle_view == viewId) {
            this.mDayTimeArrayList.get(mPrevSelectedDayTimePos).isSelected = false;
            this.mDayTimeArrayList.get(position).isSelected = true;
            mPrevSelectedDayTimePos = position;
            this.mDayTimeRecycleView.getAdapter().notifyDataSetChanged();
        } else {
            Log.d("manoj", "handlePositionChanged,  did not match to any  pre define ID");
        }
    }

    private class CustomHoursGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mHourCenterLock = new CenterLockListener(getActivityReference(), (hourHighlighterView.getTop() + hourHighlighterView.getBottom()) / 2, TimeSelectorFragment.this);
            mHourRecycleView.setOnScrollListener(mHourCenterLock);
            mHourRecycleView.scrollToPosition(mPrevSelectedHourPos);
            int paddingTop = hourHighlighterView.getTop();
            int paddingBottom = mHourRecycleView.getHeight() - hourHighlighterView.getBottom();
            mHourRecycleView.setPadding(mHourRecycleView.getPaddingLeft(), paddingTop, mHourRecycleView.getPaddingRight(), paddingBottom);
            hourHighlighterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }

    private class CustoMinutesGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mMinutesCenterLock = new CenterLockListener(getActivityReference(), (minutesHighlighterView.getTop() + hourHighlighterView.getBottom()) / 2, TimeSelectorFragment.this);
            mMinutesRecycleView.setOnScrollListener(mMinutesCenterLock);
            mMinutesRecycleView.scrollToPosition(mPrevSelectedHourPos);
            int paddingTop = minutesHighlighterView.getTop();
            int paddingBottom = mMinutesRecycleView.getHeight() - minutesHighlighterView.getBottom();
            mMinutesRecycleView.setPadding(mMinutesRecycleView.getPaddingLeft(), paddingTop, mMinutesRecycleView.getPaddingRight(), paddingBottom);
            minutesHighlighterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }

    private class CustoDayTimeGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mDayTimeCenterLock = new CenterLockListener(getActivityReference(), (dayTimeHighlighterView.getTop() + dayTimeHighlighterView.getBottom()) / 2, TimeSelectorFragment.this);
            mDayTimeRecycleView.setOnScrollListener(mDayTimeCenterLock);
            mDayTimeRecycleView.scrollToPosition(mPrevSelectedDayTimePos);
            int paddingTop = dayTimeHighlighterView.getTop();
            int paddingBottom = mDayTimeRecycleView.getHeight() - dayTimeHighlighterView.getBottom();
            mDayTimeRecycleView.setPadding(mDayTimeRecycleView.getPaddingLeft(), paddingTop, mDayTimeRecycleView.getPaddingRight(), paddingBottom);
            dayTimeHighlighterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }
}

