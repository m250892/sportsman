package com.example.manoj.sportsman.models.listview;

import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.models.CardType;
import com.example.manoj.sportsman.models.userprofile.UserFavSportModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 24/12/15.
 */
public class PlayerCardModel extends BaseCardModel {

    private String id;
    @SerializedName("image_url")
    private String imageUrl;
    private String name;
    private String gender;
    private String dob;
    @SerializedName("min_age")
    private int minAge;
    @SerializedName("area_name")
    private String areaName;
    @SerializedName("address")
    private SearchItemModel searchItemModel;
    @SerializedName("sport_details")
    private List<UserFavSportModel> userFavSportModels;

    public PlayerCardModel() {
        id = "-1";
        imageUrl = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTmSsGzNO8lyF-CxJlljuVRAlfV8Ch33OFgLYkGfyTUC8zSnPDM";
        gender = "Female";
        name = "Gagan Jhakotiya";
        minAge = 22;
        areaName = "Chandivali, Mumbai";
        userFavSportModels = new ArrayList<>();
        userFavSportModels.add(new UserFavSportModel(1));
        userFavSportModels.add(new UserFavSportModel(2));
        userFavSportModels.add(new UserFavSportModel(3));
    }


    @Override
    public CardType getCardType() {
        return CardType.PLAYER_CARD;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public String getDob() {
        return dob;
    }

    public int getMinAge() {
        return minAge;
    }

    public String getAgeGenderString() {
        String result = "";
        if (getMinAge() > 0) {
            result += minAge;
        }
        if (getGender() != null) {
            if (result.length() > 0) {
                result += ", ";
            }
            result += getGender();
        }
        return result;
    }

    public String getName() {
        return name;
    }

    public String getAreaName() {
        return areaName;
    }

    public List<UserFavSportModel> getUserFavSportModels() {
        return userFavSportModels;
    }

    public SearchItemModel getSearchItemModel() {
        return searchItemModel;
    }
}
