package com.example.manoj.sportsman.fragments.recentevents;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.View;

import com.example.manoj.sportsman.fragments.BaseListHolderFragment;

/**
 * Created by manoj on 31/12/15.
 */
public class RecentListHolderFragment extends BaseListHolderFragment {

    private RecentViewPagerAdapter pagerAdapter;

    public static RecentListHolderFragment newInstance() {
        Bundle args = new Bundle();
        RecentListHolderFragment fragment = new RecentListHolderFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void initView(View baseView) {
        super.initView(baseView);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        toolbar.clearDisappearingChildren();
        toolbar.setTitle("My Events");
    }

    @Override
    protected void setViewPagerAdapter() {
        pagerAdapter = new RecentViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
    }
}
