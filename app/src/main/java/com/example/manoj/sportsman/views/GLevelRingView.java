package com.example.manoj.sportsman.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.example.manoj.sportsman.R;

/**
 * Created by manoj on 24/12/15.
 */
public class GLevelRingView extends View {

    private RectF mArcRect = new RectF();
    private Paint mArcPaint;
    private Paint mProgressPaint;

    private int mStartAngle = 0;
    private final int mAngleOffset = -90;
    private int mRotation = 0;
    private int mSweepAngle = 360;
    private int mProgressWidth = 8;
    private int mArcWidth = 8;
    private float mProgressSweep = 0;

    int arcColor = getResources().getColor(R.color.glevel_ring_default_color);
    int beginnerColor = getResources().getColor(R.color.begginer_color);
    int intermediateColor = getResources().getColor(R.color.intermediate_color);
    int advanceColor = getResources().getColor(R.color.advance_color);


    public GLevelRingView(Context context) {
        super(context);
        init();
    }

    //Set level on scale from 0-5
    public void setLevel(float level) {
        this.mProgressSweep = level * 72;
    }

    public GLevelRingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GLevelRingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GLevelRingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        mArcPaint = new Paint();
        mArcPaint.setColor(arcColor);
        mArcPaint.setAntiAlias(true);
        mArcPaint.setStyle(Paint.Style.STROKE);
        mArcPaint.setStrokeWidth(mArcWidth);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(beginnerColor);
        mProgressPaint.setAntiAlias(true);
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setStrokeWidth(mProgressWidth);

        mArcPaint.setStrokeCap(Paint.Cap.ROUND);
        mProgressPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the arcs
        final int arcStart = mStartAngle + mAngleOffset + mRotation;
        final int arcSweep = mSweepAngle;
        canvas.drawArc(mArcRect, arcStart, arcSweep, false, mArcPaint);
        if (mProgressSweep < 120) {
            mProgressPaint.setColor(beginnerColor);
        } else if (mProgressSweep < 240) {
            mProgressPaint.setColor(intermediateColor);
        } else {
            mProgressPaint.setColor(advanceColor);
        }
        canvas.drawArc(mArcRect, arcStart + 5, mProgressSweep, false, mProgressPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(),
                heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(),
                widthMeasureSpec);
        final int min = Math.min(width, height);
        float top = 0;
        float left = 0;
        int arcDiameter = 0;

        arcDiameter = min - getPaddingLeft();
        top = height / 2 - (arcDiameter / 2);
        left = width / 2 - (arcDiameter / 2);
        mArcRect.set(left, top, left + arcDiameter, top + arcDiameter);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
