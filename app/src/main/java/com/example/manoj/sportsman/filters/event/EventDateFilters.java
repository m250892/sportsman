package com.example.manoj.sportsman.filters.event;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.dialogs.DatePickerFragment;
import com.example.manoj.sportsman.dialogs.DialogCallbacks;
import com.example.manoj.sportsman.filters.core.BaseFilterGroup;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;
import com.example.manoj.sportsman.utils.Utils;

/**
 * Created by manoj on 05/01/16.
 */
public class EventDateFilters extends BaseFilterGroup<EventFilterHash> implements View.OnClickListener, DialogCallbacks {

    private FragmentManager fragmentManager;
    private TextView selectDate;
    private LinearLayout rootView;

    public EventDateFilters(Activity activity, FragmentManager fragmentManager) {
        super(activity);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView() {
        View baseView = getInflater().inflate(R.layout.event_date_filter_layout, null);
        rootView = (LinearLayout) baseView.findViewById(R.id.root_view);
        selectDate = (TextView) baseView.findViewById(R.id.select_date);

        /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        rootView.addView((new EventTimeFilter(getActivity())).getView(), params);*/

        selectDate.setOnClickListener(this);
        return baseView;
    }

    @Override
    public boolean isActivated() {
        return false;
    }

    @Override
    public void writeToFilterHash(EventFilterHash filterHash) {

    }

    @Override
    public void readFromFilterHash(EventFilterHash filterHash) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select_date) {
            handleDataSelectClick();
        }
    }

    private void handleDataSelectClick() {
        DatePickerFragment datePickerFragment = new DatePickerFragment(this, DatePickerFragment.START_DATE);
        datePickerFragment.show(fragmentManager, "Start Date Picker");
    }

    @Override
    public void onDateSelected(int requestCode, int year, int month, int day) {
        String text = Utils.getDateMonthYearFormatString(day, month, year);
        selectDate.setText(text);
    }

    @Override
    public void onTimeSelected(int requestCode, int hourOfDay, int minute) {

    }

    @Override
    public void onGameSelected(int gameId) {

    }
}
