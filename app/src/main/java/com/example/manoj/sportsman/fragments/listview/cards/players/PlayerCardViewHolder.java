package com.example.manoj.sportsman.fragments.listview.cards.players;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.fragments.listview.cards.core.BaseListCardViewHolder;

/**
 * Created by manoj on 24/12/15.
 */
public class PlayerCardViewHolder extends BaseListCardViewHolder {

    public TextView nameView;
    public TextView ageGenderView;
    public TextView areaName;
    public ImageView profilePic;
    public View addFriendView;
    public LinearLayout gameLevelView;

    public PlayerCardViewHolder(View baseView) {
        super(baseView);
        profilePic = (ImageView) baseView.findViewById(R.id.profile_image);
        nameView = (TextView) baseView.findViewById(R.id.name);
        ageGenderView = (TextView) baseView.findViewById(R.id.age_gender);
        areaName = (TextView) baseView.findViewById(R.id.area_name);
        addFriendView = baseView.findViewById(R.id.add_friend_view);
        gameLevelView = (LinearLayout) baseView.findViewById(R.id.glevel_view);
    }
}
