package com.example.manoj.sportsman.filters.core;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.filters.filterhash.BaseFilterHash;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;
import com.example.manoj.sportsman.views.AutoCompleteWithClearButton;
import com.example.manoj.sportsman.views.ExpandableHeightGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 03/01/16.
 */
public abstract class BaseSearchAndSelectCard<T extends BaseFilterHash> extends BaseFilterGroup<T> implements AdapterView.OnItemClickListener {

    private AutoCompleteWithClearButton searchView;
    private ExpandableHeightGridView selectedView;
    private List<String> selectedItemList = new ArrayList<>();
    private ArrayAdapter<String> gridAdapter;

    public BaseSearchAndSelectCard(Activity activity) {
        super(activity);
    }


    public View getView() {
        View baseView = getInflater().inflate(R.layout.search_and_select_view_layout, null);
        searchView = (AutoCompleteWithClearButton) baseView.findViewById(R.id.search_view);
        selectedView = (ExpandableHeightGridView) baseView.findViewById(R.id.seleted_item_view);
        TextView title = (TextView) baseView.findViewById(R.id.title);
        title.setText(getLabel());
        initializeViews();
        return baseView;
    }

    public void writeToFilterHash(EventFilterHash filterHash) {

    }

    public void readFromFilterHash(EventFilterHash filterHash) {

    }

    @Override
    public boolean isActivated() {
        return false;
    }

    private void initializeViews() {
        GameSearchAndSelectAdapter adapter = new GameSearchAndSelectAdapter(getActivity(), allResults());
        searchView.setAdapter(adapter);
        searchView.setOnItemClickListener(this);
        gridAdapter = new ArrayAdapter<String>(getContext(),
                R.layout.grid_game_selected_single_item_view, R.id.text_view, selectedItemList);
        selectedView.setAdapter(gridAdapter);
        selectedView.setOnItemClickListener(girdViewItemClickListener);
    }

    private AdapterView.OnItemClickListener girdViewItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectedItemList.remove(position);
            gridAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedItemList.add((String) parent.getAdapter().getItem(position));
        searchView.setText("");
        gridAdapter.notifyDataSetChanged();
    }

    protected abstract String getLabel();

    protected abstract List<String> allResults();
}
