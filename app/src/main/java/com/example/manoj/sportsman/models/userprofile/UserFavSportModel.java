package com.example.manoj.sportsman.models.userprofile;

import com.example.manoj.sportsman.Constants;
import com.example.manoj.sportsman.utils.Model;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 10/12/15.
 */
public class UserFavSportModel extends Model {
    @SerializedName("sport_name")
    private String gameName;
    @SerializedName("sport_id")
    private int gameId;
    @SerializedName("is_editable")
    private boolean isEditable;
    @SerializedName("proficiency")
    private float rating;
    private String remarks;

    public UserFavSportModel(int gameId) {
        this.gameId = gameId;
        gameName = Constants.getGameName(gameId);
        this.rating = 2.5f;
        isEditable = false;
    }

    public String getGameName() {
        return gameName;
    }

    public int getGameId() {
        return gameId;
    }

    public float getRating() {
        return rating;
    }

    public String getRemarks() {
        return "Knows basic and getting better";
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

}
