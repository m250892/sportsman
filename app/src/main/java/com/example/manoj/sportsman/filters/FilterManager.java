package com.example.manoj.sportsman.filters;

import com.example.manoj.sportsman.filters.filterhash.BaseFilterHash;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;
import com.example.manoj.sportsman.filters.filterhash.PlayerFilterHash;
import com.example.manoj.sportsman.models.userprofile.CardsListType;

import java.util.HashMap;

/**
 * Created by manoj on 07/01/16.
 */
public class FilterManager {

    private static FilterManager instance;
    private HashMap<CardsListType, BaseFilterHash> filterHashMap;
    private CardsListType service;

    private BaseFilterHash copidFilterHash;

    private FilterManager() {
        // Temp code till all the pieces are not fit together
        filterHashMap = new HashMap<>();
        setService(CardsListType.EVENTS_LIST);
    }

    public void setService(CardsListType service) {
        this.service = service;
        if (filterHashMap.containsKey(service) == false) {
            initializeFilterHash(service);
        }
    }

    public static FilterManager getInstance() {
        if (instance == null) {
            init();
        }
        return instance;
    }

    public static synchronized void init() {
        instance = new FilterManager();
    }

    public CardsListType getService() {
        if (service == null) {
            service = CardsListType.EVENTS_LIST;
        }
        return service;
    }


    private void initializeFilterHash(CardsListType service) {
        BaseFilterHash filterHash = null;

        if (filterHash == null) {
            filterHash = createBlankFilterHashInstance(service);
        }
        filterHashMap.put(service, filterHash);
    }

    private BaseFilterHash createBlankFilterHashInstance(CardsListType service) {
        switch (service) {
            case EVENTS_LIST:
                return new EventFilterHash();
            case PLAYERS_LIST:
                return new PlayerFilterHash();
        }
        return new EventFilterHash();
    }

    public BaseFilterHash getFilterHash() {
        return this.filterHashMap.get(this.service);
    }

}
