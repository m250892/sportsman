package com.example.manoj.sportsman.filters.event;

import android.app.Activity;

import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.filters.core.BaseSpinnerFilter;
import com.example.manoj.sportsman.filters.filterhash.EventFilterHash;

/**
 * Created by manoj on 06/01/16.
 */
public class EventTimeFilter extends BaseSpinnerFilter<EventFilterHash> {

    private static final int DEFAULT_ITEM_INDEX = 0;

    public EventTimeFilter(Activity activity) {
        super(activity);
    }

    @Override
    public boolean isActivated() {
        return false;
    }

    @Override
    public void writeToFilterHash(EventFilterHash filterHash) {

    }

    @Override
    public void readFromFilterHash(EventFilterHash filterHash) {

    }

    @Override
    protected String[] getItemArray() {
        return this.getActivity().getResources().getStringArray(R.array.event_time);
    }

    @Override
    protected int getDefaultItemIndex() {
        return DEFAULT_ITEM_INDEX;
    }

    @Override
    protected String getLabel() {
        return "TIME";
    }
}
