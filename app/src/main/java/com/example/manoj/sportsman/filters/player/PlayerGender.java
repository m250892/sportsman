package com.example.manoj.sportsman.filters.player;

import android.app.Activity;

import com.example.manoj.sportsman.filters.FilterConstants;
import com.example.manoj.sportsman.filters.core.BaseCheckBoxFilterGroup;
import com.example.manoj.sportsman.filters.filterhash.PlayerFilterHash;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by manoj on 08/01/16.
 */
public class PlayerGender extends BaseCheckBoxFilterGroup<PlayerFilterHash> {

    private static final int ELEMENTS_PER_ROW = 1;

    public PlayerGender(Activity activity) {
        super(activity);
    }

    @Override
    protected Map getLinkedHashMap() {
        return FilterConstants.PLAYER_GENDER_MAP;
    }

    @Override
    protected int getNumberOfElementsPerRow() {
        return ELEMENTS_PER_ROW;
    }

    @Override
    protected String getLabel() {
        return "GENDER";
    }

    @Override
    protected void setFilterHashList(PlayerFilterHash filterHash, ArrayList<String> typeList) {

    }

    @Override
    protected ArrayList<String> getKeyListFromFilterHash(PlayerFilterHash filterHash) {
        return null;
    }
}
