package com.example.manoj.sportsman.models.listview;

import com.example.manoj.sportsman.MainApplication;
import com.example.manoj.sportsman.R;
import com.example.manoj.sportsman.databasemanager.SearchItemModel;
import com.example.manoj.sportsman.models.CardType;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 24/12/15.
 */
public class EventCardModel extends BaseCardModel {

    private String id;
    @SerializedName("sport_name")
    private String sportName;
    @SerializedName("sport_id")
    private int sportId;
    private String epochTime;
    private SearchItemModel address;
    @SerializedName("is_free")
    private boolean isFree;
    @SerializedName("event_type")
    private int eventType;

    public EventCardModel() {
        id = "1";
        sportName = "Cricket";
        sportId = 1;
        epochTime = "1449339197";
        isFree = false;
        eventType = 1;
    }

    @Override
    public CardType getCardType() {
        return CardType.EVENT_CARD;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSportName() {
        return sportName;
    }

    public int getSportId() {
        return sportId;
    }

    public String getEpochTime() {
        return epochTime;
    }

    public String getAddress() {
        String result = "";
        if (address != null) {
            result = address.getAddressDetail();
        }
        return result;
    }

    public boolean isFree() {
        return isFree;
    }

    public String getEventType() {
        return MainApplication.getContext().getResources().getStringArray(R.array.game_type)[eventType];
    }

    public String getPriceType() {
        int position;
        if (isFree) {
            position = 0;
        } else {
            position = 1;
        }
        return MainApplication.getContext().getResources().getStringArray(R.array.price_type)[position];
    }

}
